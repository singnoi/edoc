<?php 
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$uploadDir = 'book_file/'; 
$response = array( 
    'status' => 0, 
    'message' => 'บันทึกไม่สำเร็จ กรุณาตรวจสอบข้อมูลก่อนบันทึกอีกครั้ง.' 
); 

$member_id = $_SESSION['member_id'];
$dep_id = $_SESSION['doc_dep_id'];
$dep2_id = $_SESSION['doc_dep2_id'];
$ol_id = $_SESSION['operator'];



// If form is submitted 
if(isset($_POST['comment'])){ 
    // Get the submitted form data 
    $book_id = $_POST['book_id']; 
    $book_year = $_POST['book_year'];
    $book_no = $_POST['book_no'];
    $to_member_id = $_POST['member_id']; 
    $comment = clean($_POST['comment']); 


                 
            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                 
                // File path config 
                $fileName = basename($_FILES["file"]["name"]); 
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 

                $random_no = strtotime($today);

                $newfile = $book_year."_".$book_no."_".$random_no.".".$fileType;
                $targetFilePath = $uploadDir . $newfile; 

                // Allow certain file formats 
                $allowTypes = array('pdf', 'jpg', 'png', 'jpeg'); 
                if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                        $uploadedFile = $newfile; 
                        $uploadStatus = 1;
                    }else{ 
                        $uploadStatus = 0; 
                        $response['message'] = 'ผิดพลาด! ไฟล์ไม่สมบูรณ์.'; 
                    } 
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'ผิดพลาด! ไม่ใช่ไฟล์PDF.'; 
                } 
            } else {
                $uploadStatus = 0; 
                $response['message'] = 'บันทึกสำเร็จ';
            } 
             
            if($uploadStatus == 1){ 

                $qi = "INSERT INTO edoc_file (book_id, `file_name`) values ('$book_id','$uploadedFile')";
                $con->query($qi) or die ($qi);
                $qf = "SELECT `file_id` from edoc_file where `file_name` = '$uploadedFile'";
                $file_id = $con->query($qf)->fetch_object()->file_id;

                $response['message'] = 'บันทึกสำเร็จ '; 
            } else {
                $file_id = "NULL";
            } 

// test
/*
$response['message'] = $ol_id;
echo json_encode($response);
exit();
*/
// ----            
$con_s = connect();

    foreach ($to_member_id as $key => $value_id) {
        $qo = "SELECT ol_id,dep_id,dep2_id from edoc_operator where member_id = '$value_id' order by o_id ASC limit 1 ";
        $ro = $con->query($qo) or die ($qo);
        if($ro->num_rows > 0) {
            $obm = $ro->fetch_object();
            $to_ol_id = $obm->ol_id;
            $to_dep_id = $obm->dep_id;
            $to_dep2_id = $obm->dep2_id;
            //$response['message'] .= $obm->ol_id."<br>";

        } else {
            $to_ol_id = 0;

            $qm = "SELECT d2.department_sub2_id, d2.department_id from members as m left outer join department_sub2 as d2 on d2.department_sub2_id = m.department_sub2_id where m.member_id = '$value_id'";
            $rm = $con_s->query($qm) or die ($qm);
            $obm = $rm->fetch_object();
            $to_dep_id = $obm->department_id;
            $to_dep2_id = $obm->department_sub2_id;
        }
        
        
        $qb = "INSERT INTO edoc_to (book_id,dep_id,dep2_id,member_id,to_datetime,to_member_id,comment,ol_id,`file_id`,to_ol_id,to_dep_id,to_dep2_id) values ('$book_id','$dep_id','$dep2_id','$member_id','$today','$value_id','$comment','$ol_id',$file_id,'$to_ol_id','$to_dep_id','$to_dep2_id')";
        $con->query($qb) or die ($qb);

        if($to_ol_id == 5) {
            $qu = "UPDATE edoc_book SET director_accept = 4 where book_id = '$book_id' and director_accept < 5";
            $con->query($qu) or die ($qu);
        } else {
            $qu = "UPDATE edoc_book SET director_accept = 1 where book_id = '$book_id' and director_accept < 2";
            $con->query($qu) or die ($qu);
        }
        if($ol_id == 5) {
            $qu = "UPDATE edoc_book SET director_accept = 5 where book_id = '$book_id'";
            $con->query($qu) or die ($qu);
        }
        
    }
$con_s->close();
        $response['status'] = 1;
        $response['message'] = ' บันทึกสำเร็จ ';
} 
$con->close();
// Return response 
echo json_encode($response);