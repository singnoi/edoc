<?php
session_start();
$main_operator = $_SESSION['main_operator'];
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$book_id = $_GET['book_id'];
$dep_id =  $_SESSION['doc_dep_id'];
$dep2_id =  $_SESSION['doc_dep2_id'];
$operator =  $_SESSION['operator'];
$member_id = $_SESSION['member_id'];
$qb = "SELECT * from edoc_book where book_id = '$book_id'";
$rb = $con->query($qb) or die ($qb);
$ob = $rb->fetch_object();
$book_year = $ob->book_year;
$book_no = $ob->book_no;
?>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
              <i class="fas fa-plus mr-2"></i> นำส่ง
</button>


           <table class="table table-sm table-bordered table-striped ">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>ผู้ส่ง</th>
                        <th>วันที่สั่งการ</th>
                        <th>คำสั่งการ</th>
                        <th>ส่งถึง</th>                       
                        <th class='text-center'>วันที่รับทราบ</th>
                        <th class="text-center">ไฟล์แนบ</th>
                    </tr>
                </thead>
                <tbody>
<?php

$con_s = connect();
$qt = "SELECT *,
f.file_id,
f.file_name,
l.ol_name,
l2.ol_name as to_ol_name,
t.ol_id,
t.to_ol_id,
t.dep_id,
t.to_dep_id
FROM
edoc_to AS t
LEFT OUTER JOIN edoc_file AS f ON t.file_id = f.file_id
LEFT OUTER JOIN edoc_operator_level AS l ON t.ol_id = l.ol_id
LEFT OUTER JOIN edoc_operator_level AS l2 ON t.to_ol_id = l2.ol_id
WHERE
t.book_id = '$book_id'
order by t.to_datetime ASC, t.to_dep_id DESC, t.to_dep2_id ASC
";
$rt = $con->query($qt) or die ($qt);
if($rt->num_rows > 0 ) {
    $i = 0;
    while ($obt = $rt->fetch_object()) {
         $i++;
        $qs = "SELECT concat(name_only,' ',lname) as fullname from members where member_id='$obt->member_id'";
        $fullname = $con_s->query($qs)->fetch_object()->fullname;
        $qst = "SELECT concat(name_only,' ',lname) as fullname from members where member_id='$obt->to_member_id'";
        $to_fullname = $con_s->query($qst)->fetch_object()->fullname;
        if($obt->ol_name != NULL) {
            if($obt->ol_id == 1 || $obt->ol_id == 3) {
                $qd = "SELECT department_name from departments where department_id = '$obt->dep_id'";
                $dep_name = $con_s->query($qd)->fetch_object()->department_name;
            }
            if($obt->ol_id == 2 || $obt->ol_id == 4) {
                $qd = "SELECT department_sub2_name from department_sub2 where department_sub2_id = '$obt->dep2_id'";
                $dep_name = $con_s->query($qd)->fetch_object()->department_sub2_name;
            }
            $sender = $dep_name."<br><code class='small'>(".$fullname.")</code>";
        } else {
            $sender = $fullname;
        }
        if($obt->to_ol_name != NULL) {
            if($obt->to_ol_id == 1 || $obt->to_ol_id == 3) {
                $qd = "SELECT department_name from departments where department_id = '$obt->to_dep_id'";
                $to_dep_name = $con_s->query($qd)->fetch_object()->department_name;
                //$dep_name = $dep_name."-1";
            }
            if($obt->to_ol_id == 2 || $obt->to_ol_id == 4) {
                $qd = "SELECT department_sub2_name from department_sub2 where department_sub2_id = '$obt->to_dep2_id'";
                $to_dep_name = $con_s->query($qd)->fetch_object()->department_sub2_name;
                //$dep_name = $dep_name."-2";
            }
            $reciver = $to_dep_name."<br><code class='small'>(".$to_fullname.")</code>";
        } else {
            $reciver = $to_fullname;
        }
        echo "<tr>";
        echo "<td>";
        if($obt->member_id == $member_id && $obt->member_read == 'N') {
            ?>
            <a href="#" onclick="del_to('<?php echo $book_id;?>','<?php echo $obt->to_id;?>','<?php echo $obt->file_id;?>');" class="text-danger"><i class="far fa-times-circle"></i></a>
            <?php 
        } else {
            echo $i;
        }
        echo "</td>";
        echo "<td>".$sender."</td>";
        echo "<td>".date_thai_time($obt->to_datetime)."</td>";
        echo "<td>".$obt->comment."</td>";
        echo "<td>".$reciver."</td>";       
        echo "<td class='text-center'>";
        if($obt->read_datetime != NULL) {
             echo "<span class='badge badge-info shadow'>".date_thai_time($obt->read_datetime)."</span>";
        } else {
            if($obt->operator_read != NULL) {
                $qs = "SELECT name_only from members where member_id='$obt->operator_read'";
                $oname = $con_s->query($qs)->fetch_object()->name_only;
                echo "<span class='badge badge-info shadow'>".date_thai_time($obt->operator_read_datetime)."</span><span class='text-danger small'>($oname)</span>";
            } else {

                if( ($operator > 0 && $obt->to_dep_id == $dep_id) || $member_id == $obt->to_member_id  ) {
                    echo "<button onclick='confirm_read($book_id,$member_id,$obt->to_id);' class='btn btn-right btn-sm btn-secondary'><i class='fas fa-clipboard-check'></i> กดยืนยันรับ</button>";
                }
            }
        } 
        echo "</td>";
        echo "<td class='text-center'>";
        if($obt->file_name == NULL) {
            echo "";
        } else {
        ?>
        <a href="#" class="text-primary shadow " onclick="open_book('<?php echo $obt->file_name;?>','<?php echo $book_id;?>');"> <i class="fas fa-file-pdf"></i></a>
        <?php 
        }
        echo "</td>";
        echo "</tr>";
    }
}
                    ?>
                </tbody>
            </table>
                



<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">นำส่ง</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <form id="form_to"  enctype="multipart/form-data" >
        <input type="hidden" name="book_id" id="book_id" value="<?php echo $book_id;?>" >
        <input type="hidden" name="book_year" value="<?php echo $ob->book_year;?>" >
        <input type="hidden" name="book_no" value="<?php echo $ob->book_no;?>" >
        <div id="select_member">
        <div class="form-group">
            <label for="email">กรุณาเลือกหมวดผู้ส่งถึง:</label>
<?php if($_SESSION['main_operator'] == "Y" || $_SESSION['operator'] == 1 || $_SESSION['operator'] == 3 || $_SESSION['member_level'] == 'admin' ) { 
 
    ?> 
            <button type="button" class="btn btn-warning shadow" onclick="s_director(<?php echo $dep_id;?>);">ผู้อำนวยการ</button>
<?php } ?>
            <button type="button" class="btn btn-warning shadow" onclick="s_dep(<?php echo $dep_id;?>);">กลุ่มภารกิจ</button>
            <button type="button" class="btn btn-warning shadow" onclick="s_member(<?php echo $dep_id;?>);">บุคคล</button>
            <?php if($_SESSION['main_operator'] == "Y" || $_SESSION['operator'] == 1 || $_SESSION['operator'] == 3  || $_SESSION['member_level'] == 'admin' ) { ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                    กลุ่มงาน
                    </button>
                    <div class="dropdown-menu">
                    <?php 
                    $d = "SELECT department_sub2_id,department_sub2_name from department_sub2 where department_id = '$dep_id'";
                    $rd = $con_s->query($d) or die ($d);
                    while ($od = $rd->fetch_object()) {
                        ?>
                        <a class="dropdown-item" href="#" onclick="s_dep_member('<?php echo $dep_id;?>','<?php echo $od->department_sub2_id;?>');" ><?php echo $od->department_sub2_name;?></a>
                        <?php
                    }
                    ?>

                    </div>
                </div>
            
            <?php } ?>

            <?php if($_SESSION['main_operator'] == "Y" || $_SESSION['member_level'] == 'admin' ) { ?>
            <button type="button" class="btn btn-warning shadow" onclick="s_all(<?php echo $dep_id;?>);">ทุกคนใน รพ.</button>
            <?php } ?> 

        </div>

        <div class="form-group" >
                <label for="email">ส่งถึง:</label>
                        <select class="form-control" name="member_id" required >
                            <option value="">กรุณาเลือกหมวดผู้ส่งถึงด้านบนก่อน</option>                           
                        </select>
            </div>
        </div>

            
            <div class="form-group">
                <label for="comment">ความคิดเห็น:</label>
                <textarea class="form-control" rows="5" id="comment" name="comment"  required></textarea>
            </div>
            <div class="form-group">
                <input type="file"  id="file" name="file" class="form-control-file border">
            </div>

            <span class="statusMsg"></span>
            
            <button type="submit" class="btn btn-primary submitBtn float-right">บันทึก</button>
        </form>

      </div>

      <!-- Modal footer -->
      <!-- <div class="modal-footer">
      <span class="statusMsg"></span>
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div> -->

    </div>
  </div>
</div>
<?php $con->close(); ?>
<?php $con_s->close(); ?>

<script>

function del_to(id,tid,fid) {
    Swal.fire({
        title: 'แน่ใจการยกเลิก?',
        text: "หากมั่นใจที่การยกเลิกให้กดปุ่มยืนยัน!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน!',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("del_to.php",{book_id: id,to_id: tid,file_id: fid},function(info){
                if(info == 'ok') {
                    $('#show_data').load("book_to_data.php?book_id="+id);
                    //window.location = "?page=book_manage&book_id="+id;
                } else {
                    alert(info);
                }
            });
        }
    });
}

function confirm_read(id,mid,tid) {
    Swal.fire({
        title: 'ยืนยันรับ?',
        text: "หากมั่นใจหนังสือถูกต้อง ให้กดปุ่มยืนยัน!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'success',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("history_read_action.php",{book_id: id, member_id: mid,to_id: tid},function(info){
                if(info == 'ok') {
                    $('#show_data').load("book_to_data.php?book_id="+id);
                }
            });
        }
    });
}

function s_director(id) {
    $('#select_member').load("select_director.php?dep_id="+id);
}
function s_dep(id) {
    $('#select_member').load("select_dep.php?dep_id="+id);
}

function s_member(id) {
    $('#select_member').load("select_member.php?dep_id="+id);
}

function s_dep_member(id,id2) {
    $('#select_member').load("select_dep_member.php?dep_id="+id+"&dep2_id="+id2);
}

function s_all(id) {
    $('#select_member').load("select_all_member.php?dep_id="+id);
}

function open_book(n,id) {
    var url = "show_file.php?fn="+n;
    $.post("history_read_action.php",{book_id: id},function(info){
        window.open(url, '_blank');
        if(info == 'ok') {
            $('#show_data').load("book_to_data.php?book_id="+id);
        }
    });
    
}

$("#form_to").on('submit', function(e){
        e.preventDefault();
        var book_id = $('#book_id').val();
        $.ajax({
            type: 'POST',
            url: 'book_to_action.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                //$('.submitBtn').attr("disabled","disabled");
                //$('#form_to').css("opacity",".5");
            },
            success: function(response){ 
                //console.log(response);
                
               //$('.statusMsg').html('');

               $('.statusMsg').html(response);

                if(response.status == 1){

                    $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');

                    window.location = "?page=book_manage&book_id="+book_id;
                    
                } else {
                    $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    $(".submitBtn").removeAttr("disabled");
                }
                //$('#form_to').css("opacity","");
                //$(".submitBtn").removeAttr("disabled");

            }
        });
  
});

// File type validation

$("#file").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf', 'image/jpeg', 'image/png', 'image/jpg'];
    if( !( (fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) || (fileType == match[3]) ) ){
        alert('ไม่ใช่ไฟล์ PDF และ ม่ใช่ไฟล์ JPG');
        $("#file").val('');
        return false;
    }
});

</script>