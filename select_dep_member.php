<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$dep_id = $_GET['dep_id'];
$dep2_id = $_GET['dep2_id'];

?>
<div class="form-group">
            <label for="email">กรุณาเลือกหมวดผู้ส่งถึง:</label>
<?php if($_SESSION['main_operator'] == "Y" || $_SESSION['operator'] == 1 || $_SESSION['operator'] == 3 || $_SESSION['member_level'] == 'admin' ) { ?>            
            <button type="button" class="btn btn-warning shadow" onclick="s_director(<?php echo $dep_id;?>);">ผู้อำนวยการ</button>
<?php } ?>            
            <button type="button" class="btn btn-warning shadow" onclick="s_dep(<?php echo $dep_id;?>);">กลุ่มภารกิจ</button>
            <button type="button" class="btn btn-warning shadow" onclick="s_member(<?php echo $dep_id;?>);">บุคคล</button>
            <?php if($_SESSION['main_operator'] == "Y" || $_SESSION['operator'] == 1 || $_SESSION['operator'] == 3  || $_SESSION['member_level'] == 'admin' ) { ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                    กลุ่มงาน
                    </button>
                    <div class="dropdown-menu">
                    <?php 
                    $d = "SELECT department_sub2_id,department_sub2_name from department_sub2 where department_id = '$dep_id'";
                    $rd = $con_s->query($d) or die ($d);
                    while ($od = $rd->fetch_object()) {
                        ?>
                        <a class="dropdown-item" href="#" onclick="s_dep_member('<?php echo $dep_id;?>','<?php echo $od->department_sub2_id;?>');" ><?php echo $od->department_sub2_name;?></a>
                        <?php
                    }
                    ?>

                    </div>
                </div>
            
            <?php } ?>

            <?php if($_SESSION['main_operator'] == "Y" || $_SESSION['member_level'] == 'admin' ) { ?>
            <button type="button" class="btn btn-warning shadow" onclick="s_all(<?php echo $dep_id;?>);">ทุกคนใน รพ.</button>
            <?php } ?> 
</div>
<div class="form-group" >
<label for="email">ส่งถึงบุคคล:</label>
                        <select class="form-control" name="member_id[]" id="member_id" multiple="multiple"  required >
                            
                            <?php

                            $qm = "SELECT * from members as m left outer join department_sub2 as s on s.department_sub2_id = m.department_sub2_id where m.member_status = 'Y' and m.user_other = 'N' and s.department_sub2_id = '$dep2_id' order by s.department_sub2_name ASC, m.name_only ASC ";
                            $rm = $con_s->query($qm) or die ($qm);
                            $con_s->close();
                            while ($obm = $rm->fetch_object()) {
                                 $sl = "selected";

                                $occ = "";
                                $qr = "SELECT
                                l.ol_name
                                FROM
                                edoc_operator AS o
                                LEFT OUTER JOIN edoc_operator_level AS l ON o.ol_id = l.ol_id
                                WHERE
                                o.member_id = '$obm->member_id'";
                                $rr = $con->query($qr) or die ($qr);
                                $nr = $rr->num_rows;
                                if($nr > 0) {
                                    $k = 0;
                                    while ($obr = $rr->fetch_object()) {
                                        $k++;
                                        if($k == 1) $occ .= "(";
                                        if($k != 1) $occ .= ",";
                                        $occ .= $obr->ol_name;
                                        if($k == $nr) $occ .= ")";

                                    }
                                }

                                echo "<option value='$obm->member_id' $sl >".$obm->name_only." ".$obm->lname."/".$obm->department_sub2_name.$occ."</option> ";
                            }
                            ?>
                        </select>
</div>
<?php $con->close();?>
<script>
//$.fn.select2.defaults.set( "theme", "bootstrap" );
$('#member_id').select2({
    theme: "bootstrap"
});
</script>