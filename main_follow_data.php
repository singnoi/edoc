<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$status = "";
$member_id = $_SESSION['member_id'];
$operator = $_SESSION['operator'];

?>

    <div class="row ">
        <div class="col-lg-12">

            <div class="card border-0 shadow ">
            <div class="card-header bg-success text-white">
           
                <i class="fas fa-cloud-download-alt fa-1x text-white shadow mr-2"></i> <span > หนังสือติดตาม 
       
                <span class="float-right">
                
                </span>
            </div>
            <div class="card-body">
            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.reg_datetime,
                    b.book_no,
                    b.director_accept,
                    f.ext_from_name,
                    s.speed_name,
                    b.ssn,
                    b.deadline_date,
                    t.ext_to_name,
                    b.end_follow 
                    FROM
                    edoc_book AS b
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_ext_to AS t ON b.ext_to_id = t.ext_to_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    WHERE
                    b.deadline_date is not NULL
                    ORDER BY
                    b.reg_datetime DESC
                    ";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm" id="tb1">
                    <thead>
                        <tr>
                            <th>เลขที่รับ</th>
                            <th>เร่งด่วน</th>
                            <th>หนังสือเลขที่</th>
                            <th>วันที่หนังสือ</th>
                            <th>วันครบกำหนด</th> 
                            <th>เรื่อง</th>
                            <th>จาก</th>                            
                            <th>ถึง</th>
                                                  
                            <th class="text-center">อ่านแล้ว</th>
                            <th class="text-center">ติดตาม</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {
                                $from_name = $obi->ext_from_name;
                                $to_name = $obi->ext_to_name;
                                

                                echo "<tr>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php 
                                echo $obi->ssn;
                                ?>
                                </a>
                                <?php 
                                echo "</td>";
                                echo "<td>".$obi->speed_name."</td>";

                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php echo $obi->book_code; ?>
                                </a>
                                <?php 
                                echo "</td>";
                                echo "<td>".date_thai($obi->book_date)."</td>";
                                echo "<td>".date_thai($obi->deadline_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,35,'UTF-8')."..</td>";
                                echo "<td>".iconv_substr($from_name,0,22,'UTF-8')."..</td>";
                                echo "<td>".iconv_substr($to_name,0,12,'UTF-8')."..</td>";          
                                                             
                                echo "<td class='text-center'>";
                                $qr = "SELECT count(to_id) as sum_r from edoc_to where book_id = '$obi->book_id' and member_read = 'Y'";
                                $sum_read = $con->query($qr)->fetch_object()->sum_r;
                                $qrn = "SELECT count(to_id) as sum_n from edoc_to where book_id = '$obi->book_id' and member_read = 'N'";
                                $sum_no = $con->query($qrn)->fetch_object()->sum_n;
                                ?>
                                <a href="#" onclick="show_read(<?php echo $obi->book_id;?>)" class="ml-2">
                                <span class="badge badge-dark"> 
                                <?php 
                                    echo "อ่าน ".comma($sum_read);
                                    echo "/ไม่อ่าน ".comma($sum_no);
                                
                                ?>
                                </span></a>
                                <?php 
                                echo "</td>";
                                echo "<td class='text-center'>";
                                if($obi->end_follow == 'Y') {
                                    echo "<i class='fas fa-check-circle text-success'></i>";
                                } else {
                                    echo "<i class='far fa-clock text-warning'></i>";
                                }
                                echo "</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                
                
            </div>
        </div>

        </div>
    </div>

<?php
$con->close();
$con_s->close();
?>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ดำเนินการ</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ประวัติการเปิดอ่าน</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body p-1" id="md2-body">
        Modal body..
      </div>


    </div>
  </div>
</div>

<script>
$('#myModal').on('hidden.bs.modal', function (e) {
  window.location = "?page=ext_in";
})


function manage_book(id) {
    $('#md_body').load("book_detail.php?book_id="+id);
}

function del_book(id) {
    Swal.fire({
        title: 'แน่ใจจะลบ?',
        text: "ลบแล้วเอาคืนไม่ได้นะ!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน!',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("ext_in_del.php",{book_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=ext_in";
                } else {
                    alert(info);
                }
            })
        }
    });
}

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาหัวข้อเรื่อง :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 25,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    "ordering": false,
    //"searching": false,
    //"paging":  false

});

function open_book(n,id) {
    var url = "show_file.php?fn="+n;
    $.post("history_read_action.php",{book_id: id},function(info){
        window.open(url, '_blank');
    });
    
}

function show_read(id) {
    $('#md2-body').load("history_read.php?book_id="+id);
    $('#md2').modal('show');
    
}
</script>