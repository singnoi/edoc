<?php
$con = connect_db($db);
$con_s = connect();
$status = "";
$main_operator = $_SESSION['main_operator'];
$admin = $_SESSION['member_level'];
$type_id = 3;
if(isset($_GET['type_id'])) $type_id = $_GET['type_id'];
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">

            <div class="card border-0 ">
            <div class="card-header bg-danger text-white">
                
               

                
                <span class="float-right">
                <a href="?page=ext_out_add" class="text-light textshadow">
                <i class="far fa-file"></i> ลงทะเบียนหนังสือส่งออกใหม่
                </a>
                </span>
                
            </div>
            <div class="card-body">
            <form class="form-inline" >
                    <div class="input-group mb-3" id="add_type">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-danger text-white">
                        <i class="fas fa-cloud-upload-alt fa-1x text-white shadow mr-2"></i>  
                        ทะเบียนส่งออกหนังสือภายนอก</span>
                    </div>
                    <select id="type_id" class="form-control" name="type_id" required >

                            <?php
                            $qf = "SELECT * from edoc_type order by `type_id` ASC";
                            $rf = $con->query($qf) or die ($qf);
                            if( $rf->num_rows > 0 ) {
                                while ($obf = $rf->fetch_object()) {
                                    if($obf->type_id == $type_id) $sl = "selected";
                                    else $sl = "";
                                    echo "<option value='$obf->type_id' $sl > $obf->type_name </option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    </form>
            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.reg_datetime,
                    b.book_no,
                    b.director_accept,
                    f.ext_from_name,
                    s.speed_name,
                    df.file_name,
                    b.ssn,
                    b.member_id,
                    t.type_name
                    FROM
                    edoc_book AS b
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    LEFT OUTER JOIN (SELECT max(`file_id`) as max_id, book_id from edoc_file group by book_id) as file_max on (file_max.book_id = b.book_id)
                    LEFT OUTER JOIN edoc_file as df ON (df.file_id = file_max.max_id) 
                    LEFT OUTER JOIN edoc_type as t ON b.type_id = t.type_id 
                    WHERE
                    b.section_id = 2
                    and b.type_id = $type_id
                    ORDER BY
                    b.director_accept ASC,
                     b.reg_datetime DESC
                    ";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm" id="tb1">
                    <thead>
                        <tr>
                            <th>ประเภท</th>
                            <th>หนังสือเลขที่</th>
                            <th>วันที่</th>
                            <th>เรื่อง</th>
                            <th>จาก</th>                            
                            <th>ถึง</th>
                            <th>วันที่ส่ง</th>
                            <th class="text-center" width="80">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {

                                $qm1 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$obi->member_id' ";
                                $from_name = $con_s->query($qm1)->fetch_object()->fullname;

                                echo "<tr>";
                                echo "<td>".$obi->type_name."</td>";

                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php echo $obi->book_code; ?>
                                </a>
                                <?php 

                                if($obi->director_accept < 2) {
                                    ?>
                                    <a href="?page=ext_out_edit&book_id=<?php echo $obi->book_id;?>" class="text-warning">
                                        <i class="fas fa-edit fa-sm mr-1"></i>
                                    </a>
                                    <a href="#" onclick="del_book('<?php echo $obi->book_id;?>');" class="text-danger">
                                        <i class="fas fa-trash fa-sm "></i>
                                    </a>
                                    <?php 
                                }
                                echo "</td>";
                                echo "<td>".date_thai_xs($obi->book_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,45,'UTF-8')."..</td>";
                                echo "<td>".iconv_substr($from_name,0,28,'UTF-8')."..</td>";

                                echo "<td>".iconv_substr($obi->ext_from_name,0,28,'UTF-8')."..</td>";                               
                               
                                echo "<td>".date_thai_xs_time($obi->reg_datetime)."</td>";
                                echo "<td>";
                                if($main_operator == 'Y' || $admin == 'admin') {
                                ?>
                                <a href="#" onclick="manage_book('<?php echo $obi->book_id;?>');" data-toggle="modal" data-target="#myModal" > <?php echo director_accept($obi->director_accept);?></a>
                                <?php 
                                } else {
                                    echo director_accept($obi->director_accept);
                                }
                                if($obi->file_name != NULL) {
                                    echo "<a href='#' onclick='open_book(\"$obi->file_name\",$obi->book_id)'><i class=\"fas fa-paperclip\"></i></a>";
                                }
                                echo "</td>";
                               
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                
                
            </div>
        </div>

        </div>
    </div>
</div>
<?php
$con->close();
$con_s->close();
?>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ดำเนินการ</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->

    </div>
  </div>
</div>


<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ประวัติการเปิดอ่าน</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body p-1" id="md2-body">
        Modal body..
      </div>


    </div>
  </div>
</div>

<script>

function show_read(id) {
    $('#md2-body').load("history_read.php?book_id="+id);
    $('#md2').modal('show');
    
}

$('#myModal').on('hidden.bs.modal', function (e) {
  window.location = "?page=ext_out";
});


function manage_book(id) {
    $('#md_body').load("book_detail.php?book_id="+id);
}

function del_book(id) {
    Swal.fire({
        title: 'แน่ใจจะลบ?',
        text: "ลบแล้วเอาคืนไม่ได้นะ!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน!',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("ext_in_del.php",{book_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=ext_out";
                } else {
                    alert(info);
                }
            })
        }
    });
}

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาหนังสือ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    "ordering": false,
    //"searching": false,
    //"paging":  false

});

function open_book(n,id) {
    var url = "show_file.php?fn="+n;
    $.post("history_read_action.php",{book_id: id},function(info){
        window.open(url, '_blank');
    });
    
}

$("#type_id").change(function(){
    var type_id = $('#type_id').val();
    window.location = "?page=ext_out&type_id="+type_id;

});
</script>