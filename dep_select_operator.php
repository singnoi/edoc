<?php
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$id = $_GET['id'];
$dep_id = $_GET['dep_id'];
$dep2_id = $_GET['dep2_id'];
$ol_id = $_GET['ol_id'];
$member_id = $_GET['member_id'];
?>
<form id="form_member<?php echo $id;?>">
    <input type="hidden" name="dep_id" value="<?php echo $dep_id;?>">
    <input type="hidden" name="dep2_id" value="<?php echo $dep2_id;?>">
    <input type="hidden" name="ol_id" value="<?php echo $ol_id;?>">
    <input type="hidden" name="action" value="operator">
    <div class="input-group">
                        
                        <select class="form-control form-control-sm" style="width: 240px" id="member_id" name="member_id[]" multiple="multiple"  required >
                            <option value="">เลือกรายชื่อ</option>
                            <?php
                            $qm = "SELECT * from members where member_status = 'Y' and user_other = 'N' order by name_only ASC ";
                            $rm = $con_s->query($qm) or die ($qm);
                            while ($obm = $rm->fetch_object()) {

                                // if($member_id == $obm->member_id) $sl = "selected";
                                // else $sl = "";
                                $q = "SELECT member_id from edoc_operator where dep_id = $dep_id and dep2_id = $dep2_id and ol_id = $ol_id and member_id = '$obm->member_id' ";
                                if($con->query($q)->num_rows > 0) $sl = "selected";
                                else $sl = "";
                                echo "<option value='$obm->member_id' $sl >".$obm->name_only." ".$obm->lname."</option> ";
                            }
                            ?>
                        </select>
        <div class="input-group-append">
            <button class="btn btn-primary btn-sm" type="submit"><i class="fas fa-save"></i></button>
        </div>
    </div>
    <span id="o<?php echo $id;?>"></span>
</form>
<?php $con->close(); 
$con_s->close();
?>

<script>
$('select').select2({
    theme: 'bootstrap',
});
var id = "<?php echo $id;?>";
$('#form_member'+id).submit(function(e){
    e.preventDefault();
    $.post("dep_select_action.php",$('#form_member'+id).serialize(),function(info){

        $('#o'+id).html(info);
        if(info == 'ok') {
            window.location = "?page=dep";
        } else {
            $('#o'+id).html(info);
        }
    });
});
</script>