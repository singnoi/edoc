<?php
$db = "office_db";
$main_operator = 52;
$hos_code = "ขก 0032.";

function date_thai_xs_time($strDate){
	if($strDate!='' && $strDate!='0000-00-00') {
          $strYear = date("Y",strtotime($strDate))+543;
          $strYear = substr($strYear,2,2);
		  $strMonth= date("n",strtotime($strDate));
		  $strDay= date("j",strtotime($strDate));
		  $strHour= date("H",strtotime($strDate));
		  $strMinute= date("i",strtotime($strDate));
		  $strSeconds= date("s",strtotime($strDate));
		  return "$strDay/$strMonth/$strYear ".$strHour.".".$strMinute;
	} else {
	  return "-";
	}
}
function director_accept($id) {
	switch ($id) {
		case '0':
			$step_name = "<span class='badge badge-warning shadow'>หนังสือใหม่</span>";
			break;
		case '1':
			$step_name = "<span class='badge badge-info shadow'>ระหว่างนำส่ง</span>";
			break;
		case '2':
			$step_name = "<span class='badge badge-danger shadow'>เสนอรองบ.</span>";
			break;
		case '3':
			$step_name = "<span class='badge badge-secondary shadow'>รองบ.พิจารณาแล้ว</span>";
			break;
		case '4':
			$step_name = "<span class='badge badge-primary shadow'>เสนอ ผอ.</span>";
			break;
		case '5':
			$step_name = "<span class='badge badge-success shadow'>สั่งการแล้ว</span>";
			break;
		
		default:
			$step_name = "<span class='badge badge-warning shadow'>หนังสือใหม่</span>";
			break;
	}
	return $step_name;
}

function book_section($id) {
	switch ($id) {
		case '1':
			$step_name = "<span class='badge badge-info shadow'>หนังสือภายใน</span>";
			break;
		case '2':
			$step_name = "<span class='badge badge-warning shadow'>หนังสือส่งออกภายนอก</span>";
			break;
		case '3':
			$step_name = "<span class='badge badge-primary shadow'>หนังสือภายนอก</span>";
			break;
		
		default:
			$step_name = "<span class='badge badge-danger shadow'>หนังสือใหม่</span>";
			break;
	}
	return $step_name;
}

function clean($string) {
   //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9ก-๙เแ\-\+\(\)\_\=\@\.]/', ' ', $string); // Removes special chars.
}
?>