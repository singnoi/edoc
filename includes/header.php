
    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light  justify-content-between" >
        <a class="navbar-brand" href="../../edoc/">
          <img src="../../edoc/img/book_logo.png" class="d-inline-block align-top rounded-circle" alt="LOGO" width="40" height="40" >
  
          <span class="kanit_l textshadow " > สารบรรณอิเล็กทรอนิกส์ </span>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navb">
     
          <!-- Links -->
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <?php if($logined) { 
        $con = connect_db($db);
        $qxi = "SELECT count(t.to_id) as sum_3
                FROM edoc_to as t 
                left outer join edoc_book as b on b.book_id = t.book_id
                WHERE
                    t.to_member_id = '$member_id'
                    and t.member_read = 'N'
                    ";
         // and b.section_id = 3           
      
        $sum_3 = $con->query($qxi)->fetch_object()->sum_3;
        if($sum_3 > 0) $sum_3 = "<span class=\"badge badge-pill badge-danger\"> $sum_3 </span>";
        else $sum_3 = "";
        
        ?>
        
        <?php if($_SESSION['main_operator'] == 'Y') { ?>
          <li class="nav-item">
          <a class="nav-link text-danger small kanit_l" href="?">( สารบรรณกลาง )</a>
          </li>
        <?php } ?>
        <?php if($_SESSION['member_level'] == 'admin') { ?>
          <li class="nav-item">
          <a class="nav-link text-danger small kanit_l" href="?">( ผู้ดูแลระบบ )</a>
          </li>
        <?php } ?>

        <li class="nav-item">
          <?php 
          $act3 = "";
          $act1 = "";
          if(isset($_GET['section_id'])) {
            if($_GET['section_id'] == 3 ) $act3 = "active";
            if($_GET['section_id'] == 1 ) $act1 = "active";
          }
          ?>
          <a class="btn btn-outline-primary ml-2 <?php echo $act3;?> " href="?page=main">หนังสือถึงฉัน
          <?php echo $sum_3;?>
          </a>
          </li>
          <!--
          <li class="nav-item">
          <a class="btn btn-outline-info ml-2 <?php echo $act1;?> " href="?page=main_in&section_id=1">หนังสือภายใน</a>
          </li>
          -->
          <li class="nav-item">
          
          <a class="btn btn-outline-danger ml-2 <?php if($page=='main_out_int') echo "active";?>" href="?page=main_out_int">ทะเบียนหนังสือภายในของฝ่าย</a>
          </li>
        
    <!-- Dropdown -->
    <li class="nav-item dropdown ml-2">
      <a class="nav-link dropdown-toggle btn btn-outline-warning" href="#" id="navbardrop" data-toggle="dropdown"> <i class="fas fa-search"></i>
        ค้นหา
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="?page=main_all">ค้นหาหนังสือทั้งหมด</a>
        <a class="dropdown-item" href="?page=main_follow">หนังสือติดตาม</a>
        <a class="dropdown-item" href="?page=report_noread">หนังสือค้างอ่าน</a>
        <a class="dropdown-item" href="?page=report_sum">รายงานปริมาณหนังสือ</a>

      </div>
    </li>

        <?php if($_SESSION['member_level'] == 'admin' || $_SESSION['operator'] > 0 ) { 
          $dep_id = $_SESSION['doc_dep_id'];
          $dep2_id = $_SESSION['doc_dep2_id'];
          if($_SESSION['operator'] == 1 || $_SESSION['operator'] == 3 ) $dep_where = $dep_id;
          else $dep_where = $dep2_id;
          $q4 = "SELECT count(t.to_id) as sum_4
          FROM edoc_to as t 
          left outer join edoc_book as b on b.book_id = t.book_id
          WHERE
              t.to_dep_id = '$dep_where'
              and b.section_id = 3 
              and t.member_read = 'N'
              ";           

            $sum_4 = $con->query($q4)->fetch_object()->sum_4;
            if($sum_4 > 0) $sum_4 = "<span class=\"badge badge-pill badge-danger\"> $sum_4 </span>";
            else $sum_4 = "";

            $q5 = "SELECT count(t.to_id) as sum_5
            FROM edoc_to as t 
            left outer join edoc_book as b on b.book_id = t.book_id
            WHERE
                t.to_dep_id = '$dep_where'
                and b.section_id = 1 
                and t.member_read = 'N'
                ";           
  
              $sum_5 = $con->query($q5)->fetch_object()->sum_5;
              if($sum_5 > 0) $sum_5 = "<span class=\"badge badge-pill badge-danger\"> $sum_5 </span>";
              else $sum_5 = "";
          
          ?>
          <!-- Dropdown -->
          <li class="nav-item dropdown ml-2">
            <a class="nav-link dropdown-toggle btn btn-outline-success" href="#" id="operator" data-toggle="dropdown"> <i class="fas fa-user"></i>
              สารบรรณหน่วยงาน
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="?page=main_in&section_id=3">หนังสือรับภายนอกของฝ่าย <?php echo $sum_4;?></a>
              <a class="dropdown-item" href="?page=main_in&section_id=1">หนังสือภายในของฝ่าย <?php echo $sum_5;?></a>
            </div>
          </li>

        <?php } ?>
      <?php } ?>

        </ul>
       
          <!-- <form class="form-inline my-2 my-lg-0"> -->
            <?php if($logined) { ?>

              <?php if($_SESSION['member_level'] == 'admin' || $_SESSION['main_operator'] == 'Y' ) { ?>
                <a class="my-2 my-sm-0 text-danger" href="../../edoc/?page=main_admin" data-toggle="tooltip" title="จัดการระบบ" data-placement="bottom" > <i class="fas fa-cog fa-spin fa-2x mr-3"></i></a>

              <?php } ?>
               
                     
                <a class="my-2 my-sm-0 text-info mr-3" href="../../member/" data-toggle="tooltip" title="<?php echo $_SESSION['person']." (".$_SESSION['dep_name'].")"; ?>" data-placement="bottom" > <i class="fas fa-address-book fa-2x "></i><small class="text-dark">
                <?php echo $_SESSION['person']." (".$_SESSION['operator_name']; ?>)
                </small>
                </a>
                
                <a class="my-2 my-sm-0 text-warning " onclick="confirm_out();" id="bt_logout" href="#" data-toggle="tooltip" title="ออกจากระบบ" > <i class="fas fa-power-off fa-2x mr-3"></i></a>
          
            <?php } else { ?>
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#md-01" > <i class="fas fa-sign-in-alt mr-2"></i>เข้าสู่ระบบ</button>

            <?php } ?>

            <!-- </form> -->

        </div>
       
    </nav>
    <script>
      function confirm_out() {

        Swal.fire({
          title: "ต้องการออกจากระบบใช่ใหม?",
          text: "ควรออกจากระบบทุกครั้ง เมื่อเลิกใช้งาน",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#f46b02',
          cancelButtonColor: '#e0dcd9',
          confirmButtonText: "ออกจากระบบ",
        }).then((result) => {
          if (result.value) {
            window.location = '../../login/logout.php?url=edoc';
          }
        })

      }
      </script>


<!-- The Modal -->
<div class="modal" id="md-01">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">ลงชื่อเข้าใช้ระบบ</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
                <form id="login_bar" method="post" >
                <input type="hidden" name="url" id="url" value="edoc">
                    <div class="form-group">
                      <label for="email">ชื่อผู้ใช้:</label>
                      <div class="input-group mb-3">
                      <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-envelope"></i></span>
                        </div>
                      <input type="text" class="form-control" id="username" autocomplete="off" placeholder="ชื่อผู้ใช้" required >
                    </div>
                    </div>
                    <div class="form-group">
                      <label for="pwd">รหัสผ่าน:</label>
                      <div class="input-group mb-3">
                            <div class="input-group-prepend">

                                  <span class="input-group-text"><i class="fa fa-key"></i></span>
                              </div>
                      <input type="password" class="form-control" id="password" placeholder="รหัสผ่าน" required>
                      </div>
                    </div>
                    
                    <div class="form-group text-right ">
                      <button type="submit" id="btn_login" class="btn btn-primary "><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</button>
                    </div>
                  </form>
                  <span id="check_login"></span>
        </div>
  
       
  
      </div>
    </div>
  </div>
                    <script>
                    $('#login_bar').submit(function(e){
                      e.preventDefault();
                      var username = $("#username").val();
                      var password = $("#password").val();
                      var url = $("#url").val();
 
                      $.post("../../login/login_check.php",{username: username, password: password, url: url},
                        function(data){
                          var obj = jQuery.parseJSON(data);
                          //$('#check_login').html(obj.status);

                            if(obj.status=='ok'){

                              Swal.fire({
                                    title: "ลงชื่อเข้าใช้ระบบสำเร็จ",
                                    text: obj.person,
                                    closeOnClickOutside: false,
                                    type: "success"
                              }).then((result) => {
                                    if (result.value) {
                                      window.location = obj.url;
                                    }
                              });

                            } else {

                              Swal.fire({
                                title: "ลงชื่อเข้าใช้ ไม่สำเร็จ",
                                text: obj.warning,
                                closeOnClickOutside: false,
                                confirmButtonColor: '#f46b02',
                                type: "warning"
                                });

                            }

                      });

                    });
 
                    </script>
