<?php
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$book_id = $_GET['book_id'];
$q = "SELECT * from edoc_book as b 
left outer join edoc_ext_from as f on f.ext_from_id = b.ext_from_id 
left outer join edoc_ext_to as t on t.ext_to_id = b.ext_to_id 
left outer join edoc_speed as s on s.speed_id = b.speed_id 
left outer join edoc_file as df on df.file_id = b.director_file_id
where b.book_id = '$book_id'";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();
//test 
//echo $q;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header textshadow">
                เรื่อง <?php echo $ob->book_subject;?>
            </div>
            <div class="card-body">
                เลขที่รับ <span class="underline_dot font-weight-bold"><?php echo $ob->ssn;?></span>
                เลขที่ <span class="underline_dot font-weight-bold"><?php echo $ob->book_code;?></span> / วันที่ <span class="underline_dot font-weight-bold"><?php echo date_thai($ob->book_date);?></span> / โดย <span class="underline_dot font-weight-bold"><?php echo $ob->ext_from_name;?></span> / เรียน <span class="underline_dot font-weight-bold"><?php echo $ob->ext_to_name;?></span>
            </div>
        </div>
        </div>
    </div>

    <div class="row mt-1">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header textshadow bg-warning">
                ยื่นเสนอรองบริหาร
            </div>
            <div class="card-body">
            <form id="form_super">
            <input type="hidden" name="book_id" value="<?php echo $book_id;?>">
                <div class="form-row">
                <div class="col">
                <label for="fam_no" class="mr-sm-2">แฟ้มเลขที่:</label>
                <input type="number" min="1" class="form-control mb-2 mr-sm-2" id="fam_no_super" name="fam_no_super" value="<?php echo $ob->fam_no_super;?>" required >
                </div>
                <div class="col">
                <label for="in_date" class="mr-sm-2">วันที่เข้า:</label>
                <input type="text" class="form-control date" placeholder="" name="in_date_super" id="in_date_super" value="<?php echo date_thai_input($ob->in_date_super);?>" required >
                </div>
                <div class="col">
                <label for="out_date" class="mr-sm-2">วันที่ออก:</label>
                <input type="text" class="form-control date" placeholder="" name="out_date_super" id="out_date_super" value="<?php echo date_thai_input($ob->out_date_super);?>" >
                </div>
                <div class="col text-right">
                <button type="submit" class="btn btn-warning mt-4 mr-sm-2 shadow">บันทึก</button>
                 </div>
                  </div>
                <span id="show_save_super"></span>
            </form>
                
            </div>
        </div>
        </div>
    </div>

    <?php 
    if($ob->director_accept < 3 ) $ds = "disabled";
    else $ds = "";
    ?>
    <div class="row mt-1">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header textshadow bg-info">
                ยื่นเสนอผู้อำนวยการ
            </div>
            <div class="card-body">

            <form id="form_director">
                <input type="hidden" name="book_id" value="<?php echo $book_id;?>">
                <div class="form-row">
                <div class="col-4">
                <label for="fam_no" class="mr-sm-2">แฟ้มเลขที่:</label>
                <input type="number" min="1" class="form-control mb-2 mr-sm-2" id="fam_no_director" name="fam_no_director" value="<?php echo $ob->fam_no_director;?>" required >
                </div>
                <div class="col">
                <label for="in_date" class="mr-sm-2">วันที่เข้า:</label>
                <input type="text" class="form-control date" placeholder="" name="in_date_director" id="in_date_director" value="<?php echo date_thai_input($ob->in_date_director);?>" required >
                </div>
               
               
                <div class="col text-right">
                
                <button type="submit" class="btn btn-primary mt-4 shadow" <?php echo $ds;?> >บันทึก</button>
                </div>
                 </div>
                <span id="show_save_director"></span>
            </form>
            <div class="row">
                <div class="col-lg-12 bg-success p-2">

            <form id="form_director_out"  enctype="multipart/form-data">
                <input type="hidden" name="book_id" value="<?php echo $book_id;?>">
                <input type="hidden" name="book_no" value="<?php echo $ob->book_no;?>" >
                <input type="hidden" name="book_year" value="<?php echo $ob->book_year;?>" >
                <div class="form-row">
                <div class="col-4">
                <label for="out_date" class="mr-sm-2">วันที่ออก:</label>
                <input type="text" class="form-control date" placeholder="" name="out_date_director" id="out_date_director" value="<?php echo date_thai_input($ob->out_date_director);?>" required >
                </div>

                <div class="col-6">
                <label for="out_date" class="mr-sm-2">แนบไฟล์สแกน: <?php echo $ob->file_name;?> </label>
                <input type="file" class="form-control-file border" id="file" name="file" required />
                </div>
               
                <div class="col-2 text-right">
                
                <button type="submit" class="btn btn-primary mt-4 shadow submitBtn" >บันทึก</button>
                </div>
                 </div>
                <span id="show_out_director" class="statusMsg"></span>
            </form>
                </div>
            </div>
                
            </div>
        </div>
        </div>
    </div>


</div>

<script>

$(".date").inputmask({"mask": "99/99/9999"});

$('.date').datepicker({
        todayBtn: 'linked',
        language: "th-th",
        keyboardNavigation: true,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
        //startDate: '-3d'
});

$('#form_super').submit(function(e){
    e.preventDefault();
    $.post("book_to_super.php",$("#form_super").serialize(),function(info){
        if(info == 'ok') {
            $('#show_save_super').html('<p class="alert alert-success">บันทึกสำเร็จ</p>');
        } else {
            $('#show_save_super').html('<p class="alert alert-danger">ผิดพลาด:'+info+'</p>');
        }
    });
});

$('#form_director').submit(function(e){
    e.preventDefault();
    $.post("book_to_director.php",$("#form_director").serialize(),function(info){
        if(info == 'ok') {
            $('#show_save_director').html('<p class="alert alert-success">บันทึกสำเร็จ</p>');
        } else {
            $('#show_save_director').html('<p class="alert alert-danger">ผิดพลาด:'+info+'</p>');
        }
    });
});



$("#form_director_out").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'book_to_director_file.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#form_director_out').css("opacity",".5");
            },
            success: function(response){ 
                //console.log(response);
                //$('.statusMsg').html(response);
               $('.statusMsg').html('');

                if(response.status == 1){
                    //$('#form_director_out')[0].reset();
                    $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                }else{
                    $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    $(".submitBtn").removeAttr("disabled");
                }
                $('#form_director_out').css("opacity","");
                $(".submitBtn").removeAttr("disabled");

            }
        });
  
});

// File type validation

$("#file").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf'];
    if( !(fileType == match[0]) ){
        alert('ไม่ใช่ไฟล์ PDF.');
        $("#file").val('');
        return false;
    }
});

</script>