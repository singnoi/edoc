<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);

$member_id = $_SESSION['member_id'];
$operator = $_SESSION['operator'];
$dep2_id = $_SESSION['doc_dep2_id'];
$dep_id = $_SESSION['doc_dep_id'];
if($operator == 1 or $admin = 'admin') {
    $where_dep = " OR b.dep_id = $dep_id ";
    $where_dep_to = " OR t.to_dep_id = $dep_id ";
} elseif ($operator == 2) {
    $where_dep = " OR b.dep2_id = $dep2_id ";
    $where_dep_to = " OR t.to_dep2_id = $dep2_id ";
} else {
    $where_dep = "";
    $where_dep_to = "";
}
?>

    

    <div class="row">
        <div class="col-lg-6" id="show_in_int">

        <div class="card border-0 shadow">
            <div class="card-header bg-info text-white">
                <a href="?page=main_in&section_id=1" class="text-white">
                <i class="fas fa-cloud-download-alt fa-1x text-white shadow mr-2"></i> <span > หนังสือภายใน ที่ถูกส่งถึง 5 เรื่องล่าสุด </span><small  class="font-weight-bold textshadow">(<?php echo $_SESSION['doc_dep_name'];?>) </small>
                </a>
                
            </div>
            <div class="card-body">
            <?php
            $qxi = "SELECT
            b.book_id,
            b.book_code,
            b.book_subject,
            b.book_date,
            b.director_accept,
            b.member_id,
            b.ssn
            FROM
            edoc_book AS b
            inner join edoc_to as t on b.book_id = t.book_id
            LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id 
            WHERE
            (t.to_member_id = '$member_id' $where_dep_to )
            and b.section_id = 1 
            group by b.book_id 
            ORDER BY
            b.director_accept ASC,
            b.reg_datetime DESC
            LIMIT 5";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm">
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {
                                echo "<tr>";
                                //echo "<th>".$obi->ssn."</th>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php 
                                echo $obi->book_code." ".director_accept($obi->director_accept);
                                echo "</a></td>";
                                echo "<td>".date_thai_xs($obi->book_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,35,'UTF-8')."..</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                <p class="text-right">
                <a href="?page=main_in&section_id=1" >ดูทั้งหมด...</a>
                </p>
                
            </div>
        </div>

        </div>

        <div class="col-lg-6" id="show_out_int">

        <div class="card border-0 shadow">
            <div class="card-header bg-danger text-white">
                <a href="?page=main_out_int" class="text-white">
                <i class="fas fa-cloud-download-alt fa-1x text-white shadow mr-2"></i> <span > ทะเบียนหนังสือภายในของฝ่าย 5 เรื่องล่าสุด </span><small  class="font-weight-bold textshadow">(<?php echo $_SESSION['doc_dep_name'];?>) </small>
                </a>
                
            </div>
            <div class="card-body">
            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.director_accept,
                    b.reg_datetime,
                    b.book_no,
                    f.ext_from_name,
                    s.speed_name,
                    b.ssn
                    FROM
                    edoc_book AS b
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    WHERE
                    (b.member_id = '$member_id' $where_dep )
                    and b.section_id = 1 
                    ORDER BY b.director_accept ASC,b.reg_datetime DESC
                    LIMIT 5";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm">
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {
                                echo "<tr>";
                               // echo "<th>".$obi->ssn."</th>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php 
                                echo $obi->book_code." ".director_accept($obi->director_accept);
                                echo "</a></td>";
                                echo "<td>".date_thai_xs($obi->book_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,35,'UTF-8')."..</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                <p class="text-right">
                <a href="?page=main_out_int" >ดูทั้งหมด...</a>
                </p>
                
            </div>
        </div>


        </div>
    </div>


