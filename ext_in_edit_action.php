<?php 
session_start();
$member_update = $_SESSION['member_id'];
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$uploadDir = 'book_file/'; 

$file_id = $_POST['file_id'];
$file_name = $_POST['file_name'];

$response = array( 
    'status' => 0, 
    'message' => 'บันทึกไม่สำเร็จ กรุณาตรวจสอบข้อมูลก่อนบันทึกอีกครั้ง.',
    'file_name' => $file_name 
); 


 
// If form is submitted 
if(isset($_POST['book_subject']) || isset($_POST['book_no']) || isset($_POST['file'])){ 
    // Get the submitted form data 

    $book_subject = $_POST['book_subject']; 
    $book_no = $_POST['book_no']; 
    $book_year = $_POST['book_year']; 
    $section_id = $_POST['section_id']; 
    $reg_datetime = $_POST['reg_datetime']; 
    $dep_id = $_POST['dep_id']; 
    $dep2_id = $_POST['dep2_id']; 
    $member_id = $_POST['member_id']; 
    $book_code = $_POST['book_code']; 
    $book_date = date_thai_db($_POST['book_date']); 
    $speed_id = $_POST['speed_id']; 
    $deadline_date = date_thai_db($_POST['deadline_date']); 

    $book_id = $_POST['book_id'];
    $qy = "SELECT book_id from edoc_book where book_code = '$book_code' and book_year = '$book_year' and book_id <> '$book_id'";
    $ry = $con->query($qy) or die ($qy);
    if($ry->num_rows > 0) {
        $response['message'] = 'เลขที่หนังสือซ้ำ! ลงทะเบียนเลขนี้อีกไม่ได้แล้วครับ';
        echo json_encode($response);
        exit();
    }

    if($deadline_date == NULL || $deadline_date == '' || $deadline_date == '__/__/____') {
        $deadline_date = "NULL";
    } else {
        $deadline_date = "'$deadline_date'";
    }

    $ext_from_id = $_POST['ext_from_id']; 
    if( $ext_from_id !== null && !is_numeric( $ext_from_id ) ) {
        $qz2 = "SELECT ext_from_id from edoc_ext_from where ext_from_name = '$ext_from_id'";
        $rz2 = $con->query($qz2) or die ($qz2);
        if($rz2->num_rows > 0) {
            $ext_from_id = $rz2->fetch_object()->ext_from_id;
        } else {
            $qz = "INSERT INTO edoc_ext_from (ext_from_name) values ('$ext_from_id')";
            $con->query($qz) or die ($qz);
            $qz1 = "SELECT ext_from_id from edoc_ext_from where ext_from_name = '$ext_from_id'";
            $ext_from_id = $con->query($qz1)->fetch_object()->ext_from_id;
        }
    }

    $ext_to_id = $_POST['ext_to_id']; 
    if( $ext_to_id !== null && !is_numeric( $ext_to_id ) ) {
        $qz2 = "SELECT ext_to_id from edoc_ext_to where ext_to_name = '$ext_to_id'";
        $rz2 = $con->query($qz2) or die ($qz2);
        if($rz2->num_rows > 0) {
            $ext_to_id = $rz2->fetch_object()->ext_to_id;
        } else {
            $qz = "INSERT INTO edoc_ext_to (ext_to_name) values ('$ext_to_id')";
            $con->query($qz) or die ($qz);
            $qz1 = "SELECT ext_to_id from edoc_ext_to where ext_to_name = '$ext_to_id'";
            $ext_to_id = $con->query($qz1)->fetch_object()->ext_to_id;
        }
    }


             
             
            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                 
                // File path config 
                $fileName = basename($_FILES["file"]["name"]); 
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 

                $random_no = strtotime($today);

                $newfile = $book_year."_".$book_no."_".$random_no.".".$fileType;
                $targetFilePath = $uploadDir . $newfile; 

                // Allow certain file formats 
                $allowTypes = array('pdf'); 
                if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                        $uploadedFile = $newfile; 
                        $uploadStatus = 1;
                        $response['file_name'] = $uploadedFile;
                    }else{ 
                        $uploadStatus = 0; 
                        $response['message'] = 'ผิดพลาด! ไฟล์ไม่สมบูรณ์.'; 
                    } 
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'ผิดพลาด! ไม่ใช่ไฟล์PDF.'; 
                } 

            } else {
                $uploadStatus = 0; 
                $response['message'] = 'บันทึกสำเร็จ โดยไม่เปลี่ยนไฟล์ครับ';
            } 
             
            if($uploadStatus == 1){ 

                $qu = "SELECT `file_id` from edoc_file where `file_id` = '$file_id'";
                $ru = $con->query($qu) or die ($qu);
                if( $ru->num_rows > 0) {

                    $qm = "UPDATE edoc_file SET `file_name`='$uploadedFile' where `file_id` = '$file_id' ";
                    $con->query($qm) or die ($qm);

                } else {
                    // stamp file
                    $qm = "INSERT INTO edoc_file (book_id,`file_name`) values ('$book_id','$uploadedFile'); ";
                    $con->query($qm) or die ($qm);

                }

                $response['file_name'] = $uploadedFile;

                $response['message'] = 'บันทึกสำเร็จ ';
            }  

    $q = "UPDATE edoc_book SET book_code='$book_code', book_subject='$book_subject', book_date='$book_date', ext_from_id='$ext_from_id', ext_to_id='$ext_to_id', deadline_date=$deadline_date, speed_id='$speed_id', member_update = '$member_update' where book_id = '$book_id' ";
    $result = $con->query($q);
    if($result) {
        $response['status'] = 1;
        

    }
        
} 
 
// Return response 
echo json_encode($response);