<?php
$con = connect_db($db);
$note = "";
?>
<div class="container">
    <div class="row">
        
        <div class="col-lg-7">
            <div class="card">
                <div class="card-body">
                    <table class="table table-light">
                        <thead class="thead-light">
                            <tr>
                                <th>ปี พ.ศ.</th>
                                <th>หมวดหนังสือ</th>
                                <th>ประเภทหนังสือ</th>
                                
                                <th>เลขที่เริ่มต้น</th>
                                <th>ลบ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            $q = "SELECT *,n.section_id from edoc_first_no as n left outer join edoc_section as s on s.section_id = n.section_id left outer join edoc_type as t on t.type_id = n.type_id order by n.book_year DESC, n.section_id ASC, n.`type_id` ASC ";
                            $r = $con->query($q) or die ($q);
                            if($r->num_rows > 0) {
                                while ($ob = $r->fetch_object()) {
                                    echo "<tr>";
                                    echo "<th>$ob->book_year</th>";
                                    echo "<th>$ob->section_name</th>";
                                    echo "<th>$ob->type_name</th>";
                                    echo "<th>$ob->first_no</th>";
                                    echo "<th>";
                                    ?>
                                    <a href="#" onclick="del_item('<?php echo $ob->first_id;?>');" class="text-danger" > <i class="fas fa-trash"></i> </a>
                                    <?php 
                                    echo "</th>";
                                    echo "</tr>";
                                }
                                
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="card">
                <div class="card-body">

                    <form action="?page=admin_first_no" id="form_add" method="post">
                    <div class="form-group">
                        <label for="section_id">เลือกหมวดหนังสือ:</label>
                        <select class="form-control" id="section_id" name="section_id" required >
                        <option></option>
                        <?php 
                        $qs = "SELECT * from edoc_section ";
                        $rs = $con->query($qs) or die ($qs);
                        if( $rs->num_rows > 0) {
                            while ($obs = $rs->fetch_object()) {
                                echo "<option value='$obs->section_id'> $obs->section_name </option>";
                            }
                        }
                        ?>
                        </select> 
                    </div>

                    <div class="form-group">
                        <label for="type_id"><a href="#" onclick="plus_type();" > <i class="far fa-plus-square fa-sm"></i></a> &nbsp;  เลือกประเภทหนังสือ:</label>
                        <div id="div_type">
                        <select class="form-control" id="type_id" name="type_id" required >
                        <option></option>
                        <?php 
                        $qs = "SELECT * from edoc_type ";
                        $rs = $con->query($qs) or die ($qs);
                        if( $rs->num_rows > 0) {
                            while ($obs = $rs->fetch_object()) {
                                if($obs->type_id == 3) $sl = "selected";
                                else $sl = "";
                                echo "<option value='$obs->type_id' $sl > $obs->type_name </option>";
                            }
                        }
                        ?>
                        </select> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inyear">ปี พ.ศ:</label>
                        <input type="number" min="2563" maxlength="4" class="form-control" placeholder="256x" id="book_year" name="book_year" required> 
                    </div>
                    <div class="form-group">
                        <label for="first_no">เลขที่เริ่มต้น:</label>
                        <input type="number" min="1" class="form-control" placeholder="0" id="first_no" name="first_no" required >
                    </div>
                    
                    <button type="submit" class="btn btn-primary">บันทึก</button>

                    <span id="note" class="text-danger"><?php echo $note;?></span>
                    </form>
                    
                </div>
            </div>
        </div>

    </div>
</div>
<?php $con->close(); ?>
<script>

var input_type = "<input type='text' class='form-control' name='type_id' required >";
function plus_type() {
    $('#div_type').html(input_type);
}

$('#form_add').submit(function(e){
    e.preventDefault();
    $.post("admin_first_no_action.php",$('#form_add').serialize(),function(info){
        if(info == 'ok') {
            $('#note').html("<span class='text-success'> บันทึกสำเร็จ </span>");
            window.location = "?page=admin_first_no";
        } else {
            $('#note').html(info);
        }
    });
});
function del_item(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.post("admin_first_no_action.php",{id:id},function(info){
                $('#note').html(info);
                if( info == 'ok') {
                    window.location = "?page=admin_first_no";
                }
            });
        }
    });
}
</script>