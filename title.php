<div class="jumbotron text-center mt-5" style="background-color: #FFC1C1;">
  <h1 class="kanit text-light textB40 ">สารบรรณอิเล็กทรอนิกส์</h1> 
  <p class="text-light">โรงพยาบาลสิรินธร จังหวัดขอนแก่น </p> 
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissible fade show kanit">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fas fa-radiation-alt fa-spin fa-2x mr-2"></i> <span class="mx-auto my-3"> <strong>Danger!</strong> เฉพาะบุคลากรเท่านั้น กรุณาลงชื่อเข้าใช้งานที่ปุ่มด้านบนขวาเพื่อเข้าสู่ระบบ </span>
      </div>
    </div>
  </div>
</div>