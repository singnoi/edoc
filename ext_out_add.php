<?php
$dep_id = $_SESSION['doc_dep_id'];
$dep2_id = $_SESSION['doc_dep2_id'];
$con = connect_db($db);
$con_s = connect();
$dep_name = $_SESSION['doc_dep_name'];
if(isset($_GET['type_id'])) {
    $type_id = $_GET['type_id'];
} else {
    $type_id = 3;
}
$qm = "SELECT
Max(b.book_no) as max_no
FROM
edoc_book AS b
WHERE
b.book_year = $year_now_thai AND
b.section_id = 2 AND b.type_id = $type_id ";

//echo $qm;
$rm = $con->query($qm) or die ($qm);
if($rm->num_rows > 0) {

    $max_no = $rm->fetch_object()->max_no;
    if($max_no == NULL) {
        $qf = "SELECT first_no from edoc_first_no where book_year = '$year_now_thai' and section_id = 2 AND `type_id` = $type_id ";
        $rf = $con->query($qf) or die ($qf);
        if($rf->num_rows > 0) {
            $book_no = $rf->fetch_object()->first_no;
        } else {
            $book_no = 1;
        }
    } else {
        $book_no = $max_no + 1;
    }
    
} else {
    $qf = "SELECT first_no from edoc_first_no where book_year = '$year_now_thai' and section_id = 2 AND `type_id` = $type_id ";
    $rf = $con->query($qf) or die ($qf);
    if($rf->num_rows > 0) {
        $book_no = $rf->fetch_object()->first_no;
    } else {
        $book_no = 1;
    }
    
}
$qd = "SELECT department_code from departments where department_id = '$dep_id'";
$dep_code = $con_s->query($qd)->fetch_object()->department_code;

if(isset($_GET['dep2_id'])) {
    $dep2_id = $_GET['dep2_id'];

    $qd2 = "SELECT d2.dep2_edoc,d2.department_id,d.department_code from department_sub2 as d2 left outer join departments as d on d.department_id = d2.department_id where d2.department_sub2_id = '$dep2_id' ";
    $rd2 = $con_s->query($qd2) or die ($qd2);
    if($rd2->num_rows > 0) {
        $ob2 = $rd2->fetch_object();
        $dep2_code = $ob2->dep2_edoc;
        $dep_id = $ob2->department_id;
        $dep_code = $ob2->department_code;
        if($dep2_code == NULL || $dep2_code == '') {
            $book_code = $hos_code.$dep_code.".".$book_no;
        } else {
            $book_code = $hos_code.$dep_code.".".$dep2_code.".".$book_no;
        }
    } else {
        $book_code = $hos_code.$dep_code.".".$book_no;
    }
    
} else {
    $book_code = $hos_code.$dep_code.".".$book_no;
    $dep2_id = 0;
}
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <h5 class="card-title">ลงทะเบียนหนังสือส่งออกภายนอก (<?php echo $dep_name;?>) ประจำปี <?php echo $year_now_thai;?> &nbsp; * ลำดับที่ &nbsp; <span class="font-weight-bold underline_dot" id="show_no"><?php echo $book_no;?></span>  <span class="small underline_dot float-right">  วันที่ <?php echo date_thai_time($today);?> </span> </h5>

<form id="form_add" enctype="multipart/form-data">

<input type="hidden" name="book_no" value="<?php echo $book_no;?>" >
<input type="hidden" name="dep_id" value="<?php echo $dep_id;?>" >
<input type="hidden" name="book_year" value="<?php echo $year_now_thai;?>" >
<input type="hidden" name="section_id" value="2" >
<input type="hidden" name="reg_datetime" value="<?php echo $today;?>" >

<div class="row">
    <div class="col-lg-6">
        <div class="input-group mb-3" id="add_type">
            <div class="input-group-prepend">
                <span class="input-group-text bg-danger text-white">เลือกประเภท</span>
            </div>
            <select id="type_id" class="form-control" name="type_id" required >

                    <?php
                    $qf = "SELECT * from edoc_type order by `type_id` ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            if($obf->type_id == $type_id) $sl = "selected";
                            else $sl = "";
                            echo "<option value='$obf->type_id' $sl > $obf->type_name </option>";
                        }
                    }
                    ?>
                </select>
        </div>
   
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text bg-danger text-white">เลือกแผนก</span>
            </div>
            <select id="dep2_id" class="form-control" name="dep2_id" required >
            <option value="0"> </option>
                    <?php
                    $qf = "SELECT * from department_sub2 as d2 left outer join departments as d on d.department_id = d2.department_id where d2.department_id < 7 order by d.department_name DESC, d2.department_sub2_name ASC";
                    $rf = $con_s->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            if($obf->department_sub2_id == $dep2_id) $sl = "selected";
                            else $sl = "";
                            echo "<option value='$obf->department_sub2_id' $sl >".$obf->department_sub2_name." ( ".$obf->department_name." )</option>";
                        }
                    }
                    ?>
                </select>
        </div>
   
    </div>
</div>
  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เลขที่หนังสือ</span>
            </div>
            <input type="text" class="form-control" placeholder="ขกxxx" id="book_code" name="book_code" value="<?php echo $book_code;?>" readonly required >
        </div>
   
    </div>
    <div class="col-lg-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">วันที่</span>
            </div>
            <input type="text" class="form-control date" placeholder="" name="book_date" id="book_date" value="<?php echo date_thai_input($today_date);?>" required >
        </div>
    </div>
    <div class="col-lg-5">
        <div class="input-group mb-3" >
            <div class="input-group-prepend">
                <span class="input-group-text">  &nbsp; จาก</span>
            </div>

            <select class="form-control" name="member_id" id="member_id"  required >
                            <option></option>
                            <?php
                            if($dep2_id > 0) {
                                $where_d2 = " and s.department_sub2_id = '$dep2_id'";
                            } else {
                                $where_d2 = "";
                            }
                            $qm = "SELECT * from members as m left outer join department_sub2 as s on s.department_sub2_id = m.department_sub2_id where m.member_status = 'Y' and m.user_other = 'N' and s.department_id = '$dep_id' $where_d2 order by s.department_sub2_name ASC, m.name_only ASC ";
                            $rm = $con_s->query($qm) or die ($qm);
                           
                            while ($obm = $rm->fetch_object()) {
                                if($member_id == $obm->member_id) $sl = "selected";
                                else $sl = "";
                                $occ = "";
                                $qr = "SELECT
                                l.ol_name
                                FROM
                                edoc_operator AS o
                                LEFT OUTER JOIN edoc_operator_level AS l ON o.ol_id = l.ol_id
                                WHERE
                                o.member_id = '$obm->member_id'";
                                $rr = $con->query($qr) or die ($qr);
                                $nr = $rr->num_rows;
                                if($nr > 0) {
                                    $k = 0;
                                    while ($obr = $rr->fetch_object()) {
                                        $k++;
                                        if($k == 1) $occ .= "(";
                                        if($k != 1) $occ .= ",";
                                        $occ .= $obr->ol_name;
                                        if($k == $nr) $occ .= ")";

                                    }
                                }

                                echo "<option value='$obm->member_id' $sl >".$obm->name_only." ".$obm->lname."/".$obm->department_sub2_name.$occ."</option> ";
                            }
                            ?>
                        </select>
          
        </div>
    </div>
    
  </div>

  <div class="row">
    
    <div class="col-lg-8">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เรื่อง</span>
            </div>
            <input type="text" class="form-control" placeholder="" name="book_subject" required >
        </div>
    </div>
    
  </div>

  <div class="row">
  <div class="col-lg-6">
       <div class="input-group mb-3" id="ext_from_1">
            <div class="input-group-prepend">
                <span class="input-group-text"> <a href="#" onclick="plus_from();" > <i class="far fa-plus-square fa-sm"></i></a> &nbsp; เรียน</span>
            </div>

                <select id="ext_from_id" class="form-control" name="ext_from_id" required >
                    <option value="0"></option>
                    <?php
                    $qf = "SELECT * from edoc_ext_from order by ext_from_name ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            echo "<option value='$obf->ext_from_id' > $obf->ext_from_name </option>";
                        }
                    }
                    ?>
                </select>
          
        </div>
   
    </div>
    </div>
  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">ความเร่งด่วน</span>
            </div>
            <select id="speed_id" class="form-control" name="speed_id" required >
                    <?php
                    $qf = "SELECT * from edoc_speed order by speed_id ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            echo "<option value='$obf->speed_id' > $obf->speed_name </option>";
                        }
                    }
                    ?>
                </select>
        </div>
   
    </div>



    </div>

    <div class="row">
   
    <div class="col-lg-6">
        <!-- <input type="file" class="form-control-file border" namd="file" id="file"> -->
        <input type="file" class="form-control-file border" id="file" name="file"  />

    </div>
    <div class="col-lg-6">
        <span class="statusMsg"> 
        
        </span>
    </div>
    <div class="col-lg-12 text-center">
    <hr>
        <button type="submit" class="btn btn-primary submitBtn" id="btnsubmit"> บันทึก</button> &nbsp; 
        
        <a href="?page=ext_out" class="btn btn-success btn-block" id="btnhome" style="display:none;" ><i class="fas fa-home"></i> กลับหน้าหลัก</a>
    </div>
  </div>

</form>                    
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container mt-2">
    <div class="row">

    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-body">
               
                <div id="show_file">

                </div>
            </div>
        </div>
    </div>


    <?php 
$con->close();
$con_s->close();
?>

    </div>
</div>

<script>
$("#dep2_id").change(function(){
    var dep2_id = $('#dep2_id').val();
    var type_id = $('#type_id').val();
    window.location = "?page=ext_out_add&dep2_id="+dep2_id+"&type_id="+type_id;
    //alert("dd");
});

$("#type_id").change(function(){
    var type_id = $('#type_id').val();
    var dep2_id = $('#dep2_id').val();
    window.location = "?page=ext_out_add&dep2_id="+dep2_id+"&type_id="+type_id;
    //alert("dd");
});

function open_book(n,id) {
    var url = "show_file.php?fn="+n;
    $.post("history_read_action.php",{book_id: id},function(info){
        window.open(url, '_blank');
    });
    
}
var ext_from = "<div class='input-group-prepend'><span class='input-group-text'>เรียน</span></div><input type='text' class='form-control' name='ext_from_id' required >";
function plus_from() {
    $('#ext_from_1').html(ext_from);
}

var add_type = "<div class='input-group-prepend'><span class='input-group-text'>ประเภท</span></div><input type='text' class='form-control' name='type_id' required >";
function plus_type() {
    $('#add_type').html(add_type);
}

$('#ext_from_id').change(function(){
    var id = $('#ext_from_id').val();
    if(id==0) {
        $('#ext_from_1').html(ext_from);
    }
});

$("#book_date").inputmask({"mask": "99/99/9999"});


$('.date').datepicker({
        todayBtn: 'linked',
        language: "th-th",
        keyboardNavigation: true,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
        //startDate: '-3d'
});

$('select').select2({
    theme: 'bootstrap4',
});



$("#form_add").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'ext_out_add_action.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#form_add').css("opacity",".5");
            },
            success: function(response){ 
                console.log(response);
                //$('.statusMsg').html(response);
               $('.statusMsg').html('');

                if(response.status == 1){
                    //$('#form_add')[0].reset();
                    $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                    $('#btnsubmit').hide();
                    $('#btnhome').show();
                    $('#show_file').load("show_file.php?fn="+response.file_name);
                    $('#show_no').html(response.book_no);
                    $('#book_code').val(response.book_code);
                }else{
                    $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    $(".submitBtn").removeAttr("disabled");
                }
                $('#form_add').css("opacity","");
                //$(".submitBtn").removeAttr("disabled");

            }
        });
  
});

// File type validation

$("#file").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf'];
    if( !( (fileType == match[0]) ) ){
        alert('ไม่ใช่ไฟล์ PDF');
        $("#file").val('');
        return false;
    }
});

/*
https://www.codexworld.com/ajax-file-upload-with-form-data-jquery-php-mysql/
*/

</script>