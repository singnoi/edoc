<?php
//include("../includes/db_connect.php");
//include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
if(!isset($_GET['book_id'])) {
    exit();
}
$member_id = $_SESSION['member_id'];
$book_id = $_GET['book_id'];
$main_operator = $_SESSION['main_operator'];
$operator = $_SESSION['operator'];
if($book_id == '' || $book_id == NULL) {
    exit();
}
$admin = $_SESSION['member_level'];
$q = "SELECT * ,df.file_name,df1.file_name as file_name1,df2.file_name as file_name2 ,b.book_id, b.section_id 
from edoc_book as b 
left outer join edoc_ext_from as f on f.ext_from_id = b.ext_from_id 
left outer join edoc_ext_to as t on t.ext_to_id = b.ext_to_id 
left outer join edoc_speed as s on s.speed_id = b.speed_id 
left outer join edoc_section as st on st.section_id = b.section_id 
LEFT OUTER JOIN (SELECT min(`file_id`) as max_id, book_id from edoc_file group by book_id) as file_max on (file_max.book_id = b.book_id)
LEFT OUTER JOIN edoc_file as df ON (df.file_id = file_max.max_id)
LEFT OUTER JOIN edoc_file as df1 ON (df1.file_id = b.file_id)
LEFT OUTER JOIN edoc_file as df2 ON (df2.file_id = b.director_file_id)
where b.book_id = '$book_id'";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();
if($ob->section_id  == 1 ) {
  $qm1 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$ob->member_id' ";
  $from_name = $con_s->query($qm1)->fetch_object()->fullname;
  $qm1 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$ob->to_member_id' ";
  $to_name = $con_s->query($qm1)->fetch_object()->fullname;
} else {
  $from_name = $ob->ext_from_name;
  $to_name = $ob->ext_to_name;
}
if($main_operator == 'Y') $link_name = "?page=ext_in";
else $link_name = "?page=main_in&section_id=$ob->section_id";

?>

<div class="container-fluid">

<div class="alert alert-danger">
  <h5>รายละเอียดหนังสือ</h5>

    <div class="row">

        <div class="col-lg-9">
        <div class="card shadow">
            <div class="card-header textshadow bg-dark text-white">
                 <span class="underline_dot font-weight-bold"><?php echo $ob->book_code;?></span>
                &nbsp; ( ประเภทหนังสือ: <span class="underline_dot font-weight-bold"><?php echo book_section($ob->section_id);?></span> )
               <span class="float-right">
               เลขที่รับ: <span class="underline_dot font-weight-bold"><?php echo $ob->ssn;?></span> &nbsp; ปี: <span class="underline_dot font-weight-bold"><?php echo $ob->book_year;?></span> &nbsp;
               <?php 

                                if($ob->member_id == $member_id || $admin=='admin') {
                                    ?>
                                    <a href="?page=ext_in_edit&book_id=<?php echo $ob->book_id;?>" class="text-warning">
                                        <i class="fas fa-edit fa-sm mr-1"></i>
                                    </a>
                                 
                                    <?php 
                                }
                              ?>
               <a href="<?php echo $link_name;?>" class="text-white textshadow"><i class="fas fa-home"></i></a>
               </span>
            </div>
            <div class="card-body">
            <span class="font-weight-bold"> เรื่อง: </span><span class="underline_dot font-weight-bold"><?php echo $ob->book_subject;?></span>
            &nbsp;   
             <?php 
             
             if($main_operator == 'Y' || $admin == 'admin') {
              ?>
              <a href="#" onclick="manage_book('<?php echo $ob->book_id;?>');" data-toggle="modal" data-target="#myModal3" > <?php echo director_accept($ob->director_accept);?></a>
              <?php 
              } else {
                  echo director_accept($ob->director_accept);
              }
             ?>
             <hr> วันที่หนังสือ: <span class="underline_dot font-weight-bold"><?php echo date_thai($ob->book_date);?></span>  &nbsp; / จาก: <span class="underline_dot font-weight-bold"><?php echo $from_name;?></span>  &nbsp; / เรียน: <span class="underline_dot font-weight-bold"><?php echo $to_name;?></span>  &nbsp; / ความเร่งด่วน: <span class="underline_dot font-weight-bold"><?php echo $ob->speed_name;?></span>
             <br><br>
         
             ประเภท: <span class="font-weight-bold"><?php echo book_section($ob->section_id);?></span>
              &nbsp; / วันที่รับหนังสือ: <span class="underline_dot font-weight-bold"><?php echo date_thai_time($ob->reg_datetime);?></span>  &nbsp; / วันที่สิ้นสุดดำเนินการ: <span class="underline_dot font-weight-bold"><?php echo date_thai_time($ob->deadline_date);?></span>
           

            </div>
        </div>
        </div>

        <div class="col-lg-3">
          <div class="card border-0">
            <div class="card-body">
        <button class="btn btn-primary btn-block shadow" onclick="open_book('<?php echo $ob->file_name1;?>','<?php echo $book_id;?>');"> <i class="fas fa-file-pdf"></i> ไฟล์แนบก่อนสั่งการ</button>
              <?php
              if($ob->file_name2 != NULL) {
                ?>
              <button class="btn btn-success btn-block shadow" onclick="open_book('<?php echo $ob->file_name2;?>','<?php echo $book_id;?>');"> <i class="fas fa-file-pdf"></i> ไฟล์แนบหลังสั่งการ</button>
                <?php 
              } else {
                ?>
                <button class="btn btn-secondary btn-block" disabled > <i class="fas fa-file-pdf"></i> ไฟล์แนบหลังสั่งการ</button>
                  <?php 
              }
            $qr = "SELECT count(read_id) as sum_r from edoc_file_read where book_id = '$book_id'";
            $sum_read = $con->query($qr)->fetch_object()->sum_r;
              ?>
              <button onclick="show_read(<?php echo $book_id;?>)" class="btn btn-outline-dark btn-block"><span class="badge badge-dark"> อ่านแล้ว <?php echo comma($sum_read);?></span></button>

              <?php
            if($operator == 5 || $admin == 'admin' || $main_operator == 'Y') {
              if($ob->end_follow == 'N') {
                ?>
              <button class="btn btn-warning btn-block shadow" onclick="end_follow('<?php echo $book_id;?>');"> <i class="fas fa-check-circle"></i> (ผอ.)ยกเลิกการติดตาม</button>
                <?php 
              } else {
                ?>
                <button class="btn btn-success btn-block" disabled > <i class="fas fa-check-circle"></i> สิ้นสุดการติดตามแล้ว</button>
                  <?php 
              }
            }
              ?>
              

          </div>
        </div>

      </div>

    </div>

    <div class="row p-0">

        <div class="col-lg-9 col-md-10">
        <div class="card border-0 ">
            
            <div class="card-body p-0">
           <table class="table table-sm  p-0">
               <tbody>

                   <tr>
                       <td>เลขที่แฟ้ม: <?php echo $ob->fam_no_super;?> </td>
                       <td>หนังสือเข้า รองฯบริหาร </td>
                       <td><?php echo date_thai($ob->in_date_super);?></td>
                       <td>หนังสือออก</td>
                       <td><?php echo date_thai($ob->out_date_super);?></td>
                   </tr>
                   <tr>
                       <th>เลขที่แฟ้ม: <?php echo $ob->fam_no_director;?> </th>
                       <th>หนังสือเข้า ผู้อำนวยการ </th>
                       <th><?php echo date_thai($ob->in_date_director);?></th>
                       <th>หนังสือออก</th>
                       <th><?php echo date_thai($ob->out_date_director);?></th>
                   </tr>
               </tbody>
           </table>
           </div>
          </div>

        </div>
        <div class="col-lg-3 col-md-2">
          <div class="card border-0">           
            <div class="card-body">
              <?php 
              if($main_operator == 'Y' || $admin == 'admin') {
              ?>
              <button class="btn btn-danger btn-sm btn-block" onclick="manage_book('<?php echo $ob->book_id;?>');" data-toggle="modal" data-target="#myModal3"> ยื่น-รับแฟ้ม/แนบไฟล์ </button>
              <?php 
              }
              ?>
            </div>
          </div>
              
        </div>

      </div> <!-- alert -->
      </div>

      <div class="row">

        <div class="col-lg-12">
        <div class="card shadow">
          <div class="card-header textshadow bg-danger text-white">
            ประวัติการนำส่ง
            </div>
            <div class="card-body ">
            
            <div id="show_data">
            
            </div>
            </div>
        </div>
        </div>

    </div>

</div>

<?php
$con->close();
$con_s->close();
?>

<!-- The Modal -->
<div class="modal" id="myModal3">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ดำเนินการ</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md3_body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->

    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ประวัติการเปิดอ่าน</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body p-1" id="md2-body">
        Modal body..
      </div>


    </div>
  </div>
</div>

<script>
var book_id = "<?php echo $book_id;?>";
$('#show_data').load("book_to_data.php?book_id="+book_id);

function show_read(id) {
    $('#md2-body').load("history_read.php?book_id="+id);
    $('#md2').modal('show');
    
}

$('#myModal3').on('hidden.bs.modal', function (e) {
  window.location = "?page=book_manage&book_id="+book_id;
});
function manage_book(id) {
    $('#md3_body').load("book_detail.php?book_id="+id);
}

function end_follow(id) {
    Swal.fire({
        title: 'สิ้นสุดการติดตาม?',
        text: "มั่นใจให้กดยืนยัน!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'green',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน!',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("end_follow.php",{book_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=main_follow";
                } else {
                    alert(info);
                }
            })
        }
    });
}
</script>