<?php
include("../includes/db_connect.php");
$con = connect();
$id = $_GET['id'];
$dep_id = $_GET['dep_id'];
$dep2_id = $_GET['dep2_id'];
?>
<form id="form_member<?php echo $id;?>" class="form-inline">
    <input type="hidden" name="dep_id" value="<?php echo $dep_id;?>">
    <input type="hidden" name="dep2_id" value="<?php echo $dep2_id;?>">

    <div class="input-group">
                        
    <input type="text" class="form-control" name="dep2_edoc" required >
        <div class="input-group-append">
            <button class="btn btn-primary btn-sm" type="submit"><i class="fas fa-save"></i></button>
        </div>
    </div>
    <span id="s<?php echo $id;?>"></span>
</form>
<?php $con->close(); ?>

<script>

var id = "<?php echo $id;?>";
$('#form_member'+id).submit(function(e){
    e.preventDefault();
    $.post("dep_code_action.php",$('#form_member'+id).serialize(),function(info){

        $('#s'+id).html(info);
        if(info == 'ok') {
            window.location = "?page=dep_code";
        } else {
            $('#s'+id).html(info);
        }
    });
});
</script>