<?php
include("../includes/db_connect.php");
$con = connect();
$id = $_GET['id'];
$dep_id = $_GET['dep_id'];
$dep2_id = $_GET['dep2_id'];
$member_id = $_GET['member_id'];

?>
<form id="form_member<?php echo $id;?>">
    <input type="hidden" name="dep_id" value="<?php echo $dep_id;?>">
    <input type="hidden" name="dep2_id" value="<?php echo $dep2_id;?>">
    <input type="hidden" name="action" value="head">
    <div class="input-group">
                        
                        <select class="form-control form-control-sm" style="width: 240px" name="member_id" required >
                            <option value="">เลือกหัวหน้า</option>
                            <?php
                            $qm = "SELECT * from members where member_status = 'Y' and user_other = 'N' order by name_only ASC ";
                            $rm = $con->query($qm) or die ($qm);
                            while ($obm = $rm->fetch_object()) {
                                if($member_id == $obm->member_id) $sl = "selected";
                                else $sl = "";
                                echo "<option value='$obm->member_id' $sl >".$obm->name_only." ".$obm->lname."</option> ";
                            }
                            ?>
                        </select>
        <div class="input-group-append">
            <button class="btn btn-primary btn-sm" type="submit"><i class="fas fa-save"></i></button>
        </div>
    </div>
    <span id="s<?php echo $id;?>"></span>
</form>
<?php $con->close(); ?>

<script>

$('select').select2({
    theme: 'bootstrap4',
});

var id = "<?php echo $id;?>";
$('#form_member'+id).submit(function(e){
    e.preventDefault();
    $.post("dep_select_action.php",$('#form_member'+id).serialize(),function(info){

        $('#s'+id).html(info);
        if(info == 'ok') {
            window.location = "?page=dep";
        } else {
            $('#s'+id).html(info);
        }
    });
});
</script>