<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$status = "";
$member_id = $_SESSION['member_id'];
$main_operator = $_SESSION['main_operator'];
$admin = $_SESSION['member_level'];
$operator = $_SESSION['operator'];
$dep2_id = $_SESSION['doc_dep2_id'];
$dep_id = $_SESSION['doc_dep_id'];

if(isset($_GET['limit'])){
    
    $limit1 = $_GET['limit'];
    $limit = "limit ".$limit1;
    $tb = "tb2";
    $show10 = $limit1." เรื่องล่าสุด";
    
} else {  
    $limit = "";
    $tb = "tb1";
    $show10 = "";
}

if($operator == 1 || $admin == 'admin') {
    $where_owner = " AND b.dep_id = $dep_id ";
} elseif ($operator == 2) {
    $where_owner = " AND b.dep2_id = $dep2_id ";
} else {
    //$where_owner = " AND b.member_id = $member_id ";
    $where_owner = " AND b.dep2_id = $dep2_id ";
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">

            <div class="card border-0 shadow ">
            <div class="card-header bg-danger text-white">
            <a href="?page=main_out_int" class="text-white" >
                <i class="fas fa-book fa-1x text-white shadow mr-2"></i> <span > ทะเบียนหนังสือภายในของฝ่าย <?php echo $show10." ".$_SESSION['doc_dep_name'];?></span> <span class="font-weight-bold textshadow">( หนังสือภายใน )</span>
            </a>
            <?php
            if($operator > 0 || $admin == 'admin') {
                ?>
                <span class="float-right">
                <a href="?page=out_add" class="text-white textshadow">
                <i class="far fa-file"></i> ลงทะเบียนหนังสือใหม่
                </a>
                </span>
            <?php } ?>
            </div>
            <div class="card-body">
            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.reg_datetime,
                    b.book_no,
                    b.director_accept,
                    f.ext_from_name,
                    s.speed_name,
                    df.file_name,
                    b.ssn,
                    b.to_member_id,
                    b.member_id  
                    FROM
                    edoc_book AS b
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    LEFT OUTER JOIN (SELECT max(`file_id`) as max_id, book_id from edoc_file group by book_id) as file_max on (file_max.book_id = b.book_id)
                    LEFT OUTER JOIN edoc_file as df ON (df.file_id = file_max.max_id) 
                    WHERE
                    b.section_id = 1 
                    $where_owner 
                    ORDER BY
                    b.director_accept ASC,
                    b.book_date DESC,
                    b.book_no DESC
                    ";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm" id="<?php echo $tb;?>">
                    <thead>
                        <tr>
                            <th>เลขที่ส่ง</th>
                            <th>เร่งด่วน</th>
                            <th>หนังสือเลขที่</th>
                            <th>วันที่</th>
                            <th>เรื่อง</th>
                            <th>จาก</th>                            
                            <th>ถึง</th>
                            <th class="text-center">สถานะ</th>
                            <th class="text-center">อ่าน</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {

                                $qm1 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$obi->member_id' ";
                                    $from_name_int = $con_s->query($qm1)->fetch_object()->fullname;
                                    if($obi->to_member_id != NULL && $obi->to_member_id != '' ) {
                                        $qm2 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$obi->to_member_id' ";
                                        $to_name_int = $con_s->query($qm2)->fetch_object()->fullname;
                                    } else {
                                        $to_name_int = "";
                                    }

                                echo "<tr>";
                                echo "<td>".$obi->ssn."</td>";
                                echo "<td>".$obi->speed_name."</td>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php echo $obi->book_code; ?>
                                </a>
                                <?php 

                                if($obi->director_accept < 2) {
                                    ?>
                                    <a href="?page=out_edit&book_id=<?php echo $obi->book_id;?>" class="text-warning">
                                        <i class="fas fa-edit fa-sm mr-1"></i>
                                    </a>
                                    <a href="#" onclick="del_book('<?php echo $obi->book_id;?>');" class="text-danger">
                                        <i class="fas fa-trash fa-sm "></i>
                                    </a>
                                    <?php 
                                }
                                echo "</td>";
                                echo "<td>".date_thai_xs($obi->book_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,35,'UTF-8')."..</td>";
                                echo "<td>".$from_name_int."</td>";                               
                               
                                echo "<td>".$to_name_int."</td>";
                                echo "<td class='text-center'>";
                                if($main_operator == 'Y' || $admin == 'admin') {
                                ?>
                                <a href="#" onclick="manage_book('<?php echo $obi->book_id;?>');" data-toggle="modal" data-target="#myModal" > <?php echo director_accept($obi->director_accept);?></a>
                                <?php 
                                } else {
                                    echo director_accept($obi->director_accept);
                                }
                                echo "</td>";
                                echo "<td class='text-center'>";
                                $qr = "SELECT count(to_id) as sum_r from edoc_to where book_id = '$obi->book_id' and member_read = 'Y'";
                                $sum_read = $con->query($qr)->fetch_object()->sum_r;
                                $qrn = "SELECT count(to_id) as sum_n from edoc_to where book_id = '$obi->book_id' and member_read = 'N'";
                                $sum_no = $con->query($qrn)->fetch_object()->sum_n;
                                ?>
                                <a href="#" onclick="show_read(<?php echo $obi->book_id;?>)" class="ml-2">
                                <span class="badge badge-dark"> 
                                <?php 
                                    echo "อ่าน ".comma($sum_read);
                                    echo "/ไม่อ่าน ".comma($sum_no);
                                
                                ?>
                                </span></a>
                                <?php 
                                echo "</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                
                
            </div>
        </div>

        </div>
    </div>
</div>

<?php
$con->close();
$con_s->close();
?>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ดำเนินการ</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->

    </div>
  </div>
</div>


<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ประวัติการเปิดอ่าน</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body p-1" id="md2-body">
        Modal body..
      </div>


    </div>
  </div>
</div>

<script>

function show_read(id) {
    $('#md2-body').load("history_read.php?book_id="+id);
    $('#md2').modal('show');
    
}

$('#myModal').on('hidden.bs.modal', function (e) {
  window.location = "?page=main_out_int";
});


function manage_book(id) {
    $('#md_body').load("book_detail.php?book_id="+id);
}

function del_book(id) {
    Swal.fire({
        title: 'แน่ใจจะลบ?',
        text: "ลบแล้วเอาคืนไม่ได้นะ!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน!',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("ext_in_del.php",{book_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=main_out_int";
                } else {
                    alert(info);
                }
            })
        }
    });
}

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาหัวข้อเรื่อง :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 25,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    "ordering": false,
    //"searching": false,
    //"paging":  false

});

function open_book(n,id) {
    var url = "show_file.php?fn="+n;
    $.post("history_read_action.php",{book_id: id},function(info){
        window.open(url, '_blank');
    });
    
}
</script>