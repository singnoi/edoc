<?php 
session_start();
$member_update = $_SESSION['member_id'];
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$uploadDir = 'book_file/'; 

$file_id = $_POST['file_id'];
$file_name = $_POST['file_name'];

$response = array( 
    'status' => 0, 
    'message' => 'บันทึกไม่สำเร็จ กรุณาตรวจสอบข้อมูลก่อนบันทึกอีกครั้ง.',
    'file_name' => $file_name 
); 


 
// If form is submitted 
if(isset($_POST['book_subject']) || isset($_POST['book_no']) || isset($_POST['file'])){ 
    // Get the submitted form data 

    $book_subject = $_POST['book_subject']; 
    $book_no = $_POST['book_no']; 
    $book_year = $_POST['book_year']; 
    $section_id = $_POST['section_id']; 
    $reg_datetime = $_POST['reg_datetime']; 
    $dep_id = $_POST['dep_id']; 
    $dep2_id = $_POST['dep2_id']; 
    $member_id = $_POST['member_id']; 
    $to_member_id = $_POST['to_member_id']; 
    $book_code = $_POST['book_code']; 
    $book_date = date_thai_db($_POST['book_date']); 
    $speed_id = $_POST['speed_id']; 
    $book_id = $_POST['book_id'];

            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                 
                // File path config 
                $fileName = basename($_FILES["file"]["name"]); 
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 

                $random_no = strtotime($today);

                $newfile = $book_year."_".$book_no."_".$random_no.".".$fileType;
                $targetFilePath = $uploadDir . $newfile; 

                // Allow certain file formats 
                $allowTypes = array('pdf'); 
                if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                        $uploadedFile = $newfile; 
                        $uploadStatus = 1;
                        $response['file_name'] = $uploadedFile;
                    }else{ 
                        $uploadStatus = 0; 
                        $response['message'] = 'ผิดพลาด! ไฟล์ไม่สมบูรณ์.'; 
                    } 
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'ผิดพลาด! ไม่ใช่ไฟล์PDF.'; 
                } 

            } else {
                $uploadStatus = 0; 
                $response['message'] = 'บันทึกสำเร็จ โดยไม่เปลี่ยนไฟล์ครับ';
            } 
             
            if($uploadStatus == 1){ 

                $qu = "SELECT `file_id` from edoc_file where `file_id` = '$file_id'";
                $ru = $con->query($qu) or die ($qu);
                if( $ru->num_rows > 0) {

                    $qm = "UPDATE edoc_file SET `file_name`='$uploadedFile' where `file_id` = '$file_id' ";
                    $con->query($qm) or die ($qm);

                } else {
                    // stamp file
                    $qm = "INSERT INTO edoc_file (book_id,`file_name`) values ('$book_id','$uploadedFile'); ";
                    $con->query($qm) or die ($qm);

                }

                $response['file_name'] = $uploadedFile;

                $response['message'] = 'บันทึกสำเร็จ ';
            }  

    $q = "UPDATE edoc_book SET  book_subject='$book_subject', book_date='$book_date',to_member_id='$to_member_id',speed_id='$speed_id', member_update = '$member_update' where book_id = '$book_id' ";
    $result = $con->query($q);
    if($result) {
        $response['status'] = 1;
        

    }
        
} 
 
// Return response 
echo json_encode($response);