<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$status = "";
$member_id = $_SESSION['member_id'];
$member_name = $_SESSION['person'];
$operator = $_SESSION['operator'];
$to_dep2_id = $_SESSION['doc_dep2_id'];
$to_dep_id = $_SESSION['doc_dep_id'];

?>

    <div class="row ">
        <div class="col-lg-12">

            <div class="card border-0 shadow ">
            <div class="card-header bg-success text-white">
            <i class="fas fa-book fa-1x text-white shadow mr-2"></i>  หนังสือที่ถูกส่งถึง <span class="font-weight-bold textshadow"> <?php echo $member_name;?>  </span> 
  
                <span class="float-right">
                <a href="?page=main_all" class="btn btn-sm btn-warning"> <i class="fas fa-search"></i> ค้นหาหนังสือในระบบทั้งหมด </a>
                </span>
            </div>
            <div class="card-body">
            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.reg_datetime,
                    b.book_no,
                    b.director_accept,
                    b.section_id,
                    f.ext_from_name,
                    s.speed_name,
                    st.section_name,
                    b.ssn,
                    t.read_datetime,
                    t.member_read ,
                    t.to_member_id ,
                    et.ext_to_name ,
                    t.to_datetime ,
                    t.member_id
                    FROM
                    edoc_to as t
                    LEFT OUTER JOIN edoc_book AS b on b.book_id = t.book_id
                    LEFT OUTER JOIN edoc_section AS st ON b.section_id = st.section_id
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    LEFT OUTER JOIN edoc_ext_to AS et ON b.ext_to_id = et.ext_to_id
                    WHERE
                    t.to_member_id = '$member_id'
                     
                    ORDER BY
                    t.member_read ASC,
                    t.to_datetime DESC
                    ";
                    // group by b.book_id
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
            <div class="table-responsive-md">
                <table class="table table-sm" id="tb1">
                    <thead class="thead-dark">
                        <tr>
                            <th>หนังสือเลขที่</th>
                            <th>เร่งด่วน</th>
                            <th>จาก</th>  
                            <th>วันที่ส่งถึง</th>
                            <th>เรื่อง</th>                                                    
                            <!-- <th>ถึง</th> -->
                            <th class="text-center">วันที่รับ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {
                                
                                    $qm1 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$obi->member_id' ";
                                    $from_name = $con_s->query($qm1)->fetch_object()->fullname;
                                    if($obi->to_member_id != NULL && $obi->to_member_id != '' ) {
                                        $qm2 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$obi->to_member_id' ";
                                        $to_name = $con_s->query($qm2)->fetch_object()->fullname;
                                    } else {
                                        $to_name = "";
                                    }
                      

                                echo "<tr>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php echo $obi->book_code; ?>
                                </a>
                                <?php 
                                echo "</td>";
                                echo "<td>".$obi->speed_name."</td>";
                                echo "<td>".$from_name."</td>";
                                echo "<td>".date_thai($obi->to_datetime)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,65,'UTF-8')."..</td>";
                                
                                // echo "<td>".$to_name."</td>";          
                               
                                echo "<td class='text-center'>";
                                    ?>
                                    <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                    <?php 
                                if($obi->member_read == "Y") {
                                   echo "<span class='badge badge-success'>".date_thai_xs_time($obi->read_datetime)."</span>";
                                } else {
                                    echo "<span class='badge badge-warning'>ยังไม่อ่าน</span>";
                                }
                                    ?>
                                    </a>
                                <?php 
                                echo "</td>";
                               
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
            </div>
                
            </div>
        </div>

        </div>
    </div>

<?php
$con->close();
$con_s->close();
?>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ดำเนินการ</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ประวัติการเปิดอ่าน</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body p-1" id="md2-body">
        Modal body..
      </div>


    </div>
  </div>
</div>

<script>
$('#myModal').on('hidden.bs.modal', function (e) {
  window.location = "?page=ext_in";
})


function manage_book(id) {
    $('#md_body').load("book_detail.php?book_id="+id);
}

function del_book(id) {
    Swal.fire({
        title: 'แน่ใจจะลบ?',
        text: "ลบแล้วเอาคืนไม่ได้นะ!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน!',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("ext_in_del.php",{book_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=ext_in";
                } else {
                    alert(info);
                }
            })
        }
    });
}

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ยังไม่มีหนังสือถึงท่าน",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ยังไม่มีหนังสือถึงท่าน",
        "sSearch": "ค้นหาหนังสือที่ถูกส่งถึง <?php echo $member_name;?> :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 25,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    "ordering": false,
    //"searching": false,
    //"paging":  false

});

function open_book(n,id) {
    var url = "show_file.php?fn="+n;
    $.post("history_read_action.php",{book_id: id},function(info){
        window.open(url, '_blank');
    });
    
}

function show_read(id) {
    $('#md2-body').load("history_read.php?book_id="+id);
    $('#md2').modal('show');
    
}
</script>