<?php
//include("../includes/db_connect.php");
//include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
?>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12">

        <div class="card">
            <div class="card-header">
            หนังสือค้างอ่าน
            </div>
            <div class="card-body">
                <table class="table table-striped table-sm" id="tb1">
                    <thead>
                        <tr>
                            <th>ชื่อสกุล</th>
                            <th class="text-right">หนังสือเข้า</th>
                            <th class="text-right">หนังสือค้าง</th>
                            <th class="text-right">ร้อยละค้าง</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    
                    $q = "SELECT
                    Count(t.to_id) AS sum_id,
                    t.to_member_id,
                    (SELECT COUNT(t2.to_id) FROM edoc_to as t2 WHERE t2.member_read = 'N' AND t2.to_member_id = t.to_member_id) AS sum_n,
                    ( ( (SELECT COUNT(t3.to_id) FROM edoc_to as t3 WHERE t3.member_read = 'N' AND t3.to_member_id = t.to_member_id) *100 ) / Count(t.to_id) ) as percen
                    FROM
                    edoc_to AS t
                    GROUP BY
                    t.to_member_id
                    ORDER BY
                    sum_n DESC,
                    sum_id DESC
                    ";
                    $r = $con->query($q) or die ($q);
                    if($r->num_rows > 0) {
                        while ($ob = $r->fetch_object()) {
                            $qm  = "SELECT concat(name_only,' ',lname) as fullname from members as m where member_id = '$ob->to_member_id'";
                            $fullname = $con_s->query($qm)->fetch_object()->fullname;
                            echo "<tr>";
                            echo "<td>".$fullname."</td>";
                            echo "<td class='text-right'>".comma($ob->sum_id)."</td>";
                            echo "<td class='text-right'>".comma($ob->sum_n)."</td>";
                            echo "<td class='text-right'>".comma($ob->percen)."%</td>";
                            echo "</tr>";
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

        </div>
    </div>

</div>

<?php
$con_s->close();
$con->close();
?>

<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายชื่อ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": false,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    "ordering": false,
    //"searching": false,
    //"paging":  false

});

</script>