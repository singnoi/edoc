<?php
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$book_id = $_GET['book_id'];
$q = "SELECT
r.read_datetime,
r.member_id,
o.ol_id,
o.dep_id,
o.dep2_id,
l.ol_name
FROM
edoc_file_read AS r
LEFT OUTER JOIN edoc_operator AS o ON r.o_id = o.o_id
LEFT OUTER JOIN edoc_operator_level AS l ON o.ol_id = l.ol_id
WHERE
r.book_id = '$book_id'
order by r.read_datetime ASC";
$r = $con->query($q) or die ($q);
$n = $r->num_rows;
?>

        <div class="card border-0">
            <div class="card-body p-0">

            <table class="table table-sm table-striped table-bordered" id="tbr">
                <thead class="bg-success text-white">
                    <tr>
                        <th>วันที่-เวลา</th>
                        <th>รายชื่อผู้อ่าน</th>
                        <th>สถานะ</th>
                        <th>กลุ่มภารกิจ</th>
                        <th>หน่วยงาน</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($n > 0) {
                        while ($ob = $r->fetch_object()) {
                            $qm = "SELECT concat(m.name_only,' ',m.lname) as fullname, s2.department_sub2_name, d.department_name from members as m left outer join department_sub2 as s2 on s2.department_sub2_id = m.department_sub2_id left outer join departments as d on d.department_id = s2.department_id where m.member_id = '$ob->member_id' ";
                            $obm = $con_s->query($qm)->fetch_object();

                            if($ob->ol_id != NULL) {
                                $ol_name = $ob->ol_name;
                                $q2 = "SELECT * from departments where department_id = '$ob->dep_id'";
                                $dep_name = $con_s->query($q2)->fetch_object()->department_name;
                                if($ob->dep2_id == 0) {
                                    $dep2_name = "";
                                } else {
                                    $q3 = "SELECT * from department_sub2 where department_sub2_id = '$ob->dep2_id'";
                                    $dep2_name = $con_s->query($q3)->fetch_object()->department_sub2_name;
                                }
                            } else {
                                $ol_name = "บุคลากร";
                                $dep_name = $obm->department_name;
                                $dep2_name = $obm->department_sub2_name;
                            }

                            echo "<tr>";
                            echo "<td>".date_thai_time($ob->read_datetime)."</td>";
                            echo "<td>".$obm->fullname."</td>";
                            echo "<td>".$ol_name."</td>";
                            echo "<td>".$dep_name."</td>";
                            echo "<td>".$dep2_name."</td>";
                            echo "</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>

                
            </div>
        </div>


<?php
$con->close();
$con_s->close();
?>
<script>
$('#tbr').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายชื่อ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 25,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    "ordering": false,
    //"searching": false,
    //"paging":  false

});
</script>