<?php
$con = connect_db($db);
$qm = "SELECT
Max(b.book_no) as max_no
FROM
edoc_book AS b
WHERE
b.book_year = $year_now_thai AND
b.section_id = 3";

$rm = $con->query($qm) or die ($qm);
if($rm->num_rows > 0) {

    $max_no = $rm->fetch_object()->max_no;
    if($max_no == NULL) {
        $qf = "SELECT first_no from edoc_first_no where book_year = '$year_now_thai' and section_id = 3 ";
        $rf = $con->query($qf) or die ($qf);
        if($rf->num_rows > 0) {
            $book_no = $rf->fetch_object()->first_no;
        } else {
            $book_no = 1;
        }
    } else {
        $book_no = $max_no + 1;
    }
    
} else {
    $qf = "SELECT first_no from edoc_first_no where book_year = '$year_now_thai' and section_id = 3 ";
    $rf = $con->query($qf) or die ($qf);
    if($rf->num_rows > 0) {
        $book_no = $rf->fetch_object()->first_no;
    } else {
        $book_no = 1;
    }
    
}
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <h5 class="card-title">ลงทะเบียน  รับหนังสือ ปี <?php echo $year_now_thai;?> &nbsp; * เลขที่รับ: &nbsp; <span class="text20 underline_dot" id="show_no"><?php echo $book_no;?></span>  <span class="small underline_dot float-right">  วันที่ <?php echo date_thai_time($today);?> </span> </h5>

<form id="form_add" enctype="multipart/form-data">

<input type="hidden" name="book_no" value="<?php echo $book_no;?>" >
<input type="hidden" name="book_year" value="<?php echo $year_now_thai;?>" >
<input type="hidden" name="section_id" value="3" >
<input type="hidden" name="reg_datetime" value="<?php echo $today;?>" >
<input type="hidden" name="dep_id" value="<?php echo $dep_id;?>" >
<input type="hidden" name="dep2_id" value="<?php echo $dep2_id;?>" >
<input type="hidden" name="member_id" value="<?php echo $member_id;?>" >

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เลขที่หนังสือ</span>
            </div>
            <input type="text" class="form-control" placeholder="ขกxxx" name="book_code" required >
        </div>
   
    </div>
    <div class="col-lg-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">วันที่</span>
            </div>
            <input type="text" class="form-control date" placeholder="" name="book_date" id="book_date" value="<?php echo date_thai_input($today_date);?>" required >
        </div>
    </div>
    <div class="col-lg-5">
        <div class="input-group mb-3" id="ext_from_1">
            <div class="input-group-prepend">
                <span class="input-group-text"> <a href="#" onclick="plus_from();" > <i class="far fa-plus-square fa-sm"></i></a> &nbsp; จาก</span>
            </div>

                <select id="ext_from_id" class="form-control" name="ext_from_id" required >
                    <option></option>
                    <?php
                    $qf = "SELECT * from edoc_ext_from order by ext_from_name ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            echo "<option value='$obf->ext_from_id' > $obf->ext_from_name </option>";
                        }
                    }
                    ?>
                </select>
          
        </div>
    </div>
    
  </div>

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3" id="ext_to_1">
            <div class="input-group-prepend">
                <span class="input-group-text"> <a href="#" onclick="plus_to();" > <i class="far fa-plus-square fa-sm"></i></a> &nbsp;  เรียน</span>
            </div>

                <select id="ext_to_id" class="form-control" name="ext_to_id" required >
                    <option ></option>
                    <?php
                    $qf = "SELECT * from edoc_ext_to order by ext_to_name ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            echo "<option value='$obf->ext_to_id' > $obf->ext_to_name </option>";
                        }
                    }
                    ?>
                </select>
          
        </div>
   
    </div>
    <div class="col-lg-8">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เรื่อง</span>
            </div>
            <input type="text" class="form-control" placeholder="" name="book_subject" required >
        </div>
    </div>
    
  </div>

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">ความเร่งด่วน</span>
            </div>
            <select id="speed_id" class="form-control" name="speed_id" required >
                    <?php
                    $qf = "SELECT * from edoc_speed order by speed_id ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            echo "<option value='$obf->speed_id' > $obf->speed_name </option>";
                        }
                    }
                    ?>
                </select>
        </div>
   
    </div>
    <div class="col-lg-5">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">วันที่สิ้นสุดดำเนินการ</span>
            </div>
            <input type="text" class="form-control date" placeholder="" name="deadline_date" id="deadline_date" >
        </div>
    </div>
   
    
  </div>

  <div class="row">
    <div class="col-lg-6">
        <!-- <input type="file" class="form-control-file border" namd="file" id="file"> -->
        <input type="file" class="form-control-file border" id="file" name="file" required />

    </div>
    <div class="col-lg-6">
        <span class="statusMsg"> 
        
        </span>
    </div>
    <div class="col-lg-12 text-center">
    <hr>
        <button type="submit" class="btn btn-primary submitBtn" id="btnsubmit"> บันทึก</button> &nbsp; 
        
        <a href="?page=ext_in" class="btn btn-success btn-block" id="btnhome" style="display:none;" ><i class="fas fa-home"></i> กลับหน้าหลัก</a>
    </div>
  </div>

</form>                    
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container mt-2">
    <div class="row">

    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-body">
               
                <div id="show_file">

                </div>
            </div>
        </div>
    </div>


    <?php 

?>

    </div>
</div>

<script>
function open_book(n,id) {
    var url = "show_file.php?fn="+n;
    $.post("history_read_action.php",{book_id: id},function(info){
        window.open(url, '_blank');
    });
    
}
$("#book_date").inputmask({"mask": "99/99/9999"});
$("#deadline_date").inputmask({"mask": "99/99/9999"});

$('.date').datepicker({
        todayBtn: 'linked',
        language: "th-th",
        keyboardNavigation: true,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
        //startDate: '-3d'
});

$('select').select2({
    theme: 'bootstrap4',
});

var ext_from = "<div class='input-group-prepend'><span class='input-group-text'>โดย</span></div><input type='text' class='form-control' name='ext_from_id' required >";
var ext_to = "<div class='input-group-prepend'><span class='input-group-text'>เรียน</span></div><input type='text' class='form-control' name='ext_to_id' required >";

function plus_from() {
    $('#ext_from_1').html(ext_from);
}
$('#ext_from_id').change(function(){
    var id = $('#ext_from_id').val();
    if(id==0) {
        $('#ext_from_1').html(ext_from);
    }
});
function plus_to() {
    $('#ext_to_1').html(ext_to);
}
$('#ext_to_id').change(function(){
    var id = $('#ext_to_id').val();
    if(id==0) {
        $('#ext_to_1').html(ext_to);
    }
});


$("#form_add").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'ext_in_action.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#form_add').css("opacity",".5");
            },
            success: function(response){ 
                console.log(response);
                //$('.statusMsg').html(response);
               $('.statusMsg').html('');

                if(response.status == 1){
                    //$('#form_add')[0].reset();
                    $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                    $('#btnsubmit').hide();
                    $('#btnhome').show();
                    $('#show_file').load("show_file.php?fn="+response.file_name);
                    $('#show_no').html(response.book_no);
                }else{
                    $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    $(".submitBtn").removeAttr("disabled");
                }
                $('#form_add').css("opacity","");
                //$(".submitBtn").removeAttr("disabled");

            }
        });
  
});

// File type validation

$("#file").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf'];
    if( !( (fileType == match[0]) ) ){
        alert('ไม่ใช่ไฟล์ PDF');
        $("#file").val('');
        return false;
    }
});

/*
https://www.codexworld.com/ajax-file-upload-with-form-data-jquery-php-mysql/
*/

</script>