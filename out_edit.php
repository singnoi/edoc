<?php
$con = connect_db($db);
$con_s = connect();
$book_id = $_GET['book_id'];

$ql = "SELECT * from edoc_file where book_id ='$book_id' order by `file_id` ASC LIMIT 1";
$rl = $con->query($ql) or die ($ql);
if($rl->num_rows > 0) {
    $obl = $rl->fetch_object();
    $file_id = $obl->file_id;
    $file_name = $obl->file_name;
} else {
    $file_id = 0;
    $file_name = "";
}



$q = "SELECT * from edoc_book where book_id = '$book_id'";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();
$dep2_id = $ob->dep2_id;
$dep_id = $ob->dep_id;
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <h5 class="card-title">แก้ไขหนังสือส่งภายใน  <span class="small underline_dot float-right">  วันที่ <?php echo date_thai_time($ob->reg_datetime)." เลขที่ส่ง: ".$ob->ssn;?> </span> </h5>

<form id="form_add" enctype="multipart/form-data">
<input type="hidden" name="book_id" value="<?php echo $ob->book_id;?>" >
<input type="hidden" name="book_no" value="<?php echo $ob->book_no;?>" >
<input type="hidden" name="book_year" value="<?php echo $ob->book_year;?>" >
<input type="hidden" name="section_id" value="1" >
<input type="hidden" name="reg_datetime" value="<?php echo $ob->reg_datetime;?>" >
<input type="hidden" name="dep_id" value="<?php echo $ob->dep_id;?>" >
<input type="hidden" name="file_id" value="<?php echo $file_id;?>" >
<input type="hidden" name="file_name" value="<?php echo $file_name;?>" >
<div class="row">
    <div class="col-lg-6">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text bg-danger text-white">เลือกแผนก</span>
            </div>
            <select id="dep2_id" class="form-control" name="dep2_id" readonly required >
            <option value="0"> </option>
                    <?php
                    $qf = "SELECT * from department_sub2 where department_id = '$dep_id' order by department_sub2_name ASC";
                    $rf = $con_s->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            if($obf->department_sub2_id == $ob->dep2_id) $sl = "selected";
                            else $sl = "";
                            echo "<option value='$obf->department_sub2_id' $sl > $obf->department_sub2_name </option>";
                        }
                    }
                    ?>
                </select>
        </div>
   
    </div>
</div>
  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เลขที่หนังสือ</span>
            </div>
            <input type="text" class="form-control" placeholder="ขกxxx" name="book_code" value="<?php echo $ob->book_code;?>" readonly required >
        </div>
   
    </div>
    <div class="col-lg-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">วันที่</span>
            </div>
            <input type="text" class="form-control date" placeholder="" name="book_date" id="book_date" value="<?php echo date_thai_input($ob->book_date);?>" required >
        </div>
    </div>
    <div class="col-lg-5">
        <div class="input-group mb-3" id="ext_from_1">
            <div class="input-group-prepend">
                <span class="input-group-text">  จาก</span>
            </div>

            <select class="form-control" name="member_id" id="member_id"  required >
                            <option></option>
                            <?php
                            if($dep2_id > 0) {
                                $where_d2 = " and s.department_sub2_id = '$dep2_id'";
                            } else {
                                $where_d2 = "";
                            }
                            $qm = "SELECT * from members as m left outer join department_sub2 as s on s.department_sub2_id = m.department_sub2_id where m.member_status = 'Y' and m.user_other = 'N' and s.department_id = '$dep_id' $where_d2 order by s.department_sub2_name ASC, m.name_only ASC ";
                            $rm = $con_s->query($qm) or die ($qm);
                           
                            while ($obm = $rm->fetch_object()) {
                                if($ob->member_id == $obm->member_id) $sl = "selected";
                                else $sl = "";
                                $occ = "";
                                $qr = "SELECT
                                l.ol_name
                                FROM
                                edoc_operator AS o
                                LEFT OUTER JOIN edoc_operator_level AS l ON o.ol_id = l.ol_id
                                WHERE
                                o.member_id = '$obm->member_id'";
                                $rr = $con->query($qr) or die ($qr);
                                $nr = $rr->num_rows;
                                if($nr > 0) {
                                    $k = 0;
                                    while ($obr = $rr->fetch_object()) {
                                        $k++;
                                        if($k == 1) $occ .= "(";
                                        if($k != 1) $occ .= ",";
                                        $occ .= $obr->ol_name;
                                        if($k == $nr) $occ .= ")";

                                    }
                                }

                                echo "<option value='$obm->member_id' $sl >".$obm->name_only." ".$obm->lname."/".$obm->department_sub2_name.$occ."</option> ";
                            }
                            ?>
                        </select>
          
        </div>
    </div>
    
  </div>

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3" id="ext_to_1">
            <div class="input-group-prepend">
                <span class="input-group-text"> เรียน</span>
            </div>

            <select class="form-control" name="to_member_id" id="to_member_id"  required >
                            <option></option>
                            <?php
                          
                            $qm = "SELECT * from members as m left outer join department_sub2 as s on s.department_sub2_id = m.department_sub2_id where m.member_status = 'Y' and m.user_other = 'N' and s.department_id < 7 order by s.department_sub2_name ASC, m.name_only ASC ";
                            $rm = $con_s->query($qm) or die ($qm);
                           
                            while ($obm = $rm->fetch_object()) {
                                if($ob->to_member_id == $obm->member_id) $sl = "selected";
                                else $sl = "";
                                $occ = "";
                                $qr = "SELECT
                                l.ol_name
                                FROM
                                edoc_operator AS o
                                LEFT OUTER JOIN edoc_operator_level AS l ON o.ol_id = l.ol_id
                                WHERE
                                o.member_id = '$obm->member_id'";
                                $rr = $con->query($qr) or die ($qr);
                                $nr = $rr->num_rows;
                                if($nr > 0) {
                                    $k = 0;
                                    while ($obr = $rr->fetch_object()) {
                                        $k++;
                                        if($k == 1) $occ .= "(";
                                        if($k != 1) $occ .= ",";
                                        $occ .= $obr->ol_name;
                                        if($k == $nr) $occ .= ")";

                                    }
                                }

                                echo "<option value='$obm->member_id' $sl >".$obm->name_only." ".$obm->lname."/".$obm->department_sub2_name.$occ."</option> ";
                            }
                            ?>
                        </select>
          
        </div>
   
    </div>
    <div class="col-lg-8">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เรื่อง</span>
            </div>
            <input type="text" class="form-control" name="book_subject" value="<?php echo $ob->book_subject; ?>" required >
        </div>
    </div>
    
  </div>

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">ความเร่งด่วน</span>
            </div>
            <select id="speed_id" class="form-control" name="speed_id" required >
                    <?php
                    $qf = "SELECT * from edoc_speed order by speed_id ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            if($obf->speed_id == $ob->speed_id) $sl = "selected";
                            else $sl = "";
                            echo "<option value='$obf->speed_id' $sl > $obf->speed_name </option>";
                        }
                    }
                    ?>
                </select>
        </div>
   
    </div>

    <div class="col-lg-6">
        <!-- <input type="file" class="form-control-file border" namd="file" id="file"> -->
        <input type="file" class="form-control-file border" id="file" name="file"  />

    </div>
    <div class="col-lg-6">
        <span class="statusMsg">
        <span class="alert alert-danger"> 
        (หากแนบไฟล์ จะแทนที่ไฟล์เดิมด้วยไฟล์ใหม่)
        </span>
        </span>
    </div>
    <div class="col-lg-12 text-center">
    <hr>
        <button type="submit" class="btn btn-primary submitBtn" id="btnsubmit"> บันทึก</button>
        <a href="?page=main_out_int" class="btn btn-success btn-block" id="btnhome" style="display:none;" ><i class="fas fa-home"></i> กลับหน้าหลัก</a>
    </div>
  </div>

</form>                    
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mt-2">
    <div class="row">

    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-body">
                <p class="card-text"><i class="fas fa-file-pdf mr-2"></i><?php echo $file_name; ?></p>
                <div id="show_file">

                </div>
            </div>
        </div>
    </div>


    <?php 

?>

    </div>
</div>




<script>
var file_name = "<?php echo $file_name; ?>";
$('#show_file').load("show_file.php?fn="+file_name);

$("#book_date").inputmask({"mask": "99/99/9999"});

$('.date').datepicker({
        todayBtn: 'linked',
        language: "th-th",
        keyboardNavigation: true,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
        //startDate: '-3d'
});

$('select').select2({
    theme: 'bootstrap4',
});

$("#form_add").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'out_edit_action.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#form_add').css("opacity",".5");
            },
            success: function(response){ 
                console.log(response);
                $('.statusMsg').html(response);
               $('.statusMsg').html('');

                if(response.status == 1){
                    //$('#form_add')[0].reset();
                    $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                    $('#btnsubmit').hide();
                    $('#btnhome').show();
                    $('#show_file').load("show_file.php?fn="+response.file_name);
                }else{
                    $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    $(".submitBtn").removeAttr("disabled");
                }
                $('#form_add').css("opacity","");
                //$(".submitBtn").removeAttr("disabled");

            }
        });
  
});

// File type validation

$("#file").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf'];
    if( !(fileType == match[0]) ){
        alert('ไม่ใช่ไฟล์ PDF.');
        $("#file").val('');
        return false;
    }
});

/*
https://www.codexworld.com/ajax-file-upload-with-form-data-jquery-php-mysql/
*/
</script>