<?php 
session_start();
$member_update = $_SESSION['member_id'];
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$uploadDir = 'book_file/'; 
$response = array( 
    'status' => 0, 
    'message' => 'บันทึกไม่สำเร็จ กรุณาตรวจสอบข้อมูลก่อนบันทึกอีกครั้ง.',
    'file_name' => ''  ,
    'book_no' => '' 
); 


 
// If form is submitted 
if(isset($_POST['book_subject']) || isset($_POST['book_no']) || isset($_POST['file'])){ 
    // Get the submitted form data 

    $book_subject = $_POST['book_subject']; 
    $book_no = $_POST['book_no']; 
    $book_year = $_POST['book_year']; 
    $section_id = $_POST['section_id']; 
    $reg_datetime = $_POST['reg_datetime']; 
    $dep_id = $_POST['dep_id']; 
    $dep2_id = $_POST['dep2_id']; 
    $member_id = $_POST['member_id']; 
    $book_code = $_POST['book_code']; 
    $book_date = date_thai_db($_POST['book_date']); 
    $speed_id = $_POST['speed_id']; 
    $deadline_date = date_thai_db($_POST['deadline_date']); 
    

    $qy = "SELECT book_id from edoc_book where book_code = '$book_code' and book_year = '$book_year'";
    $ry = $con->query($qy) or die ($qy);
    if($ry->num_rows > 0) {
        $response['message'] = "หนังสือเลขที่ {$book_code} ลงทะเบียนไปรอบนึงแล้วครับ";
        echo json_encode($response);
        exit();
    }


// ถ้าเลขซ้ำให้ออกเลขถัดไป เริ่ม -----
$qy = "SELECT book_id from edoc_book where book_no = '$book_no' and book_year = '$book_year' and section_id = '$section_id' ";
$ry = $con->query($qy) or die ($qy);
if($ry->num_rows > 0) {

    $qm = "SELECT
            Max(b.book_no) as max_no
            FROM
            edoc_book AS b
            WHERE
            b.book_year = $book_year AND
            b.section_id = $section_id ";
    $rm = $con->query($qm) or die ($qm);
    $max_no = $rm->fetch_object()->max_no;
    $book_no = $max_no + 1;

}
// ถ้าเลขซ้ำให้ออกเลขถัดไป จบ. ----
$response['book_no'] = $book_no; 

$ssn = substr($book_year,2,2)."-".$section_id."-".$book_no;

    if($deadline_date == NULL || $deadline_date == '' || $deadline_date == '__/__/____') {
        $deadline_date = "NULL";
    } else {
        $deadline_date = "'".$deadline_date."'";
    }

    $ext_from_id = $_POST['ext_from_id']; 
    if( $ext_from_id !== null && !is_numeric( $ext_from_id ) ) {
        $qz2 = "SELECT ext_from_id from edoc_ext_from where ext_from_name = '$ext_from_id'";
        $rz2 = $con->query($qz2) or die ($qz2);
        if($rz2->num_rows > 0) {
            $ext_from_id = $rz2->fetch_object()->ext_from_id;
        } else {
            $qz = "INSERT INTO edoc_ext_from (ext_from_name) values ('$ext_from_id')";
            $con->query($qz) or die ($qz);
            $qz1 = "SELECT ext_from_id from edoc_ext_from where ext_from_name = '$ext_from_id'";
            $ext_from_id = $con->query($qz1)->fetch_object()->ext_from_id;
        }
    }

    $ext_to_id = $_POST['ext_to_id']; 
    if( $ext_to_id !== null && !is_numeric( $ext_to_id ) ) {
        $qz2 = "SELECT ext_to_id from edoc_ext_to where ext_to_name = '$ext_to_id'";
        $rz2 = $con->query($qz2) or die ($qz2);
        if($rz2->num_rows > 0) {
            $ext_to_id = $rz2->fetch_object()->ext_to_id;
        } else {
            $qz = "INSERT INTO edoc_ext_to (ext_to_name) values ('$ext_to_id')";
            $con->query($qz) or die ($qz);
            $qz1 = "SELECT ext_to_id from edoc_ext_to where ext_to_name = '$ext_to_id'";
            $ext_to_id = $con->query($qz1)->fetch_object()->ext_to_id;
        }
    }


             
             
            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                 
                // File path config 
                $fileName = basename($_FILES["file"]["name"]); 
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 

                $random_no = strtotime($today);

                $newfile = $book_year."_".$book_no."_".$random_no.".".$fileType;
                $targetFilePath = $uploadDir . $newfile; 

                // Allow certain file formats 
                $allowTypes = array('pdf'); 
                if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                        $uploadedFile = $newfile; 
                        $uploadStatus = 1;
                        $response['file_name'] = $uploadedFile;
                    }else{ 
                        $uploadStatus = 0; 
                        $response['message'] = 'ผิดพลาด! ไฟล์ไม่สมบูรณ์.'; 
                    } 
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'ผิดพลาด! ไม่ใช่ไฟล์PDF.'; 
                } 
            } else {
                $uploadStatus = 0; 
                $response['message'] = 'ท่านลืมแนบไฟล์ครับ';
            } 
             
            if($uploadStatus == 1){ 

                // Insert form data in the database 
                $q = "INSERT INTO edoc_book (book_code,book_subject,book_date,section_id,book_year,reg_datetime,ext_from_id,ext_to_id,book_no,dep_id,dep2_id,member_id,deadline_date,speed_id,ssn,member_update) VALUES ('$book_code','$book_subject','$book_date','$section_id','$book_year','$reg_datetime','$ext_from_id','$ext_to_id','$book_no','$dep_id','$dep2_id','$member_id',$deadline_date,'$speed_id','$ssn','$member_update')";
                $insert = $con->query($q) ; 
                 
                if($insert){ 
                    $response['status'] = 1; 
                    $response['message'] = 'บันทึกสำเร็จ'; 

                    $qs = "SELECT book_id from edoc_book where ssn = '$ssn'";
                    $book_id = $con->query($qs)->fetch_object()->book_id;

                    // stamp file
                    $qm = "INSERT INTO edoc_file (book_id,`file_name`) values ('$book_id','$uploadedFile'); ";
                    $con->query($qm) or die ($qm);
                    $qf = "SELECT `file_id` from edoc_file where `file_name` = '$uploadedFile'";
                    $file_id = $con->query($qf)->fetch_object()->file_id;

                    $qu = "UPDATE edoc_book set `file_id` = '$file_id' where book_id = '$book_id' ";
                    $con->query($qu) or die ($qu);
                } else {
                    $response['message'] = $q;
                }
            }  
        
} 
$con->close();
$con_s->close();
// Return response 
echo json_encode($response);