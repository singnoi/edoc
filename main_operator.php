<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
?>


<?php if( $_SESSION['main_operator'] == 'Y' || $_SESSION['member_level'] == 'admin') { ?>
    <div class="row mb-5">
        <div class="col-lg-12">

    <div class="card-deck">

        <div class="card border-0 shadow">
            <div class="card-header bg-warning text-white">
                <a href="?page=ext_in" class="text-white">
                <i class="fas fa-cloud-download-alt fa-1x text-white shadow mr-2"></i> <span > ลงทะเบียนรับหนังสือภายนอก 5 เรื่องล่าสุด </span><small  class="font-weight-bold textshadow">(สารบรรณกลาง) </small>
                </a>
                <span class="float-right">
                <a href="?page=ext_in_add" class="text-dark textshadow">
                <i class="far fa-file"></i> ลงทะเบียนหนังสือเข้าใหม่
                </a>
                </span>
            </div>
            <div class="card-body">
            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.director_accept,
                    b.reg_datetime,
                    b.book_no,
                    f.ext_from_name,
                    s.speed_name,
                    df.file_name,
                    b.ssn
                    FROM
                    edoc_book AS b
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    LEFT OUTER JOIN (SELECT max(`file_id`) as max_id, book_id from edoc_file group by book_id) as file_max on (file_max.book_id = b.book_id)
                    LEFT OUTER JOIN edoc_file as df ON (df.file_id = file_max.max_id) 
                    WHERE
                    b.section_id = 3
                    ORDER BY b.director_accept ASC,b.reg_datetime DESC
                    LIMIT 5";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm">
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {
                                echo "<tr>";
                                //echo "<th>".$obi->ssn."</th>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php 
                                echo $obi->book_code." ".director_accept($obi->director_accept);
                                echo "</a></td>";
                                echo "<td>".date_thai_xs($obi->book_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,35,'UTF-8')."..</td>";
                                //echo "<td>".$obi->ext_from_name."</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                <p class="text-right">
                <a href="?page=ext_in" >ดูทั้งหมด...</a>
                </p>
                
            </div>
        </div>

        <div class="card border-0 shadow">
            <div class="card-header bg-danger text-white">
            <a href="?page=ext_out" class="text-white">
                <i class="fas fa-cloud-upload-alt fa-1x text-white shadow mr-2"></i> <span > ทะเบียนส่งหนังสือภายนอก 5 เรื่องล่าสุด </span><small class="font-weight-bold textshadow">(สารบรรณกลาง) </small>
                </a>
                <span class="float-right">
                <a href="?page=ext_out_add" class="text-dark textshadow">
                <i class="far fa-file"></i> ลงทะเบียนส่งหนังสือ
                </a>
                </span>
            </div>
            <div class="card-body">

            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.director_accept,
                    b.reg_datetime,
                    b.book_no,
                    f.ext_from_name,
                    s.speed_name,
                    b.ssn
                    FROM
                    edoc_book AS b
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    WHERE
                    b.section_id = 2
                    ORDER BY b.director_accept ASC,b.reg_datetime DESC
                    LIMIT 5";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm">
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {
                                echo "<tr>";
                                //echo "<th>".$obi->ssn."</th>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php 
                                echo $obi->book_code;
                                echo "</a></td>";
                                echo "<td>".date_thai_xs($obi->book_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,35,'UTF-8')."..</td>";
                                echo "<td>".iconv_substr($obi->ext_from_name,0,22,'UTF-8')."..</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                <p class="text-right">
                <a href="?page=ext_out" >ดูทั้งหมด...</a>
                </p>
                
                
            </div>
        </div>

    </div>

        
        </div> <!-- col -->
    </div> <!-- row -->
<?php } ?>
