<?php
$con = connect_db($db);
$book_id = $_GET['book_id'];

$ql = "SELECT * from edoc_file where book_id ='$book_id' order by `file_id` ASC LIMIT 1";
$rl = $con->query($ql) or die ($ql);
if($rl->num_rows > 0) {
    $obl = $rl->fetch_object();
    $file_id = $obl->file_id;
    $file_name = $obl->file_name;
} else {
    $file_id = 0;
    $file_name = "";
}
$q = "SELECT * from edoc_book where book_id = '$book_id'";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <h5 class="card-title">แก้ไขหนังสือรับภายนอก  <span class="small underline_dot float-right">  วันที่ <?php echo date_thai_time($ob->reg_datetime)." เลขที่รับ: ".$ob->ssn;?> </span> </h5>

<form id="form_add" enctype="multipart/form-data">
<input type="hidden" name="book_id" value="<?php echo $ob->book_id;?>" >
<input type="hidden" name="book_no" value="<?php echo $ob->book_no;?>" >
<input type="hidden" name="book_year" value="<?php echo $ob->book_year;?>" >
<input type="hidden" name="section_id" value="3" >
<input type="hidden" name="reg_datetime" value="<?php echo $ob->reg_datetime;?>" >
<input type="hidden" name="dep_id" value="<?php echo $ob->dep_id;?>" >
<input type="hidden" name="dep2_id" value="<?php echo $ob->dep2_id;?>" >
<input type="hidden" name="member_id" value="<?php echo $ob->member_id;?>" >
<input type="hidden" name="file_id" value="<?php echo $file_id;?>" >
<input type="hidden" name="file_name" value="<?php echo $file_name;?>" >

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เลขที่หนังสือ</span>
            </div>
            <input type="text" class="form-control" placeholder="ขกxxx" name="book_code" value="<?php echo $ob->book_code;?>" required >
        </div>
   
    </div>
    <div class="col-lg-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">วันที่</span>
            </div>
            <input type="text" class="form-control date" placeholder="" name="book_date" id="book_date" value="<?php echo date_thai_input($ob->book_date);?>" required >
        </div>
    </div>
    <div class="col-lg-5">
        <div class="input-group mb-3" id="ext_from_1">
            <div class="input-group-prepend">
                <span class="input-group-text"> <a href="#" onclick="plus_from();" > <i class="far fa-plus-square fa-sm"></i></a> โดย</span>
            </div>

                <select id="ext_from_id" class="form-control" name="ext_from_id" required >
                    <option></option>
                    <?php
                    $qf = "SELECT * from edoc_ext_from order by ext_from_name ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            if($obf->ext_from_id == $ob->ext_from_id) $sl3 = "selected";
                            else $sl3 = "";
                            echo "<option value='$obf->ext_from_id' $sl3 > $obf->ext_from_name </option>";
                        }
                    }
                    ?>
                </select>
          
        </div>
    </div>
    
  </div>

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3" id="ext_to_1">
            <div class="input-group-prepend">
                <span class="input-group-text"> <a href="#" onclick="plus_to();" > <i class="far fa-plus-square fa-sm"></i></a> เรียน</span>
            </div>

                <select id="ext_to_id" class="form-control" name="ext_to_id" required >
                    <option></option>
                    <?php
                    $qf = "SELECT * from edoc_ext_to order by ext_to_name ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            if($obf->ext_to_id == $ob->ext_to_id) $sl2 = "selected";
                            else $sl2 = "";
                            echo "<option value='$obf->ext_to_id' $sl2 > $obf->ext_to_name </option>";
                        }
                    }
                    ?>
                </select>
          
        </div>
   
    </div>
    <div class="col-lg-8">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">เรื่อง</span>
            </div>
            <input type="text" class="form-control" name="book_subject" value="<?php echo $ob->book_subject; ?>" required >
        </div>
    </div>
    
  </div>

  <div class="row">
    <div class="col-lg-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">ความเร่งด่วน</span>
            </div>
            <select id="speed_id" class="form-control" name="speed_id" required >
                    <?php
                    $qf = "SELECT * from edoc_speed order by speed_id ASC";
                    $rf = $con->query($qf) or die ($qf);
                    if( $rf->num_rows > 0 ) {
                        while ($obf = $rf->fetch_object()) {
                            if($obf->speed_id == $ob->speed_id) $sl = "selected";
                            else $sl = "";
                            echo "<option value='$obf->speed_id' $sl > $obf->speed_name </option>";
                        }
                    }
                    ?>
                </select>
        </div>
   
    </div>
    <div class="col-lg-5">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">วันที่สิ้นสุดดำเนินการ</span>
            </div>
            <input type="text" class="form-control date" name="deadline_date" id="deadline_date" value="<?php echo date_thai_input($ob->deadline_date);?>" >
        </div>
    </div>
   
    
  </div>

  <div class="row">
    <div class="col-lg-6">
        <!-- <input type="file" class="form-control-file border" namd="file" id="file"> -->
        <input type="file" class="form-control-file border" id="file" name="file"  />

    </div>
    <div class="col-lg-6">
        <span class="statusMsg">
        <span class="alert alert-danger"> 
        (หากอัพโหลดใหม่จะแทนที่ไฟล์เดิมด้วยไฟล์ใหม่)
        </span>
        </span>
    </div>
    <div class="col-lg-12 text-center">
    <hr>
        <button type="submit" class="btn btn-primary submitBtn" id="btnsubmit"> บันทึก</button>
        <a href="?page=ext_in" class="btn btn-success btn-block" id="btnhome" style="display:none;" ><i class="fas fa-home"></i> กลับหน้าหลัก</a>
    </div>
  </div>

</form>                    
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mt-2">
    <div class="row">

    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-body">
                <p class="card-text"><i class="fas fa-file-pdf mr-2"></i><?php echo $file_name; ?></p>
                <div id="show_file">

                </div>
            </div>
        </div>
    </div>


    <?php 

?>

    </div>
</div>




<script>
var file_name = "<?php echo $file_name; ?>";
$('#show_file').load("show_file.php?fn="+file_name);

$("#book_date").inputmask({"mask": "99/99/9999"});
$("#deadline_date").inputmask({"mask": "99/99/9999"});

$('.date').datepicker({
        todayBtn: 'linked',
        language: "th-th",
        keyboardNavigation: true,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
        //startDate: '-3d'
});

$('select').select2({
    theme: 'bootstrap4',
});

var ext_from = "<div class='input-group-prepend'><span class='input-group-text'>โดย</span></div><input type='text' class='form-control' name='ext_from_id' required >";
var ext_to = "<div class='input-group-prepend'><span class='input-group-text'>เรียน</span></div><input type='text' class='form-control' name='ext_to_id' required >";

function plus_from() {
    $('#ext_from_1').html(ext_from);
}
$('#ext_from_id').change(function(){
    var id = $('#ext_from_id').val();
    if(id==0) {
        $('#ext_from_1').html(ext_from);
    }
});
function plus_to() {
    $('#ext_to_1').html(ext_to);
}
$('#ext_to_id').change(function(){
    var id = $('#ext_to_id').val();
    if(id==0) {
        $('#ext_to_1').html(ext_to);
    }
});


$("#form_add").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'ext_in_edit_action.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#form_add').css("opacity",".5");
            },
            success: function(response){ 
                console.log(response);
                $('.statusMsg').html(response);
               $('.statusMsg').html('');

                if(response.status == 1){
                    //$('#form_add')[0].reset();
                    $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                    $('#btnsubmit').hide();
                    $('#btnhome').show();
                    $('#show_file').load("show_file.php?fn="+response.file_name);
                }else{
                    $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    $(".submitBtn").removeAttr("disabled");
                }
                $('#form_add').css("opacity","");
                //$(".submitBtn").removeAttr("disabled");

            }
        });
  
});

// File type validation

$("#file").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf'];
    if( !(fileType == match[0]) ){
        alert('ไม่ใช่ไฟล์ PDF.');
        $("#file").val('');
        return false;
    }
});

/*
https://www.codexworld.com/ajax-file-upload-with-form-data-jquery-php-mysql/
*/
</script>