<?php 
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$uploadDir = 'book_file/'; 
$response = array( 
    'status' => 0, 
    'message' => 'บันทึกไม่สำเร็จ กรุณาตรวจสอบข้อมูลก่อนบันทึกอีกครั้ง.' 
); 

// If form is submitted 
if( isset($_POST['book_id']) ){ 
    // Get the submitted form data 
    $book_id = $_POST['book_id'];
    $book_no = $_POST['book_no'];
    $book_year = $_POST['book_year'];
    $out_date_director = date_thai_db($_POST['out_date_director']);
    $director_accept = 5 ;

            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                 
                // File path config 
                $fileName = basename($_FILES["file"]["name"]); 
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 

                $random_no = strtotime($today);

                $newfile = $book_year."_".$book_no."_".$random_no.".".$fileType;
                $targetFilePath = $uploadDir . $newfile; 

                // Allow certain file formats 
                $allowTypes = array('pdf'); 
                if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                        $uploadedFile = $newfile; 
                        $uploadStatus = 1;
                    }else{ 
                        $uploadStatus = 0; 
                        $response['message'] = 'ผิดพลาด! ไฟล์ไม่สมบูรณ์.'; 
                    } 
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'ผิดพลาด! ไม่ใช่ไฟล์PDF.'; 
                } 
            } else {
                $uploadStatus = 0; 
                $response['message'] = 'ท่านลืมแนบไฟล์ครับ';
            } 
             
            if($uploadStatus == 1){ 
                // stamp file
                $qm = "INSERT INTO edoc_file (book_id,`file_name`) values ('$book_id','$uploadedFile'); ";
                $con->query($qm) or die ($qm);
                $qf = "SELECT `file_id` from edoc_file where `file_name` = '$uploadedFile'";
                $file_id = $con->query($qf)->fetch_object()->file_id;

                $qu = "UPDATE edoc_book set `director_file_id` = '$file_id', out_date_director = '$out_date_director',director_accept = '$director_accept' where book_id = '$book_id' ";
                $con->query($qu) or die ($qu);

                $response['status'] = 1; 
                $response['message'] = 'บันทึกสำเร็จ'; 
            }  
        
} 
 
// Return response 
echo json_encode($response);