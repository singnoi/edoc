/*
Navicat MySQL Data Transfer

Source Server         : 192.168.45.236_3306
Source Server Version : 50727
Source Host           : 192.168.45.236:3306
Source Database       : office_db

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2020-02-21 09:15:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for edoc_book
-- ----------------------------
DROP TABLE IF EXISTS `edoc_book`;
CREATE TABLE `edoc_book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_code` varchar(30) NOT NULL,
  `book_subject` varchar(255) NOT NULL,
  `book_date` date NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '1',
  `book_year` int(4) NOT NULL DEFAULT '2563',
  `reg_datetime` datetime DEFAULT NULL,
  `ext_from_id` int(11) DEFAULT NULL,
  `ext_to_id` int(11) DEFAULT NULL,
  `book_no` int(11) NOT NULL DEFAULT '0',
  `dep_id` int(11) DEFAULT NULL,
  `dep2_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `to_member_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '3',
  `speed_id` int(11) NOT NULL DEFAULT '1',
  `director_accept` int(11) NOT NULL DEFAULT '0',
  `member_update` int(11) DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_follow` enum('N','Y') NOT NULL DEFAULT 'N',
  `fam_no_super` int(11) DEFAULT NULL,
  `in_date_super` date DEFAULT NULL,
  `out_date_super` date DEFAULT NULL,
  `fam_no_director` int(11) DEFAULT NULL,
  `in_date_director` date DEFAULT NULL,
  `out_date_director` date DEFAULT NULL,
  `ssn` varchar(30) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `director_file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_book
-- ----------------------------
INSERT INTO `edoc_book` VALUES ('1', 'อว 660301.5.1.4.1/ว4952 ', 'ขอเชิญชวนสมัครเข้าเข้าร่วมโครงการประชุมเชิงปฏิบัติการฯ', '2020-02-14', '3', '2563', '2020-02-14 09:40:43', '3', '2', '771', '5', '52', '576', null, null, '3', '1', '5', '576', '2020-02-14 09:43:23', 'N', '1', '2020-02-14', '2020-02-14', '1', '2020-02-14', '2020-02-14', '63-3-771', '1', '2');
INSERT INTO `edoc_book` VALUES ('2', 'ขก 0024/001605 ', 'ขอทราบผลการฟื้นฟูสมรรถภาพผู้ติดยาเสพติด นายนิธิกุล แก้วงาม', '2020-02-14', '3', '2563', '2020-02-14 10:53:28', '4', '2', '772', '5', '52', '576', null, null, '3', '1', '5', '576', '2020-02-14 10:55:23', 'N', '1', '2020-02-14', '2020-02-14', '1', '2020-02-14', '2020-02-14', '63-3-772', '4', '5');
INSERT INTO `edoc_book` VALUES ('3', '0132.112/99 ', 'รับรองการมีสิทธิรับเงินค่ารักษาพยาบาล นางทองผลัด ดอนบันเทา', '2020-02-14', '3', '2563', '2020-02-14 10:56:17', '5', '2', '773', '5', '52', '576', null, null, '3', '1', '5', '122', '2020-02-20 10:54:26', 'N', '1', '2020-02-14', '2020-02-14', '1', '2020-02-14', '2020-02-14', '63-3-773', '6', '7');
INSERT INTO `edoc_book` VALUES ('4', 'ขก 0032.003/ว374', 'แนวทางการใช้งานโปรแกรมประเมินระดับการพัฒนาหมู่บ้านปรับเปลี่ยนฯ/โรงเรียนสุขบัญญัติแห่งชาติและแบบประเมินความรอบรู้ด้านสุขภาพและพฤติกรรมสุขภาพ ปี 2563', '2020-02-17', '3', '2563', '2020-02-17 09:57:50', '6', '2', '774', '5', '52', '576', null, null, '3', '1', '5', '576', '2020-02-17 10:00:35', 'N', '1', '2020-02-17', '2020-02-17', '1', '2020-02-17', '2020-02-17', '63-3-774', '8', '9');
INSERT INTO `edoc_book` VALUES ('5', 'ขก 0032.005/ว414', 'ขอให้ส่งข้อมูลการให้บริการ', '2020-02-18', '3', '2563', '2020-02-18 10:32:17', '1', '2', '775', '5', '52', '576', '2020-02-18', null, '3', '1', '5', '576', '2020-02-20 11:56:16', 'Y', '1', '2020-02-18', '2020-02-18', '1', '2020-02-18', '2020-02-18', '63-3-775', '10', '11');
INSERT INTO `edoc_book` VALUES ('6', 'ขก 0032.010/791', 'การชำระค่าใช้จ่ายการผลิตยาแผนโบราณ', '2020-02-19', '3', '2563', '2020-02-19 14:45:39', '6', '2', '776', '5', '52', '576', null, null, '3', '1', '5', '576', '2020-02-19 14:46:55', 'N', '1', '2020-02-19', '2020-02-19', '1', '2020-02-12', '2020-02-19', '63-3-776', '12', '13');
INSERT INTO `edoc_book` VALUES ('7', 'สธ 0217/ว1855 ', 'ขอเชิญเข้าร่วมประชุม สร้างสุขที่ปลายทาง ครั้งที่ 3', '2020-01-29', '3', '2563', '2020-02-19 16:31:55', '7', '2', '777', '5', '52', '576', null, null, '3', '1', '5', '576', '2020-02-19 16:34:27', 'N', '1', '2020-02-19', '2020-02-19', '1', '2020-02-19', '2020-02-19', '63-3-777', '14', '15');
INSERT INTO `edoc_book` VALUES ('8', 'ขก.032.201.1.124', 'ขอส่งรายงานการเงิน', '2020-02-20', '2', '2563', '2020-02-20 11:49:54', '6', null, '124', '5', '77', '200', null, null, '3', '1', '0', '248', '2020-02-20 11:51:56', 'N', null, null, null, null, null, null, '63-2-124', null, null);
INSERT INTO `edoc_book` VALUES ('9', 'ขห5667/8767', 'ทดสอบ', '2020-02-20', '3', '2563', '2020-02-20 14:10:01', '6', '2', '778', '4', '35', '122', '2020-03-07', null, '3', '1', '5', '122', '2020-02-20 14:14:28', 'N', '1', '2020-02-20', '2020-02-20', '1', '2020-02-20', '2020-02-20', '63-3-778', '16', '17');

-- ----------------------------
-- Table structure for edoc_ext_from
-- ----------------------------
DROP TABLE IF EXISTS `edoc_ext_from`;
CREATE TABLE `edoc_ext_from` (
  `ext_from_id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_from_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ext_from_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_ext_from
-- ----------------------------
INSERT INTO `edoc_ext_from` VALUES ('1', 'สำนักงานสาธารณสุขจังหวัดขอนแก่น');
INSERT INTO `edoc_ext_from` VALUES ('2', 'รพ.สต.บ้านดง');
INSERT INTO `edoc_ext_from` VALUES ('3', 'มหาวิทยาลัย ขอนแก่น');
INSERT INTO `edoc_ext_from` VALUES ('4', 'สำนักงานคุมประพฤติจังหวัดขอนแก่น');
INSERT INTO `edoc_ext_from` VALUES ('5', 'รพ.สต.ท่าพระ');
INSERT INTO `edoc_ext_from` VALUES ('6', 'สำนักงาน สาธารณสุขจังหวัดขอนแก่น');
INSERT INTO `edoc_ext_from` VALUES ('7', 'สำนักงานปลัดกระทรวงสาธารณสุข');

-- ----------------------------
-- Table structure for edoc_ext_to
-- ----------------------------
DROP TABLE IF EXISTS `edoc_ext_to`;
CREATE TABLE `edoc_ext_to` (
  `ext_to_id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_to_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ext_to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_ext_to
-- ----------------------------
INSERT INTO `edoc_ext_to` VALUES ('1', 'ผู้อำนวยการ');
INSERT INTO `edoc_ext_to` VALUES ('2', 'ผู้อำนวยการโรงพยาบาลสิรินธร จังหวัดขอนแก่น');

-- ----------------------------
-- Table structure for edoc_file
-- ----------------------------
DROP TABLE IF EXISTS `edoc_file`;
CREATE TABLE `edoc_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_file
-- ----------------------------
INSERT INTO `edoc_file` VALUES ('1', '1', '2563_771_1581648120.pdf', '2020-02-14 09:42:00');
INSERT INTO `edoc_file` VALUES ('2', '1', '2563_771_1581648203.pdf', '2020-02-14 09:43:23');
INSERT INTO `edoc_file` VALUES ('3', '1', '2563_771_1581648291.pdf', '2020-02-14 09:44:51');
INSERT INTO `edoc_file` VALUES ('4', '2', '2563_772_1581652481.pdf', '2020-02-14 10:54:41');
INSERT INTO `edoc_file` VALUES ('5', '2', '2563_772_1581652523.pdf', '2020-02-14 10:55:23');
INSERT INTO `edoc_file` VALUES ('6', '3', '2563_773_1581652653.pdf', '2020-02-14 10:57:34');
INSERT INTO `edoc_file` VALUES ('7', '3', '2563_773_1581652688.pdf', '2020-02-14 10:58:08');
INSERT INTO `edoc_file` VALUES ('8', '4', '2563_774_1581908375.pdf', '2020-02-17 09:59:35');
INSERT INTO `edoc_file` VALUES ('9', '4', '2563_774_1581908435.pdf', '2020-02-17 10:00:35');
INSERT INTO `edoc_file` VALUES ('10', '5', '2563_775_1581996832.pdf', '2020-02-18 10:33:52');
INSERT INTO `edoc_file` VALUES ('11', '5', '2563_775_1581996903.pdf', '2020-02-18 10:35:03');
INSERT INTO `edoc_file` VALUES ('12', '6', '2563_776_1582098384.pdf', '2020-02-19 14:46:24');
INSERT INTO `edoc_file` VALUES ('13', '6', '2563_776_1582098416.pdf', '2020-02-19 14:46:55');
INSERT INTO `edoc_file` VALUES ('14', '7', '2563_777_1582104831.pdf', '2020-02-19 16:33:51');
INSERT INTO `edoc_file` VALUES ('15', '7', '2563_777_1582104868.pdf', '2020-02-19 16:34:27');
INSERT INTO `edoc_file` VALUES ('16', '9', '2563_778_1582182730.pdf', '2020-02-20 14:12:10');
INSERT INTO `edoc_file` VALUES ('17', '9', '2563_778_1582182868.pdf', '2020-02-20 14:14:28');
INSERT INTO `edoc_file` VALUES ('18', '9', '2563_778_1582183449.jpg', '2020-02-20 14:24:10');
INSERT INTO `edoc_file` VALUES ('19', '3', '2563_773_1582184248.jpg', '2020-02-20 14:37:29');
INSERT INTO `edoc_file` VALUES ('20', '9', '2563_778_1582184749.pdf', '2020-02-20 14:45:50');

-- ----------------------------
-- Table structure for edoc_file_read
-- ----------------------------
DROP TABLE IF EXISTS `edoc_file_read`;
CREATE TABLE `edoc_file_read` (
  `read_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `o_id` int(11) NOT NULL,
  `read_datetime` datetime NOT NULL,
  PRIMARY KEY (`read_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_file_read
-- ----------------------------
INSERT INTO `edoc_file_read` VALUES ('1', '1', '122', '0', '2020-02-15 15:30:14');
INSERT INTO `edoc_file_read` VALUES ('2', '5', '21', '0', '2020-02-18 10:36:08');
INSERT INTO `edoc_file_read` VALUES ('3', '4', '21', '0', '2020-02-18 10:37:07');
INSERT INTO `edoc_file_read` VALUES ('4', '5', '231', '36', '2020-02-18 10:51:03');
INSERT INTO `edoc_file_read` VALUES ('5', '3', '122', '0', '2020-02-20 09:34:49');
INSERT INTO `edoc_file_read` VALUES ('6', '9', '226', '44', '2020-02-20 14:18:32');
INSERT INTO `edoc_file_read` VALUES ('7', '9', '122', '0', '2020-02-20 14:24:29');
INSERT INTO `edoc_file_read` VALUES ('8', '3', '153', '7', '2020-02-20 14:33:50');
INSERT INTO `edoc_file_read` VALUES ('9', '7', '204', '0', '2020-02-20 14:35:18');
INSERT INTO `edoc_file_read` VALUES ('10', '3', '215', '0', '2020-02-20 14:35:39');
INSERT INTO `edoc_file_read` VALUES ('11', '3', '142', '50', '2020-02-20 14:38:05');
INSERT INTO `edoc_file_read` VALUES ('12', '9', '723', '87', '2020-02-20 14:38:13');
INSERT INTO `edoc_file_read` VALUES ('13', '3', '226', '44', '2020-02-20 14:38:18');
INSERT INTO `edoc_file_read` VALUES ('14', '9', '126', '89', '2020-02-20 14:39:52');
INSERT INTO `edoc_file_read` VALUES ('15', '7', '723', '87', '2020-02-20 14:40:47');
INSERT INTO `edoc_file_read` VALUES ('16', '9', '240', '0', '2020-02-20 14:42:39');
INSERT INTO `edoc_file_read` VALUES ('17', '7', '243', '34', '2020-02-20 14:43:06');
INSERT INTO `edoc_file_read` VALUES ('18', '3', '240', '0', '2020-02-20 14:45:53');
INSERT INTO `edoc_file_read` VALUES ('19', '9', '729', '0', '2020-02-20 14:48:13');
INSERT INTO `edoc_file_read` VALUES ('20', '7', '231', '36', '2020-02-20 15:07:50');
INSERT INTO `edoc_file_read` VALUES ('21', '9', '115', '0', '2020-02-20 15:20:25');

-- ----------------------------
-- Table structure for edoc_first_no
-- ----------------------------
DROP TABLE IF EXISTS `edoc_first_no`;
CREATE TABLE `edoc_first_no` (
  `book_year` int(11) NOT NULL,
  `first_no` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_first_no
-- ----------------------------
INSERT INTO `edoc_first_no` VALUES ('2563', '40', '1');
INSERT INTO `edoc_first_no` VALUES ('2563', '771', '3');
INSERT INTO `edoc_first_no` VALUES ('2563', '124', '2');

-- ----------------------------
-- Table structure for edoc_operator
-- ----------------------------
DROP TABLE IF EXISTS `edoc_operator`;
CREATE TABLE `edoc_operator` (
  `o_id` int(11) NOT NULL AUTO_INCREMENT,
  `ol_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `dep2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`o_id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_operator
-- ----------------------------
INSERT INTO `edoc_operator` VALUES ('3', '4', '253', '1', '2');
INSERT INTO `edoc_operator` VALUES ('5', '3', '230', '1', '0');
INSERT INTO `edoc_operator` VALUES ('6', '4', '228', '1', '1');
INSERT INTO `edoc_operator` VALUES ('7', '4', '153', '1', '93');
INSERT INTO `edoc_operator` VALUES ('8', '4', '248', '1', '5');
INSERT INTO `edoc_operator` VALUES ('9', '4', '134', '1', '7');
INSERT INTO `edoc_operator` VALUES ('10', '4', '134', '1', '91');
INSERT INTO `edoc_operator` VALUES ('11', '4', '230', '1', '8');
INSERT INTO `edoc_operator` VALUES ('12', '4', '185', '1', '9');
INSERT INTO `edoc_operator` VALUES ('13', '4', '135', '1', '97');
INSERT INTO `edoc_operator` VALUES ('14', '4', '217', '1', '89');
INSERT INTO `edoc_operator` VALUES ('15', '4', '228', '1', '14');
INSERT INTO `edoc_operator` VALUES ('16', '4', '233', '1', '11');
INSERT INTO `edoc_operator` VALUES ('17', '4', '190', '1', '12');
INSERT INTO `edoc_operator` VALUES ('18', '4', '134', '1', '15');
INSERT INTO `edoc_operator` VALUES ('19', '4', '238', '1', '6');
INSERT INTO `edoc_operator` VALUES ('20', '3', '180', '2', '0');
INSERT INTO `edoc_operator` VALUES ('21', '4', '175', '2', '16');
INSERT INTO `edoc_operator` VALUES ('22', '4', '158', '2', '17');
INSERT INTO `edoc_operator` VALUES ('23', '4', '197', '2', '18');
INSERT INTO `edoc_operator` VALUES ('24', '4', '135', '2', '10');
INSERT INTO `edoc_operator` VALUES ('25', '4', '208', '2', '19');
INSERT INTO `edoc_operator` VALUES ('26', '4', '225', '2', '20');
INSERT INTO `edoc_operator` VALUES ('27', '4', '131', '2', '23');
INSERT INTO `edoc_operator` VALUES ('28', '4', '135', '2', '99');
INSERT INTO `edoc_operator` VALUES ('29', '4', '180', '2', '25');
INSERT INTO `edoc_operator` VALUES ('30', '4', '197', '2', '26');
INSERT INTO `edoc_operator` VALUES ('31', '4', '260', '2', '54');
INSERT INTO `edoc_operator` VALUES ('32', '4', '208', '2', '56');
INSERT INTO `edoc_operator` VALUES ('33', '4', '244', '2', '27');
INSERT INTO `edoc_operator` VALUES ('34', '4', '243', '2', '22');
INSERT INTO `edoc_operator` VALUES ('35', '4', '141', '2', '48');
INSERT INTO `edoc_operator` VALUES ('36', '3', '231', '3', '0');
INSERT INTO `edoc_operator` VALUES ('37', '4', '234', '3', '71');
INSERT INTO `edoc_operator` VALUES ('38', '4', '231', '3', '29');
INSERT INTO `edoc_operator` VALUES ('39', '4', '229', '3', '28');
INSERT INTO `edoc_operator` VALUES ('40', '4', '35', '3', '32');
INSERT INTO `edoc_operator` VALUES ('41', '4', '139', '3', '33');
INSERT INTO `edoc_operator` VALUES ('42', '4', '231', '3', '31');
INSERT INTO `edoc_operator` VALUES ('43', '5', '653', '6', '0');
INSERT INTO `edoc_operator` VALUES ('44', '3', '226', '4', '0');
INSERT INTO `edoc_operator` VALUES ('45', '4', '172', '4', '34');
INSERT INTO `edoc_operator` VALUES ('46', '4', '128', '4', '36');
INSERT INTO `edoc_operator` VALUES ('47', '4', '226', '4', '37');
INSERT INTO `edoc_operator` VALUES ('48', '4', '319', '4', '96');
INSERT INTO `edoc_operator` VALUES ('49', '4', '226', '4', '38');
INSERT INTO `edoc_operator` VALUES ('50', '4', '142', '4', '50');
INSERT INTO `edoc_operator` VALUES ('51', '4', '549', '4', '35');
INSERT INTO `edoc_operator` VALUES ('52', '4', '206', '4', '39');
INSERT INTO `edoc_operator` VALUES ('53', '4', '138', '4', '40');
INSERT INTO `edoc_operator` VALUES ('54', '3', '216', '5', '0');
INSERT INTO `edoc_operator` VALUES ('55', '4', '216', '5', '77');
INSERT INTO `edoc_operator` VALUES ('56', '4', '257', '5', '41');
INSERT INTO `edoc_operator` VALUES ('57', '4', '246', '5', '47');
INSERT INTO `edoc_operator` VALUES ('58', '1', '576', '5', '0');
INSERT INTO `edoc_operator` VALUES ('59', '1', '248', '5', '0');
INSERT INTO `edoc_operator` VALUES ('60', '1', '579', '5', '0');
INSERT INTO `edoc_operator` VALUES ('61', '4', '135', '2', '98');
INSERT INTO `edoc_operator` VALUES ('62', '4', '134', '1', '4');
INSERT INTO `edoc_operator` VALUES ('63', '4', '134', '1', '73');
INSERT INTO `edoc_operator` VALUES ('64', '4', '248', '5', '46');
INSERT INTO `edoc_operator` VALUES ('65', '4', '178', '5', '42');
INSERT INTO `edoc_operator` VALUES ('66', '4', '248', '5', '90');
INSERT INTO `edoc_operator` VALUES ('67', '4', '93', '5', '53');
INSERT INTO `edoc_operator` VALUES ('68', '4', '28', '5', '49');
INSERT INTO `edoc_operator` VALUES ('69', '4', '629', '5', '45');
INSERT INTO `edoc_operator` VALUES ('70', '4', '93', '5', '44');
INSERT INTO `edoc_operator` VALUES ('71', '4', '134', '2', '57');
INSERT INTO `edoc_operator` VALUES ('72', '4', '191', '1', '13');
INSERT INTO `edoc_operator` VALUES ('73', '4', '182', '2', '21');
INSERT INTO `edoc_operator` VALUES ('74', '4', '524', '2', '95');
INSERT INTO `edoc_operator` VALUES ('75', '4', '631', '2', '51');
INSERT INTO `edoc_operator` VALUES ('76', '4', '231', '3', '24');
INSERT INTO `edoc_operator` VALUES ('77', '4', '377', '3', '30');
INSERT INTO `edoc_operator` VALUES ('78', '4', '208', '2', '74');
INSERT INTO `edoc_operator` VALUES ('79', '4', '653', '6', '55');
INSERT INTO `edoc_operator` VALUES ('80', '4', '740', '5', '43');
INSERT INTO `edoc_operator` VALUES ('81', '4', '697', '2', '3');
INSERT INTO `edoc_operator` VALUES ('82', '4', '248', '5', '52');
INSERT INTO `edoc_operator` VALUES ('83', '4', '248', '5', '94');
INSERT INTO `edoc_operator` VALUES ('86', '1', '172', '4', '0');
INSERT INTO `edoc_operator` VALUES ('87', '1', '723', '1', '0');
INSERT INTO `edoc_operator` VALUES ('88', '1', '21', '3', '0');
INSERT INTO `edoc_operator` VALUES ('89', '1', '126', '6', '0');

-- ----------------------------
-- Table structure for edoc_operator_level
-- ----------------------------
DROP TABLE IF EXISTS `edoc_operator_level`;
CREATE TABLE `edoc_operator_level` (
  `ol_id` int(11) NOT NULL,
  `ol_name` varchar(200) DEFAULT '',
  PRIMARY KEY (`ol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_operator_level
-- ----------------------------
INSERT INTO `edoc_operator_level` VALUES ('1', 'สารบรรณกลุ่มภาระกิจ');
INSERT INTO `edoc_operator_level` VALUES ('2', 'สารบรรณหน่วยงาน');
INSERT INTO `edoc_operator_level` VALUES ('3', 'หัวหน้ากลุ่มภาระกิจ');
INSERT INTO `edoc_operator_level` VALUES ('4', 'หัวหน้ากลุ่มงาน');
INSERT INTO `edoc_operator_level` VALUES ('5', 'ผู้อำนวยการ');

-- ----------------------------
-- Table structure for edoc_out_ext
-- ----------------------------
DROP TABLE IF EXISTS `edoc_out_ext`;
CREATE TABLE `edoc_out_ext` (
  `out_ext_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_year` int(4) DEFAULT NULL,
  `out_no` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `out_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`out_ext_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_out_ext
-- ----------------------------

-- ----------------------------
-- Table structure for edoc_section
-- ----------------------------
DROP TABLE IF EXISTS `edoc_section`;
CREATE TABLE `edoc_section` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(200) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_section
-- ----------------------------
INSERT INTO `edoc_section` VALUES ('1', 'หนังสือภายใน');
INSERT INTO `edoc_section` VALUES ('2', 'หนังสือส่งออกภายนอก');
INSERT INTO `edoc_section` VALUES ('3', 'หนังสือรับภายนอก');

-- ----------------------------
-- Table structure for edoc_speed
-- ----------------------------
DROP TABLE IF EXISTS `edoc_speed`;
CREATE TABLE `edoc_speed` (
  `speed_id` int(11) NOT NULL AUTO_INCREMENT,
  `speed_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`speed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_speed
-- ----------------------------
INSERT INTO `edoc_speed` VALUES ('1', 'ปรกติ');
INSERT INTO `edoc_speed` VALUES ('2', 'ด่วน');
INSERT INTO `edoc_speed` VALUES ('3', 'ด่วนมาก');
INSERT INTO `edoc_speed` VALUES ('4', 'ด่วนที่สุด');

-- ----------------------------
-- Table structure for edoc_to
-- ----------------------------
DROP TABLE IF EXISTS `edoc_to`;
CREATE TABLE `edoc_to` (
  `to_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `dep2_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `ol_id` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `to_datetime` datetime DEFAULT NULL,
  `to_dep_id` int(11) DEFAULT NULL,
  `to_dep2_id` int(11) DEFAULT NULL,
  `to_member_id` int(11) DEFAULT NULL,
  `to_ol_id` int(11) NOT NULL DEFAULT '0',
  `file_id` int(11) DEFAULT NULL,
  `member_read` enum('N','Y') NOT NULL DEFAULT 'N',
  `read_datetime` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alert_status` enum('N','Y') NOT NULL DEFAULT 'Y',
  `operator_read` int(11) DEFAULT NULL,
  `operator_read_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_to
-- ----------------------------
INSERT INTO `edoc_to` VALUES ('1', '1', '5', '0', '576', '1', 'แจ้งกลุ่มพยาบาลประชาสัมพันธ์', '2020-02-14 09:44:51', '1', '0', '723', '1', '3', 'N', null, '2020-02-14 09:44:51', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('2', '2', '5', '0', '576', '1', 'แจ้งกลุ่มงานจิตและยาเสพติต', '2020-02-14 10:55:58', '1', '0', '723', '1', null, 'N', null, '2020-02-14 10:55:58', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('3', '3', '5', '0', '576', '1', 'แจ้งกลุ่มการพยาบาลเพื่อทราบและดำเนินการต่อไป', '2020-02-14 10:58:39', '1', '0', '723', '1', null, 'N', null, '2020-02-14 10:58:39', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('4', '4', '5', '0', '576', '1', 'แจ้งกลุ่มภารกิจปฐมภูมิเพื่อทราบและดำเนินการต่อไป', '2020-02-17 10:02:35', '3', '24', '21', '0', null, 'Y', '2020-02-18 10:37:07', '2020-02-18 10:37:07', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('5', '5', '5', '0', '576', '1', 'เวชกรรมดำเนินการ', '2020-02-18 10:34:22', '3', '24', '21', '0', null, 'Y', '2020-02-18 10:36:08', '2020-02-18 10:36:08', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('6', '4', '3', '24', '21', '0', 'เพื่อพิจารณาดำเนินการต่อไป', '2020-02-18 10:38:54', '3', '24', '247', '0', null, 'N', null, '2020-02-18 10:38:54', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('7', '4', '3', '24', '21', '0', 'เพื่อโปรดพิจารณา', '2020-02-18 10:48:43', '3', '0', '231', '3', null, 'N', null, '2020-02-18 10:48:43', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('8', '5', '3', '0', '231', '3', 'ดำเนิการแล้ว', '2020-02-18 10:52:49', '6', '0', '653', '5', null, 'N', null, '2020-02-18 10:52:48', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('9', '5', '3', '24', '21', '0', 'เพื่อโปรดพิจารณา', '2020-02-18 11:22:37', '3', '24', '247', '0', null, 'N', null, '2020-02-18 11:22:37', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('10', '6', '5', '0', '576', '1', 'มอบเภสัชกรรมตรวจสอบและแจ้งการเงินดำเนินการต่อไป', '2020-02-19 14:47:42', '2', '22', '497', '0', null, 'N', null, '2020-02-19 14:47:42', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('11', '7', '5', '0', '576', '1', 'แจ้งทุกกลุ่มภารกิจเพื่อทราบและถือปฏิบัติ', '2020-02-19 16:36:19', '1', '0', '723', '1', null, 'Y', '2020-02-20 14:40:47', '2020-02-20 14:40:48', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('12', '7', '5', '0', '576', '1', 'แจ้งทุกกลุ่มภารกิจเพื่อทราบและถือปฏิบัติ', '2020-02-19 16:52:20', '3', '0', '21', '1', null, 'Y', null, '2020-02-20 15:07:51', 'Y', '231', '2020-02-20 15:07:50');
INSERT INTO `edoc_to` VALUES ('13', '7', '5', '0', '576', '1', 'แจ้งทุกกลุ่มภารกิจเพื่อทราบและถือปฏิบัติ', '2020-02-19 16:52:41', '4', '0', '172', '1', null, 'N', null, '2020-02-19 16:52:41', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('14', '3', '4', '35', '122', '0', 'ทดลอง', '2020-02-20 09:32:54', '4', '35', '122', '0', null, 'Y', '2020-02-20 09:34:49', '2020-02-20 09:34:49', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('15', '3', '4', '35', '122', '0', 'ทอลอง', '2020-02-20 09:36:42', '1', '5', '248', '4', null, 'N', null, '2020-02-20 09:36:42', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('16', '7', '5', '0', '576', '1', 'แจ้งทุกกลุ่มภารกิจเพื่อทราบและถือปฏบัติ', '2020-02-20 09:40:29', '2', '19', '208', '4', null, 'N', null, '2020-02-20 09:40:29', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('17', '7', '5', '0', '576', '1', 'แจ้งทุกกลุ่มภารกิจเพื่อทราบและถือปฏบัติ', '2020-02-20 09:42:06', '2', '22', '497', '0', null, 'N', null, '2020-02-20 09:42:07', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('22', '7', '5', '0', '576', '1', 'แจ้งทุกกลุ่มภารกิจเพื่อทราบและถือปฏบัติ', '2020-02-20 09:42:52', '2', '20', '204', '0', null, 'Y', '2020-02-20 14:35:18', '2020-02-20 14:35:18', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('23', '9', '4', '35', '122', '0', 'เพื่อทราบ', '2020-02-20 14:16:38', '1', '0', '723', '1', null, 'Y', '2020-02-20 14:38:13', '2020-02-20 14:38:13', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('24', '9', '4', '35', '122', '0', 'เพื่อทราบ', '2020-02-20 14:16:38', '6', '0', '126', '1', null, 'Y', '2020-02-20 14:39:52', '2020-02-20 14:39:53', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('25', '9', '4', '35', '122', '0', 'เพื่อทราบ', '2020-02-20 14:16:38', '5', '0', '579', '1', null, 'N', null, '2020-02-20 14:16:39', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('26', '9', '4', '35', '122', '0', 'เพื่อทราบ', '2020-02-20 14:16:38', '4', '34', '172', '4', null, 'N', null, '2020-02-20 14:16:39', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('27', '9', '4', '35', '122', '0', 'เพื่อทราบ', '2020-02-20 14:17:58', '4', '0', '226', '3', null, 'Y', '2020-02-20 14:18:32', '2020-02-20 14:18:33', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('28', '9', '4', '35', '122', '0', 'เพื่อเตรียมการต้อนรับ', '2020-02-20 14:21:46', '2', '16', '240', '0', null, 'Y', '2020-02-20 14:42:39', '2020-02-20 14:42:40', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('29', '9', '4', '35', '122', '0', 'เพื่อเตรียมการต้อนรับ', '2020-02-20 14:21:46', '2', '51', '666', '0', null, 'N', null, '2020-02-20 14:21:47', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('30', '9', '4', '35', '122', '0', 'เสร็จแล้วค่ะ', '2020-02-20 14:24:09', '4', '0', '226', '3', '18', 'Y', '2020-02-20 14:25:00', '2020-02-20 14:25:01', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('31', '3', '4', '0', '226', '3', 'เพื่อแจ้่งบุคลากรในสังกัดทราบ', '2020-02-20 14:31:52', '2', '16', '240', '0', null, 'Y', '2020-02-20 14:45:53', '2020-02-20 14:45:53', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('32', '3', '4', '0', '226', '3', 'เพื่อแจ้่งบุคลากรในสังกัดทราบ', '2020-02-20 14:31:52', '1', '93', '153', '4', null, 'Y', '2020-02-20 14:33:50', '2020-02-20 14:33:51', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('33', '3', '4', '0', '226', '3', 'เพื่อแจ้่งบุคลากรในสังกัดทราบ', '2020-02-20 14:31:52', '4', '50', '142', '4', null, 'Y', '2020-02-20 14:38:05', '2020-02-20 14:38:06', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('34', '3', '4', '0', '226', '3', 'เพื่อแจ้งบุคคลในฝ่ายทราบ', '2020-02-20 14:33:07', '1', '8', '215', '0', null, 'Y', '2020-02-20 14:35:39', '2020-02-20 14:35:39', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('35', '3', '1', '93', '153', '4', 'กำลังดำเนินการค่ะ', '2020-02-20 14:37:28', '4', '0', '226', '3', '19', 'Y', '2020-02-20 14:38:18', '2020-02-20 14:38:18', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('36', '3', '4', '50', '142', '4', 'รับทราบแล้วค่ะ', '2020-02-20 14:39:36', '4', '0', '226', '3', null, 'N', null, '2020-02-20 14:39:36', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('37', '3', '1', '8', '215', '0', 'คับ ๆๆๆๆๆ', '2020-02-20 14:45:15', '4', '0', '226', '3', null, 'N', null, '2020-02-20 14:45:15', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('38', '9', '6', '0', '126', '1', 'ปฏิบัติด่วน', '2020-02-20 14:45:49', '5', '42', '729', '0', '20', 'Y', '2020-02-20 14:48:13', '2020-02-20 14:48:13', 'Y', null, null);
INSERT INTO `edoc_to` VALUES ('39', '9', '1', '0', '723', '1', 'แจ้งเพื่อทราบ', '2020-02-20 15:00:53', '1', '0', '230', '3', null, 'Y', null, '2020-02-20 15:01:43', 'Y', '723', '2020-02-20 15:01:43');
INSERT INTO `edoc_to` VALUES ('40', '9', '1', '0', '723', '1', 'เอกสารนำส่งผิดพลาด', '2020-02-20 15:02:49', '5', '0', '576', '1', null, 'N', null, '2020-02-20 15:02:50', 'Y', null, null);

-- ----------------------------
-- Table structure for edoc_type
-- ----------------------------
DROP TABLE IF EXISTS `edoc_type`;
CREATE TABLE `edoc_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_type
-- ----------------------------
INSERT INTO `edoc_type` VALUES ('1', 'คำสั่ง');
INSERT INTO `edoc_type` VALUES ('2', 'ประกาศ');
INSERT INTO `edoc_type` VALUES ('3', 'ทั่วไป');
