/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : office_db

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-05 13:31:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for edoc_book
-- ----------------------------
DROP TABLE IF EXISTS `edoc_book`;
CREATE TABLE `edoc_book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_code` varchar(30) NOT NULL,
  `book_subject` varchar(255) NOT NULL,
  `book_date` date NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '1',
  `book_year` int(4) NOT NULL DEFAULT '2563',
  `reg_datetime` datetime DEFAULT NULL,
  `ext_from_id` int(11) DEFAULT NULL,
  `ext_to_id` int(11) DEFAULT NULL,
  `book_no` int(11) NOT NULL DEFAULT '0',
  `dep_id` int(11) DEFAULT NULL,
  `dep2_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `int_to_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '3',
  `speed_id` int(11) NOT NULL DEFAULT '1',
  `director_accept` int(11) NOT NULL DEFAULT '0',
  `member_update` int(11) DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_follow` enum('N','Y') NOT NULL DEFAULT 'N',
  `fam_no_super` int(11) DEFAULT NULL,
  `in_date_super` date DEFAULT NULL,
  `out_date_super` date DEFAULT NULL,
  `fam_no_director` int(11) DEFAULT NULL,
  `in_date_director` date DEFAULT NULL,
  `out_date_director` date DEFAULT NULL,
  `ssn` varchar(30) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `director_file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_book
-- ----------------------------
INSERT INTO `edoc_book` VALUES ('2', 'ขก.032.200.21', 'ขอเชิญประชุมคณะทำงานสารบรรณเขตที่7', '2020-01-26', '3', '2563', '2020-01-26 15:41:54', '1', '1', '1', '4', '35', '122', null, null, '3', '2', '5', '122', '2020-02-02 18:29:28', 'N', '1', '2020-01-30', '2020-02-04', '2', '2020-02-04', '2020-02-04', null, null, '6');
INSERT INTO `edoc_book` VALUES ('3', 'ขก.032.200.22', 'ขอเชิญประชุมคณะทำงานสารบรรณเขตที่7888888', '2020-01-30', '3', '2563', '2020-01-30 11:48:29', '2', '1', '2', '4', '35', '122', null, null, '3', '1', '5', '122', '2020-02-05 11:10:10', 'N', '1', '2020-02-02', '2020-02-02', '1', '2020-02-02', '2020-02-02', null, null, '10');
INSERT INTO `edoc_book` VALUES ('4', 'ขก.032.200.2', 'ขอเชิญประชุมความเร่งด่วนทางการแพทย์ปกติ', '2020-02-03', '3', '2563', '2020-02-03 13:22:07', '2', '1', '3', '4', '35', '122', '2020-02-03', null, '3', '1', '5', '122', '2020-02-05 11:11:49', 'N', '1', '2020-02-05', '2020-02-05', '1', '2020-02-05', '2020-02-05', '63-3-3', '9', '11');

-- ----------------------------
-- Table structure for edoc_ext_from
-- ----------------------------
DROP TABLE IF EXISTS `edoc_ext_from`;
CREATE TABLE `edoc_ext_from` (
  `ext_from_id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_from_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ext_from_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_ext_from
-- ----------------------------
INSERT INTO `edoc_ext_from` VALUES ('1', 'สสจ');
INSERT INTO `edoc_ext_from` VALUES ('2', 'สำนักงานสาธารณสุขจังหวัดขอนแก่น');

-- ----------------------------
-- Table structure for edoc_ext_to
-- ----------------------------
DROP TABLE IF EXISTS `edoc_ext_to`;
CREATE TABLE `edoc_ext_to` (
  `ext_to_id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_to_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ext_to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_ext_to
-- ----------------------------
INSERT INTO `edoc_ext_to` VALUES ('1', 'ผู้อำนวยการ');

-- ----------------------------
-- Table structure for edoc_file
-- ----------------------------
DROP TABLE IF EXISTS `edoc_file`;
CREATE TABLE `edoc_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_file
-- ----------------------------
INSERT INTO `edoc_file` VALUES ('2', '2', '2563_1_1580316661.pdf', '2020-01-29 23:51:01');
INSERT INTO `edoc_file` VALUES ('3', '3', '2563_2_1580359760.pdf', '2020-01-30 11:49:20');
INSERT INTO `edoc_file` VALUES ('5', '2', '2563_1_1580399706.pdf', '2020-01-30 22:55:06');
INSERT INTO `edoc_file` VALUES ('6', '2', '2563_1_1580642968.pdf', '2020-02-02 18:29:28');
INSERT INTO `edoc_file` VALUES ('7', '3', '2563_2_1580646067.pdf', '2020-02-02 19:21:07');
INSERT INTO `edoc_file` VALUES ('8', '3', '2563_2_1580646134.pdf', '2020-02-02 19:22:14');
INSERT INTO `edoc_file` VALUES ('9', '4', '2563_3_1580710947.pdf', '2020-02-03 13:22:27');
INSERT INTO `edoc_file` VALUES ('10', '3', '2563_2_1580875810.pdf', '2020-02-05 11:10:10');
INSERT INTO `edoc_file` VALUES ('11', '4', '2563_3_1580875909.pdf', '2020-02-05 11:11:49');

-- ----------------------------
-- Table structure for edoc_file_read
-- ----------------------------
DROP TABLE IF EXISTS `edoc_file_read`;
CREATE TABLE `edoc_file_read` (
  `read_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `o_id` int(11) NOT NULL,
  `read_datetime` datetime NOT NULL,
  PRIMARY KEY (`read_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_file_read
-- ----------------------------
INSERT INTO `edoc_file_read` VALUES ('1', '2', '122', '0', '2020-01-29 21:45:52');
INSERT INTO `edoc_file_read` VALUES ('2', '2', '172', '1', '2020-02-02 18:57:42');
INSERT INTO `edoc_file_read` VALUES ('3', '3', '122', '0', '2020-02-02 19:53:36');

-- ----------------------------
-- Table structure for edoc_first_no
-- ----------------------------
DROP TABLE IF EXISTS `edoc_first_no`;
CREATE TABLE `edoc_first_no` (
  `inyear` int(11) NOT NULL,
  `first_no` int(11) NOT NULL,
  PRIMARY KEY (`inyear`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_first_no
-- ----------------------------
INSERT INTO `edoc_first_no` VALUES ('2563', '101');

-- ----------------------------
-- Table structure for edoc_operator
-- ----------------------------
DROP TABLE IF EXISTS `edoc_operator`;
CREATE TABLE `edoc_operator` (
  `o_id` int(11) NOT NULL AUTO_INCREMENT,
  `ol_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `dep2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`o_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_operator
-- ----------------------------
INSERT INTO `edoc_operator` VALUES ('1', '1', '172', '4', '0');
INSERT INTO `edoc_operator` VALUES ('2', '1', '248', '5', '0');
INSERT INTO `edoc_operator` VALUES ('3', '4', '253', '1', '2');

-- ----------------------------
-- Table structure for edoc_operator_level
-- ----------------------------
DROP TABLE IF EXISTS `edoc_operator_level`;
CREATE TABLE `edoc_operator_level` (
  `ol_id` int(11) NOT NULL,
  `ol_name` varchar(200) DEFAULT '',
  PRIMARY KEY (`ol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_operator_level
-- ----------------------------
INSERT INTO `edoc_operator_level` VALUES ('1', 'สารบรรณกลุ่มภาระกิจ');
INSERT INTO `edoc_operator_level` VALUES ('2', 'สารบรรณหน่วยงาน');
INSERT INTO `edoc_operator_level` VALUES ('3', 'หัวหน้ากลุ่มภาระกิจ');
INSERT INTO `edoc_operator_level` VALUES ('4', 'หัวหน้ากลุ่มงาน');
INSERT INTO `edoc_operator_level` VALUES ('5', 'ผู้อำนวยการ');

-- ----------------------------
-- Table structure for edoc_out_ext
-- ----------------------------
DROP TABLE IF EXISTS `edoc_out_ext`;
CREATE TABLE `edoc_out_ext` (
  `out_ext_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_year` int(4) DEFAULT NULL,
  `out_no` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `out_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`out_ext_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_out_ext
-- ----------------------------

-- ----------------------------
-- Table structure for edoc_section
-- ----------------------------
DROP TABLE IF EXISTS `edoc_section`;
CREATE TABLE `edoc_section` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(200) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_section
-- ----------------------------
INSERT INTO `edoc_section` VALUES ('1', 'หนังสือภายใน');
INSERT INTO `edoc_section` VALUES ('2', 'หนังสือส่งออกภายนอก');
INSERT INTO `edoc_section` VALUES ('3', 'หนังสือรับภายนอก');

-- ----------------------------
-- Table structure for edoc_speed
-- ----------------------------
DROP TABLE IF EXISTS `edoc_speed`;
CREATE TABLE `edoc_speed` (
  `speed_id` int(11) NOT NULL AUTO_INCREMENT,
  `speed_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`speed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_speed
-- ----------------------------
INSERT INTO `edoc_speed` VALUES ('1', 'ปรกติ');
INSERT INTO `edoc_speed` VALUES ('2', 'ด่วน');
INSERT INTO `edoc_speed` VALUES ('3', 'ด่วนมาก');
INSERT INTO `edoc_speed` VALUES ('4', 'ด่วนที่สุด');

-- ----------------------------
-- Table structure for edoc_to
-- ----------------------------
DROP TABLE IF EXISTS `edoc_to`;
CREATE TABLE `edoc_to` (
  `to_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `dep2_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `ol_id` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `to_datetime` datetime DEFAULT NULL,
  `to_member_id` int(11) DEFAULT NULL,
  `to_ol_id` int(11) NOT NULL DEFAULT '0',
  `file_id` int(11) DEFAULT NULL,
  `member_read` enum('N','Y') NOT NULL DEFAULT 'N',
  `read_datetime` datetime DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alert_status` enum('N','Y') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_to
-- ----------------------------
INSERT INTO `edoc_to` VALUES ('6', '2', '4', '35', '122', '0', 'dsaf  ladks   aldksf  l adf a ldsfk a sdl fkasdfpasdfja dso k', '2020-01-30 22:55:06', '248', '1', '5', 'N', null, '2020-01-30 22:55:06', 'Y');
INSERT INTO `edoc_to` VALUES ('7', '2', '4', '35', '122', '0', 'dsaf  ladks   aldksf  l adf a ldsfk a sdl fkasdfpasdfja dso k', '2020-01-30 22:55:06', '172', '1', '5', 'Y', '2020-02-02 18:57:42', '2020-02-02 18:57:42', 'Y');
INSERT INTO `edoc_to` VALUES ('9', '3', '4', '35', '122', '0', 'ทดสอบความคิดเห็น   ', '2020-02-02 19:31:33', '172', '1', null, 'N', null, '2020-02-02 19:31:33', 'Y');
INSERT INTO `edoc_to` VALUES ('10', '4', '4', '35', '122', '0', 'ห', '2020-02-05 11:35:00', '122', '0', null, 'N', null, '2020-02-05 11:35:00', 'Y');

-- ----------------------------
-- Table structure for edoc_type
-- ----------------------------
DROP TABLE IF EXISTS `edoc_type`;
CREATE TABLE `edoc_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edoc_type
-- ----------------------------
INSERT INTO `edoc_type` VALUES ('1', 'คำสั่ง');
INSERT INTO `edoc_type` VALUES ('2', 'ประกาศ');
INSERT INTO `edoc_type` VALUES ('3', 'ทั่วไป');
