<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-6">

        <div class="card">
            <div class="card-header">


<form class="form-inline" id="form_date">
  <label for="bdate" class="mr-sm-2">ปริมาณหนังสือ วันที่:</label>
  <input type="text" class="form-control mb-2 mr-sm-2 date" name="bdate" id="bdate" value="<?php echo date_thai_input($today_date);?>" required  >
  <label for="edate" class="mr-sm-2"> - </label>
  <input type="text" class="form-control mb-2 mr-sm-2 date" name="edate" id="edate" value="<?php echo date_thai_input($today_date);?>" required  >

  <button type="submit" class="btn btn-primary mb-2">แสดงผล</button>
</form>

            </div>
            <div class="card-body" id="show_data">
                
            </div>
        </div>

        </div>

        <div class="col-lg-6">

        <div class="card">
            <div class="card-header bg-info text-white">
                กราฟแสดงปริมาณหนังสือ
            </div>
            <div class="card-body">

            <div id="show_graph">กำลังประมวลผล...</div>
                
            </div>
        </div>

        <div class="card">
            <div class="card-header bg-primary text-white">
                % กราฟแสดงร้อยละที่อ่านแล้ว
            </div>
            <div class="card-body">

            <div id="show_graph2">กำลังประมวลผล...</div>
                
            </div>
        </div>

        </div>
    </div>

</div>

<script>
$("#bdate").inputmask({"mask": "99/99/9999"});
$("#edate").inputmask({"mask": "99/99/9999"});

$('.date').datepicker({
        todayBtn: 'linked',
        language: "th-th",
        keyboardNavigation: true,
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
        //startDate: '-3d'
});

$('#show_data').load("report_sum_data.php");

$("#form_date").submit(function(e){
    e.preventDefault();
    //alert("dd");
    
    $.post("report_sum_data.php",$('#form_date').serialize(),function(info){
        $('#show_data').html(info);
    });

    $.post("report_graph.php",$('#form_date').serialize(),function(info2){
        $('#show_graph').html(info2);
    });
    $.post("report_graph2.php",$('#form_date').serialize(),function(info3){
        $('#show_graph2').html(info3);
    });
    
});

$("#show_graph").load("report_graph.php");
$("#show_graph2").load("report_graph2.php");

</script>