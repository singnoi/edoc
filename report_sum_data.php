<?php
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
if(isset($_POST['bdate'])) {
    $bdate = date_thai_db($_POST['bdate']);
    $edate = date_thai_db($_POST['edate']);
} else {
    $bdate = $today_date;
    $edate = $today_date;
}
?>

                <table class="table table-striped table-bordered table-sm" id="tb1">
                    <thead>
                        <tr>
                            <th>วันที่</th>
                            <th class="text-right">จำนวนเรื่อง</th>
                            <th class="text-right">จำนวนที่ต้องอ่าน</th>
                            <th class="text-right">จำนวนอ่านแล้ว</th>
                            <th class="text-right">ร้อยละที่อ่านแล้ว</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    
                    $q = "SELECT
                    DATE(t.to_datetime) AS t_date,
                    Count(t.to_id) AS c_t,
                    Count(DISTINCT t.book_id) AS c_b,
                    (SELECT Count(t2.to_id) FROM edoc_to as t2 WHERE t2.member_read = 'Y' AND DATE(t2.to_datetime) = DATE(t.to_datetime) ) as c_y
                    FROM
                    edoc_to AS t
                    WHERE
                    DATE(t.to_datetime) BETWEEN '$bdate' AND '$edate'
                    GROUP BY
                    DATE(t.to_datetime)
                    order by  DATE(t.to_datetime) DESC             
                    ";
                    $r = $con->query($q) or die ($q);
                    if($r->num_rows > 0) {
                        while ($ob = $r->fetch_object()) {
                            $percen = 0;
                            $percen = ($ob->c_y * 100) / $ob->c_t;
                            echo "<tr>";
                            echo "<td>".date_thai($ob->t_date)."</td>";
                            echo "<td class='text-right'>".comma($ob->c_b)."</td>";
                            echo "<td class='text-right'>".comma($ob->c_t)."</td>"; 
                            echo "<td class='text-right'>".comma($ob->c_y)."</td>";
                            echo "<td class='text-right'>".comma($percen)."%</td>";
                            echo "</tr>";
                        }
                    }
                    ?>
                    </tbody>
                </table>
            
<?php
$con->close();
?>

<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหา :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 25,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": false,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    "ordering": false,
    "searching": false,
    //"paging":  false

});

</script>