<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$status = "";
$member_id = $_SESSION['member_id'];
$operator = $_SESSION['operator'];

function utf8_strlen($str)

   {
      $c = strlen($str);


            $l = 0;
      for ($i = 0; $i < $c; ++$i)
      {
         if ((ord($str[$i]) & 0xC0) != 0x80)
         {
            ++$l;
         }
      }
      return $l;
   }

$book_code = $_POST['book_code'];
$bn = utf8_strlen($book_code);
if($bn < 4) {
    echo "<div class='alert alert-danger'> กรุณากรอกคำค้นหาเกิน 4 ตัวอักษร </div>";
    exit();
}

?>

            <div class="card border-0 shadow ">
            <div class="card-header">

                <i class="fas fa-book fa-1x shadow mr-2"></i> <span > หนังสือที่ค้นพบ</span> 
         
                <span class="float-right">
                
                </span>
            </div>
            <div class="card-body">
            <?php
            $qxi = "SELECT
                    b.book_id,
                    b.book_code,
                    b.book_subject,
                    b.book_date,
                    b.reg_datetime,
                    b.book_no,
                    b.director_accept,
                    b.section_id,
                    f.ext_from_name,
                    s.speed_name,
                    st.section_name,
                    b.member_id,
                    b.ssn,
                    t.read_datetime,
                    t.member_read ,
                    t.to_member_id ,
                    et.ext_to_name
                    FROM
                    edoc_book AS b
                    LEFT OUTER JOIN edoc_to as t on b.book_id = t.book_id
                    LEFT OUTER JOIN edoc_section AS st ON b.section_id = st.section_id
                    LEFT OUTER JOIN edoc_ext_from AS f ON b.ext_from_id = f.ext_from_id
                    LEFT OUTER JOIN edoc_ext_to AS et ON b.ext_to_id = et.ext_to_id
                    LEFT OUTER JOIN edoc_speed AS s ON b.speed_id = s.speed_id
                    WHERE
                    b.book_code like '%$book_code%'
                    OR b.book_subject like '%$book_code%'
                    OR b.book_no like '%$book_code%'
                    group by b.book_id 
                    ORDER BY
                    b.reg_datetime DESC
                    LIMIT 50
                    ";
            $rxi = $con->query($qxi) or die ($qxi);
            
            ?>
                <table class="table table-sm" id="tb1">
                    <thead>
                        <tr>
                            <th>ประเภท</th>
                            <th>เลขที่รับ</th>
                            <th>เร่งด่วน</th>
                            <th>หนังสือเลขที่</th>
                            <th>วันที่</th>
                            <th>เรื่อง</th>
                            <th>จาก</th>                            
                            <th class="text-center">สถานะ</th>
                            <th class="text-center">อ่านแล้ว</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if($rxi->num_rows > 0) {
                            while ($obi = $rxi->fetch_object()) {
                                $section_id = $obi->section_id;
                                if($section_id < 3){
                                    $qm1 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$obi->member_id' ";
                                    $from_name = $con_s->query($qm1)->fetch_object()->fullname;
                                    if($obi->to_member_id != NULL && $obi->to_member_id != '' ) {
                                        $qm2 = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$obi->to_member_id' ";
                                        $to_name = $con_s->query($qm2)->fetch_object()->fullname;
                                    } else {
                                        $to_name = "";
                                    }
                                } else {
                                    $from_name = $obi->ext_from_name;
                                    $to_name = $obi->ext_to_name;
                                }

                                echo "<tr>";
                                echo "<td>".book_section($obi->section_id)."</td>";
                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php 
                                echo $obi->ssn;
                                ?>
                                </a>
                                <?php 
                                echo "</td>";
                                echo "<td>".$obi->speed_name."</td>";

                                echo "<td>";
                                ?>
                                <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                <?php echo $obi->book_code; ?>
                                </a>
                                <?php 
                                echo "</td>";
                                echo "<td>".date_thai_xs($obi->book_date)."</td>";
                                echo "<td>".iconv_substr($obi->book_subject,0,45,'UTF-8')."..</td>";
                                echo "<td>";
                                echo $from_name;
                                echo "</td>";                               
                                echo "<td class='text-center'>";

                                    ?>
                                    <a href="?page=book_manage&book_id=<?php echo $obi->book_id;?>" class="text-primary">
                                    <?php 
                                    echo director_accept($obi->director_accept);
                                    ?>
                                    </a>
                                    <?php 

                                echo "</td>";
                                echo "<td class='text-center'>";
                                $qr = "SELECT count(to_id) as sum_r from edoc_to where book_id = '$obi->book_id' and member_read = 'Y'";
                                $sum_read = $con->query($qr)->fetch_object()->sum_r;
                                $qrn = "SELECT count(to_id) as sum_n from edoc_to where book_id = '$obi->book_id' and member_read = 'N'";
                                $sum_no = $con->query($qrn)->fetch_object()->sum_n;
                                ?>
                                <a href="#" onclick="show_read(<?php echo $obi->book_id;?>)" class="ml-2">
                                <span class="badge badge-dark"> 
                                <?php 
                                    echo "อ่าน ".comma($sum_read);
                                    echo "/ไม่อ่าน ".comma($sum_no);
                                
                                ?>
                                </span></a>
                                <?php 
                                echo "</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                    </tbody>
                </table>
                
                
            </div>
        </div>


<?php
$con->close();
$con_s->close();
?>


<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ประวัติการเปิดอ่าน</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body p-1" id="md2-body">
        Modal body..
      </div>


    </div>
  </div>
</div>

<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาหัวข้อเรื่อง :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 25,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": false,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    "ordering": false,
    //"searching": false,
    //"paging":  false

});

function show_read(id) {
    $('#md2-body').load("history_read.php?book_id="+id);
    $('#md2').modal('show');
    
}
</script>