<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-12">
        
<div class="card-deck">

  <div class="card card-link  border border-info text-center shadow-sm">
    <a href="./?page=admin_first_no" class="nav-link" >
    <i class="fas fa-fast-backward fa-5x "></i>
        <h5 class="textIT" >เลขที่ตั้งต้นของปี</h5>
    </a>
  </div>

  <div class="card card-link  border border-info text-center shadow-sm">
    <a href="./?page=dep" class="nav-link" >
    <i class="fas fa-users-cog fa-5x "></i>
        <h5 class="textIT" >สิทธิ์การใช้งาน</h5>
    </a>
  </div>

  <div class="card card-link  border border-info text-center shadow-sm">
    <a href="./?page=dep_code" class="nav-link" >
    <i class="far fa-list-alt fa-5x "></i>
        <h5 class="textIT" >เลขที่ประจำหน่วยงาน</h5>
    </a>
  </div>
  
  <div class="card card-link  border border-info text-center shadow-sm">
    <a href="./?page=main_in&section_id=3" class="nav-link" >
    <i class="fas fa-atlas fa-3x "></i>
        <h5 class="textIT" >หนังสือภายนอกของฝ่าย</h5>
    </a>
  </div>
  <div class="card card-link  border border-info text-center shadow-sm">
    <a href="./?page=main_in&section_id=1" class="nav-link" >
    <i class="fas fa-book fa-3x "></i>
        <h5 class="textIT" >หนังสือภายในของฝ่าย</h5>
    </a>
  </div>
 

</div> <!-- card-deck -->

        
        </div> <!-- col -->
    </div> <!-- row -->
</div> <!-- container -->

<div class="container-fluid mt-5" id="main_oper">

</div> <!-- container -->

<script>
$('#main_oper').load("main_operator.php");
</script>