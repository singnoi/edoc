<?php 
session_start();
$member_update = $_SESSION['member_id'];
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
$con_s = connect();
$uploadDir = 'book_file/'; 
$response = array( 
    'status' => 0, 
    'message' => 'บันทึกไม่สำเร็จ กรุณาตรวจสอบข้อมูลก่อนบันทึกอีกครั้ง.',
    'file_name' => '' ,
    'book_no' => '',
    'book_code' => '' 
); 


 
// If form is submitted 
if(isset($_POST['book_subject']) || isset($_POST['book_no']) ){ 
    // Get the submitted form data 

    $book_subject = $_POST['book_subject']; 
    $book_no = $_POST['book_no']; 
    $book_year = $_POST['book_year']; 
    $section_id = $_POST['section_id']; 
    $reg_datetime = $_POST['reg_datetime']; 
    $dep_id = $_POST['dep_id']; 
    $dep2_id = $_POST['dep2_id']; 
    $member_id = $_POST['member_id']; 
    $to_member_id = $_POST['to_member_id']; 
    $book_code = $_POST['book_code']; 
    $book_date = date_thai_db($_POST['book_date']); 
    $speed_id = $_POST['speed_id']; 
   
/*
    $qy = "SELECT book_id from edoc_book where book_code = '$book_code' and book_year = '$book_year' and section_id = 1 ";
    $ry = $con->query($qy) or die ($qy);
    if($ry->num_rows > 0) {
        $response['message'] = "เลขที่หนังสือ {$book_code} นี้ มีผู้ลงทะเบียนสำเร็จก่อนแล้วครับ กรุณารีเฟรชใหม่อีกครั้ง";
        echo json_encode($response);
        exit();
    }
*/
    
// ถ้าเลขซ้ำให้ออกเลขถัดไป เริ่ม -----
$qy = "SELECT book_id from edoc_book where book_code = '$book_code' and book_year = '$book_year' and section_id = $section_id ";
$ry = $con->query($qy) or die ($qy);
if($ry->num_rows > 0) {

    $qm = "SELECT
            Max(b.book_no) as max_no
            FROM
            edoc_book AS b
            WHERE
            b.book_year = $book_year AND
            b.section_id = $section_id ";
    $rm = $con->query($qm) or die ($qm);
    $max_no = $rm->fetch_object()->max_no;
    $book_no = $max_no + 1;

    $qd = "SELECT department_code from departments where department_id = '$dep_id'";
    $dep_code = $con_s->query($qd)->fetch_object()->department_code;

    if($dep2_id > 0) {
        $qd2 = "SELECT d2.dep2_edoc,d2.department_id,d.department_code from department_sub2 as d2 left outer join departments as d on d.department_id = d2.department_id where d2.department_sub2_id = '$dep2_id' ";
        $rd2 = $con_s->query($qd2) or die ($qd2);
        if($rd2->num_rows > 0) {
            $ob2 = $rd2->fetch_object();
            $dep2_code = $ob2->dep2_edoc;
            $dep_id = $ob2->department_id;
            $dep_code = $ob2->department_code;
            if($dep2_code == NULL || $dep2_code == '') {
                $book_code = $hos_code.$dep_code.".".$book_no;
            } else {
                $book_code = $hos_code.$dep_code.".".$dep2_code.".".$book_no;
            }
        } else {
            $book_code = $hos_code.$dep_code.".".$book_no;
        }
    } else {
        $book_code = $hos_code.$dep_code.".".$book_no;
    }

}
// ถ้าเลขซ้ำให้ออกเลขถัดไป จบ. ----
$response['book_no'] = $book_no; 
$response['book_code'] = $book_code; 

$ssn = substr($book_year,2,2)."-".$section_id."-".$book_no;
             
            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                 
                // File path config 
                $fileName = basename($_FILES["file"]["name"]); 
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 

                $random_no = strtotime($today);

                $newfile = $book_year."_".$book_no."_".$random_no.".".$fileType;
                $targetFilePath = $uploadDir . $newfile; 

                // Allow certain file formats 
                $allowTypes = array('pdf'); 
                if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                        $uploadedFile = $newfile; 
                        $uploadStatus = 1;
                        $response['file_name'] = $uploadedFile;
                    }else{ 
                        $uploadStatus = 0; 
                        $response['message'] = 'ผิดพลาด! ไฟล์ไม่สมบูรณ์.'; 
                    } 
                }else{ 
                    $uploadStatus = 0; 
                    $response['message'] = 'ผิดพลาด! ไม่ใช่ไฟล์PDF.'; 
                } 
            } else {
                $uploadStatus = 0; 
                $response['message'] = 'ไม่มีไฟล์แนบไฟล์ บันทึกสำเร็จ';
            } 

// Insert form data in the database 
                $q = "INSERT INTO edoc_book (book_code,book_subject,book_date,section_id,book_year,reg_datetime,to_member_id,book_no,dep_id,dep2_id,member_id,speed_id,ssn,member_update) VALUES ('$book_code','$book_subject','$book_date','$section_id','$book_year','$reg_datetime','$to_member_id','$book_no','$dep_id','$dep2_id','$member_id','$speed_id','$ssn','$member_update')";
                $insert = $con->query($q) ; 
                $response['status'] = 1;
             
            if($uploadStatus == 1){ 

                              
                if($insert){ 
                     
                    $response['message'] = 'บันทึกสำเร็จ'; 

                    $qs = "SELECT book_id from edoc_book where ssn = '$ssn'";
                    $book_id = $con->query($qs)->fetch_object()->book_id;

                    // stamp file
                    $qm = "INSERT INTO edoc_file (book_id,`file_name`) values ('$book_id','$uploadedFile'); ";
                    $con->query($qm) or die ($qm);
                    $qf = "SELECT `file_id` from edoc_file where `file_name` = '$uploadedFile'";
                    $file_id = $con->query($qf)->fetch_object()->file_id;

                    $qu = "UPDATE edoc_book set `file_id` = '$file_id' where book_id = '$book_id' ";
                    $con->query($qu) or die ($qu);
                } else {
                    $response['message'] = $q;
                }
            }  
        
} 
 $con->close();
 $con_s->close();
// Return response 
echo json_encode($response);