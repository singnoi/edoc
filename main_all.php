<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12" >
        <div class="media border p-3">
            ค้นหาหนังสือ:
            <div class="media-body">
                <div class="input-group col-md-4">
                    <input class="form-control py-2 border-right-0 border" type="search" value="" id="book_code" name="book_code">
                    <span class="input-group-append">
                    <button class="btn btn-outline-secondary border-left-0 border" type="button" onclick="search_code();">
                            <i class="fa fa-search"></i>
                    </button>
                    </span>
                </div>
            </div>
        </div>
        
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" id="data_all">
        
        </div>
    </div>
</div>

<script>
function search_code() {
    var book_code = $("#book_code").val();
    $.post("main_all_data.php",{book_code: book_code},function(info){
        $("#data_all").html(info);
    });
}
$("#book_code").on("keyup", function() {
    search_code();
});
</script>