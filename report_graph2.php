<?php 
include("../includes/db_connect.php");
include("./includes/function.php");
$con = connect_db($db);
if(isset($_POST['bdate'])) {
    $bdate = date_thai_db($_POST['bdate']);
    $edate = date_thai_db($_POST['edate']);
} else {
    $bdate = $today_date;
    $edate = $today_date;
}

$q = "SELECT
DATE(t.to_datetime) AS t_date,
Count(t.to_id) AS c_t,
Count(DISTINCT t.book_id) AS c_b,
(SELECT Count(t2.to_id) FROM edoc_to as t2 WHERE t2.member_read = 'Y' AND DATE(t2.to_datetime) = DATE(t.to_datetime) ) as c_y
FROM
edoc_to AS t
WHERE
DATE(t.to_datetime) BETWEEN '$bdate' AND '$edate'
GROUP BY
DATE(t.to_datetime)
order by  DATE(t.to_datetime) ASC ";
$r = $con->query($q) or die ($q);
$n = $r->num_rows;

//echo $n;

$x = array();
$percen = array();
$x1 = "";
$percen1 = "";

if($n > 0) {
    while ($ob = $r->fetch_object()) {
        $time1 = date_thai($ob->t_date);
		array_push($x,$time1);
		$percen2 = ($ob->c_y * 100) / $ob->c_t;
		array_push($percen,$percen2);

    }
	$x1 = implode("','",$x);
	$percen1 = implode(",",$percen);

}
$con->close();
?>
 
<link rel="stylesheet" href="../node_modules/chart.js/dist/Chart.min.css">

<p>
<?php
//echo $x1;
?>
</p>
<canvas id="canvas4" >กำลังประมวลผล...</canvas>

<script>

		var config = {
			type: 'line',
			data: {
				labels: ['<?php echo $x1;?>'],

				datasets: [{
					label: 'ร้อยละที่อ่านแล้ว',
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [
						<?php echo $percen1;?>
					],
					fill: false,
				}]
			},
			options: {
				responsive: true,
				
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				animation: {
					duration: 0
				},
				scales: {
					xAxes: [{
                        /*
						ticks: {
							callback: function(dataLabel, index) {
								// Hide the label of every 2nd dataset. return null to hide the grid line too
								return index % 1 === 0 ? dataLabel : '';
							}
						},
                        */
						/*
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'วันที่ '
						}
						*/
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'จำนวนร้อยละ'
						},
						
					}]
				}
			}
		};
/*
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};
*/
		var ctx = document.getElementById('canvas4').getContext('2d');
		window.myLine = new Chart(ctx, config);

	</script>