<?php
$con_s = connect();
$con = connect_db($db);
$q = "SELECT
d2.department_sub2_id,
d2.department_id,
d2.department_sub2_name,
d.department_name,
d2.department_sub2_head_id,
d.manager_id,
concat(m.name_only,' ',m.lname) as dep_head_name,
concat(m2.name_only,' ',m2.lname) as dep2_head_name
FROM
department_sub2 AS d2
LEFT OUTER JOIN departments AS d ON d2.department_id = d.department_id
LEFT OUTER JOIN members AS m ON d.manager_id = m.member_id
LEFT OUTER JOIN members AS m2 ON d2.department_sub2_head_id = m2.member_id
WHERE
d2.department_id < 7
ORDER BY
d.department_name ASC,
d2.department_sub2_name ASC";
$r = $con_s->query($q) or die ($q);
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    ผู้รับผิดชอบงานสารบรรณ
                </div>
                <div class="card-body">

<table class="table table-sm table-striped table-bordered">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>รายการหน่วยงาน</th>
            <th>หัวหน้า</th>
            <th>เจ้าหน้าที่สารบรรณ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        $j = 0;
        $dep_id = 0;
        while ($ob = $r->fetch_object()) {

            // show dep
            if($dep_id != $ob->department_id) {
                $i++;
                $j = 0;
                $dep_id = $ob->department_id;
                $dep2_id = 0;
                $member_id = 0;
                echo "<tr class='bg-info'>";
                echo "<th colspan='2' >กลุ่มภารกิจ: ".$ob->department_name."</th>";
                echo "<th> <span id='head{$i}'>";
                $qd2 = "SELECT * from edoc_operator where member_id = '$ob->manager_id' and (ol_id = 3 or ol_id = 5) and dep_id = '$ob->department_id' ";
                if($con->query($qd2)->num_rows) {
                    echo "<span class='badge badge-secondary'>".$ob->dep_head_name."</span>";
                } else {
                    echo $ob->dep_head_name;
                }
  
                    ?>
                    <button onclick="show_select_head('<?php echo $i;?>','<?php echo $dep_id;?>','0','<?php echo $ob->manager_id;?>');" class="btn btn-sm text-warning ml-2" ><i class="fas fa-pencil-alt fa-xs"></i></button>
                    <?php
                echo "</span></th>";
                echo "<th> <span id='ol{$i}'>";
                $qs = "SELECT g.member_id FROM edoc_operator AS g WHERE g.dep_id = $dep_id AND g.ol_id = 1";
                $rs = $con->query($qs) or die ($qs);
                if($rs->num_rows > 0) {
                    $j = 0;
                    while ($obs = $rs->fetch_object()) {
                        $j++;
                        if($j != 1) echo ", ";
                        $member_id = $obs->member_id;
                        $qm = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$member_id'";
                        $fullname = $con_s->query($qm)->fetch_object()->fullname;
                        echo $fullname;

                    }
                    
                } 
                    ?>
                    <button onclick="show_select_dep('<?php echo $i;?>','<?php echo $dep_id;?>','0','1','<?php echo $member_id;?>');" class="btn btn-sm text-warning ml-2" ><i class="fas fa-pencil-alt fa-xs"></i></button>
                    <?php 
                echo "</span></th>";
                echo "</tr>";
            }
            // end show dep
            $i++;
            $j++;

            echo "<tr>";
            echo "<td class='text-right'>$j</td>";
            echo "<td>$ob->department_sub2_name</td>";

            echo "<td> <span id='head{$i}'>";
            $qd2 = "SELECT * from edoc_operator where member_id = '$ob->department_sub2_head_id' and ol_id = 4 and dep2_id = '$ob->department_sub2_id' ";
            if($con->query($qd2)->num_rows) {
                echo "<span class='badge badge-success shadow'>".$ob->dep2_head_name."</span>";
            } else {
                echo $ob->dep2_head_name;
            }
                    ?>
                    <button onclick="show_select_head('<?php echo $i;?>','<?php echo $ob->department_id;?>','<?php echo $ob->department_sub2_id;?>','<?php echo $ob->department_sub2_head_id;?>');" class="btn btn-sm text-warning ml-2" ><i class="fas fa-pencil-alt fa-xs"></i></button>
                    <?php
                echo "</span></td>";
                echo "<td> <span id='ol{$i}'>";
                $qs = "SELECT g.member_id FROM edoc_operator AS g WHERE g.dep2_id = $ob->department_sub2_id AND g.ol_id = 2";
                $rs = $con->query($qs) or die ($qs);
                if($rs->num_rows > 0) {
                     $j = 0;
                    while ($obs = $rs->fetch_object()) {
                        $j++;
                        if($j != 1) echo ", ";
                        $member_id = $obs->member_id;
                        $qm = "SELECT concat(name_only,' ',lname) as fullname from members where member_id = '$member_id'";
                        $fullname = $con_s->query($qm)->fetch_object()->fullname;
                        echo $fullname;

                    }
                    
                } else {
                    $member_id = 0;
                }
                    ?>
                    <button onclick="show_select_dep('<?php echo $i;?>','<?php echo $ob->department_id;?>','<?php echo $ob->department_sub2_id;?>','2','<?php echo $member_id;?>');" class="btn btn-sm text-warning ml-2" ><i class="fas fa-pencil-alt fa-xs"></i></button>
                    <?php 
                echo "</span></td>";

            echo "</tr>";
            
        }

        ?>
    </tbody>
</table>


                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$con->close();
$con_s->close();
?>
<script>

function show_select_dep(id,d1,d2,l,m) {
    $('#ol'+id).load("dep_select_operator.php?dep_id="+d1+"&dep2_id="+d2+"&ol_id="+l+"&id="+id+"&member_id="+m);
}
function show_select_head(id,d1,d2,m) {
    //alert(m);
    $('#head'+id).load("dep_select_head.php?dep_id="+d1+"&dep2_id="+d2+"&id="+id+"&member_id="+m);
}
</script>
            