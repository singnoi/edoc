<?php
$con_s = connect();
$con = connect_db($db);
$q = "SELECT
d2.department_sub2_id,
d2.department_id,
d2.department_sub2_name,
d.department_name,
d2.department_sub2_head_id,
d2.dep2_edoc,
d.department_code
FROM
department_sub2 AS d2
LEFT OUTER JOIN departments AS d ON d2.department_id = d.department_id
WHERE
d2.department_id < 7
ORDER BY
d.department_name ASC,
d2.department_sub2_name ASC";
$r = $con_s->query($q) or die ($q);
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    เลขที่สารบรรณ ประจำหน่วยงาน
                </div>
                <div class="card-body">

<table class="table table-sm table-striped table-bordered">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>รายการหน่วยงาน</th>
            <th>เลขที่</th>
            
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        $j = 0;
        $dep_id = 0;
        while ($ob = $r->fetch_object()) {

            // show dep
            if($dep_id != $ob->department_id) {
                $i++;
                $j = 0;
                $dep_id = $ob->department_id;
                $dep2_id = 0;
                 $member_id = 0;
                echo "<tr class='bg-info'>";
                echo "<th colspan='2' >กลุ่มภารกิจ: ".$ob->department_name."</th>";
                echo "<th> <span id='head{$i}'>".$ob->department_code;
                    ?>
                    <button onclick="show_input_code('<?php echo $i;?>','<?php echo $dep_id;?>','0');" class="btn btn-sm text-warning ml-2" ><i class="fas fa-pencil-alt fa-xs"></i></button>
                    <?php
                echo "</span></th>";
 
                echo "</tr>";
            }
            // end show dep
            $i++;
            $j++;

            echo "<tr>";
            echo "<td class='text-right'>$j</td>";
            echo "<td>$ob->department_sub2_name</td>";

            echo "<td> <span id='head{$i}'>".$ob->dep2_edoc;
                    ?>
                    <button onclick="show_input_code('<?php echo $i;?>','<?php echo $ob->department_id;?>','<?php echo $ob->department_sub2_id;?>');" class="btn btn-sm text-warning ml-2" ><i class="fas fa-pencil-alt fa-xs"></i></button>
                    <?php
                echo "</span></td>";
               
            echo "</tr>";
            
        }

        ?>
    </tbody>
</table>


                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$con->close();
$con_s->close();
?>
<script>

function show_input_code(id,d1,d2) {
    //alert(m);
    $('#head'+id).load("dep_code_input.php?dep_id="+d1+"&dep2_id="+d2+"&id="+id);
}
</script>
            