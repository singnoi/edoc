<?php
session_start();
include("../includes/db_connect.php");
include("./includes/function.php");
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "main";
}
  
if (isset($_SESSION['member_id'])) {
    $logined = true;
    $dep2_id = $_SESSION['dep_id'];
    $dep_id = $_SESSION['dep1_id'];
    $member_level = $_SESSION['member_level'];
    $member_id = $_SESSION['member_id'];
    
    if(!isset($_SESSION['operator'])) {
        $con = connect_db($db);
        $con_s = connect();

        if($dep2_id == $main_operator) {
             $_SESSION['main_operator'] = "Y";
        } else {
             $_SESSION['main_operator'] = "N";
        }

        $_SESSION['operator_name'] = "บุคลากร";

        $qg = "SELECT m.o_id, m.ol_id, g.ol_name,m.dep_id,m.dep2_id from edoc_operator as m left outer join edoc_operator_level as g on g.ol_id = m.ol_id where m.member_id = '$member_id' order by m.ol_id ASC limit 1";
        $rg = $con->query($qg) or die ($qg);
        if($rg->num_rows > 0) {
            $obg = $rg->fetch_object();
            $_SESSION['o_id'] = $obg->o_id;
            $_SESSION['operator'] = $obg->ol_id;
            $_SESSION['operator_name'] = $obg->ol_name;
            if($obg->ol_id == 1 || $obg->ol_id == 3) {
                $q2 = "SELECT * from departments where department_id = '$obg->dep_id'";
                $dep_name = $con_s->query($q2)->fetch_object()->department_name;
            } else {
                $q2 = "SELECT * from department_sub2 where department_sub2_id = '$obg->dep2_id'";
                $dep_name = $con_s->query($q2)->fetch_object()->department_sub2_name;
            }
            $_SESSION['doc_dep_id'] = $obg->dep_id;
            $_SESSION['doc_dep2_id'] = $obg->dep2_id;
            $_SESSION['doc_dep_name'] = $dep_name;

            if($obg->ol_id == 1 && $obg->dep_id == 5) {
                $_SESSION['main_operator'] = "Y";
            }


        } else {
            $_SESSION['o_id'] = 0;
            $_SESSION['operator'] = 0;          
            $_SESSION['doc_dep_id'] = $_SESSION['dep1_id'];
            $_SESSION['doc_dep2_id'] = $_SESSION['dep_id'];
            $_SESSION['doc_dep_name'] = $_SESSION['dep_name'];
           
        }
        $con->close();
        $con_s->close();
    }
    
} else {
    $logined = false;
    $dep_id = 0;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>สารบรรณอิเล็กทรอนิกส์ รพ.สิรินธร จ.ขอนแก่น</title>
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../node_modules/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="../node_modules/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="../node_modules/select2-bootstrap-theme-master/dist/select2-bootstrap.css">
    <link rel="stylesheet" href="../node_modules/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="../node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="../node_modules/css/style.css">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../node_modules/select2/dist/js/select2.min.js"></script>
    <script src="../node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../node_modules/bootstrap-datepicker/datepicker3.css" />

<script type="text/javascript" src="../node_modules/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../node_modules/bootstrap-datepicker/bootstrap-datepicker-thai.js"></script>
<script type="text/javascript" src="../node_modules/bootstrap-datepicker/locales/bootstrap-datepicker.th.js"></script>
<script src="../node_modules/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="../node_modules/input-mask/jquery.inputmask.numeric.extensions.js" type="text/javascript"></script>
    

</head>
<body>

<?php
include './includes/header.php';

if($logined) { 
  include($page.'.php');
} else {
  include('title.php');
}

?>

<script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../node_modules/chart.js/dist/Chart.min.js"></script>
<script src="./utils.js"></script>
<script>
$('[data-toggle="tooltip"]').tooltip(); 
</script>

</body>
</html>